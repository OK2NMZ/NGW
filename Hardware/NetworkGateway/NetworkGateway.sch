<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.3">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Gold Plating" color="14" fill="1" visible="yes" active="yes"/>
<layer number="111" name="TopOverlay" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="BottomOverlay" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="Format" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Nep_mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="Potisk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Tester_1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="Tester_2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="Form_arc" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="Values2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="156" name="BRD_Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="157" name="Pozice_ku" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="254" name="SMDcool" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="NetworkGateway">
<packages>
<package name="HC49UP">
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0322" width="0.0508" layer="21" curve="-55.769501" cap="flat"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.127" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485" cap="flat"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.127" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.127" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.127" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.127" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.127" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.127" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.127" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.127" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828" cap="flat"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828" cap="flat"/>
<wire x1="3.429" y1="-2.032" x2="5.1089" y2="-1.1429" width="0.0508" layer="21" curve="55.768337" cap="flat"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524" cap="flat"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213" cap="flat"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828" cap="flat"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828" cap="flat"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.127" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213" cap="flat"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.127" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.127" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.127" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.127" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.127" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.127" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.127" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.127" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.127" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="4.6" dy="1.93" layer="1"/>
<smd name="2" x="4.826" y="0" dx="4.6" dy="1.93" layer="1"/>
<text x="-5.715" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="HC49U-V">
<wire x1="-2.921" y1="-2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="2.286" x2="2.921" y2="2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="-1.778" x2="2.921" y2="-1.778" width="0.127" layer="21"/>
<wire x1="2.921" y1="1.778" x2="-2.921" y2="1.778" width="0.127" layer="21"/>
<wire x1="2.921" y1="1.778" x2="2.921" y2="-1.778" width="0.127" layer="21" curve="-180"/>
<wire x1="2.921" y1="2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.921" y1="2.286" x2="-2.921" y2="-2.286" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.921" y1="1.778" x2="-2.921" y2="-1.778" width="0.127" layer="21" curve="180"/>
<wire x1="5.588" y1="-1.143" x2="5.588" y2="1.143" width="0" layer="43"/>
<wire x1="5.588" y1="1.143" x2="4.064" y2="2.667" width="0" layer="43"/>
<wire x1="4.064" y1="2.667" x2="-4.064" y2="2.667" width="0" layer="43"/>
<wire x1="-5.588" y1="1.143" x2="-4.064" y2="2.667" width="0" layer="43"/>
<wire x1="-5.588" y1="1.143" x2="-5.588" y2="-1.143" width="0" layer="43"/>
<wire x1="-4.064" y1="-2.667" x2="-5.588" y2="-1.143" width="0" layer="43"/>
<wire x1="5.588" y1="-1.143" x2="4.064" y2="-2.667" width="0" layer="43"/>
<wire x1="4.064" y1="-2.667" x2="-4.064" y2="-2.667" width="0" layer="43"/>
<wire x1="-0.254" y1="0.889" x2="0.254" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.889" x2="0.254" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.889" x2="-0.254" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.889" x2="-0.254" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.889" x2="0.635" y2="0" width="0.127" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.889" x2="-0.635" y2="0" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.127" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="2.413" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-1.78" y="2.821" size="0.762" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.08" y="-3.591" size="0.762" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SOIC127P600X175-8N">
<smd name="1" x="-2.4638" y="1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="2" x="-2.4638" y="0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="3" x="-2.4638" y="-0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="4" x="-2.4638" y="-1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="5" x="2.4638" y="-1.905" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="6" x="2.4638" y="-0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="7" x="2.4638" y="0.635" dx="1.9812" dy="0.5588" layer="1"/>
<smd name="8" x="2.4638" y="1.905" dx="1.9812" dy="0.5588" layer="1"/>
<wire x1="-2.0066" y1="1.651" x2="-2.0066" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.159" x2="-3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.159" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.381" x2="-2.0066" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.889" x2="-3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.889" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.889" x2="-2.0066" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.381" x2="-3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.381" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.159" x2="-2.0066" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.651" x2="-3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.651" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.651" x2="2.0066" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.159" x2="3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.381" x2="2.0066" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.889" x2="3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.889" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.889" x2="2.0066" y2="0.381" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.381" x2="3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.381" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.889" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.159" x2="2.0066" y2="1.651" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.651" x2="3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.651" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.127" layer="51" curve="-180"/>
<wire x1="-1.8796" y1="-2.4892" x2="1.8796" y2="-2.4892" width="0.1524" layer="21"/>
<wire x1="1.8796" y1="2.4892" x2="-1.8796" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0" layer="21" curve="-180"/>
<text x="-4.2926" y="2.9972" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.4864" y="-5.0292" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<circle x="-2.9" y="2.8" radius="0.22360625" width="0.4064" layer="21"/>
</package>
<package name="QFP50P1600X1600X160-100N">
<smd name="1" x="-7.7216" y="5.9944" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="2" x="-7.7216" y="5.5118" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="3" x="-7.7216" y="5.0038" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="4" x="-7.7216" y="4.4958" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="5" x="-7.7216" y="3.9878" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="6" x="-7.7216" y="3.5052" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="7" x="-7.7216" y="2.9972" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="8" x="-7.7216" y="2.4892" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="9" x="-7.7216" y="2.0066" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="10" x="-7.7216" y="1.4986" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="11" x="-7.7216" y="0.9906" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="12" x="-7.7216" y="0.508" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="13" x="-7.7216" y="0" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="14" x="-7.7216" y="-0.508" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="15" x="-7.7216" y="-0.9906" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="16" x="-7.7216" y="-1.4986" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="17" x="-7.7216" y="-2.0066" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="18" x="-7.7216" y="-2.4892" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="19" x="-7.7216" y="-2.9972" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="20" x="-7.7216" y="-3.5052" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="21" x="-7.7216" y="-3.9878" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="22" x="-7.7216" y="-4.4958" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="23" x="-7.7216" y="-5.0038" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="24" x="-7.7216" y="-5.5118" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="25" x="-7.7216" y="-5.9944" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="26" x="-5.9944" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="27" x="-5.5118" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="28" x="-5.0038" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="29" x="-4.4958" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="30" x="-3.9878" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="31" x="-3.5052" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="32" x="-2.9972" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="33" x="-2.4892" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="34" x="-2.0066" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="35" x="-1.4986" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="36" x="-0.9906" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="37" x="-0.508" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="38" x="0" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="39" x="0.508" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="40" x="0.9906" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="41" x="1.4986" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="42" x="2.0066" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="43" x="2.4892" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="44" x="2.9972" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="45" x="3.5052" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="46" x="3.9878" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="47" x="4.4958" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="48" x="5.0038" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="49" x="5.5118" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="50" x="5.9944" y="-7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="51" x="7.7216" y="-5.9944" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="52" x="7.7216" y="-5.5118" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="53" x="7.7216" y="-5.0038" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="54" x="7.7216" y="-4.4958" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="55" x="7.7216" y="-3.9878" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="56" x="7.7216" y="-3.5052" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="57" x="7.7216" y="-2.9972" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="58" x="7.7216" y="-2.4892" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="59" x="7.7216" y="-2.0066" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="60" x="7.7216" y="-1.4986" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="61" x="7.7216" y="-0.9906" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="62" x="7.7216" y="-0.508" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="63" x="7.7216" y="0" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="64" x="7.7216" y="0.508" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="65" x="7.7216" y="0.9906" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="66" x="7.7216" y="1.4986" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="67" x="7.7216" y="2.0066" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="68" x="7.7216" y="2.4892" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="69" x="7.7216" y="2.9972" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="70" x="7.7216" y="3.5052" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="71" x="7.7216" y="3.9878" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="72" x="7.7216" y="4.4958" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="73" x="7.7216" y="5.0038" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="74" x="7.7216" y="5.5118" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="75" x="7.7216" y="5.9944" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="76" x="5.9944" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="77" x="5.5118" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="78" x="5.0038" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="79" x="4.4958" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="80" x="3.9878" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="81" x="3.5052" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="82" x="2.9972" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="83" x="2.4892" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="84" x="2.0066" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="85" x="1.4986" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="86" x="0.9906" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="87" x="0.508" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="88" x="0" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="89" x="-0.508" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="90" x="-0.9906" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="91" x="-1.4986" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="92" x="-2.0066" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="93" x="-2.4892" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="94" x="-2.9972" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="95" x="-3.5052" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="96" x="-3.9878" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="97" x="-4.4958" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="98" x="-5.0038" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="99" x="-5.5118" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="100" x="-5.9944" y="7.7216" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<wire x1="-8.9662" y1="1.7018" x2="-8.9662" y2="1.3208" width="0.1524" layer="49"/>
<wire x1="-8.9662" y1="1.3208" x2="-8.7122" y2="1.3208" width="0.1524" layer="49"/>
<wire x1="-8.7122" y1="1.3208" x2="-8.7122" y2="1.7018" width="0.1524" layer="49"/>
<wire x1="-8.7122" y1="1.7018" x2="-8.9662" y2="1.7018" width="0.1524" layer="49"/>
<wire x1="-8.9662" y1="-3.302" x2="-8.9662" y2="-3.683" width="0.1524" layer="49"/>
<wire x1="-8.9662" y1="-3.683" x2="-8.7122" y2="-3.683" width="0.1524" layer="49"/>
<wire x1="-8.7122" y1="-3.683" x2="-8.7122" y2="-3.302" width="0.1524" layer="49"/>
<wire x1="-8.7122" y1="-3.302" x2="-8.9662" y2="-3.302" width="0.1524" layer="49"/>
<wire x1="-4.191" y1="-8.7122" x2="-4.191" y2="-8.9662" width="0.1524" layer="49"/>
<wire x1="-4.191" y1="-8.9662" x2="-3.81" y2="-8.9662" width="0.1524" layer="49"/>
<wire x1="-3.81" y1="-8.9662" x2="-3.81" y2="-8.7122" width="0.1524" layer="49"/>
<wire x1="-3.81" y1="-8.7122" x2="-4.191" y2="-8.7122" width="0.1524" layer="49"/>
<wire x1="0.8128" y1="-8.7122" x2="0.8128" y2="-8.9662" width="0.1524" layer="49"/>
<wire x1="0.8128" y1="-8.9662" x2="1.1938" y2="-8.9662" width="0.1524" layer="49"/>
<wire x1="1.1938" y1="-8.9662" x2="1.1938" y2="-8.7122" width="0.1524" layer="49"/>
<wire x1="1.1938" y1="-8.7122" x2="0.8128" y2="-8.7122" width="0.1524" layer="49"/>
<wire x1="5.8166" y1="-8.7122" x2="5.8166" y2="-8.9662" width="0.1524" layer="49"/>
<wire x1="5.8166" y1="-8.9662" x2="6.1976" y2="-8.9662" width="0.1524" layer="49"/>
<wire x1="6.1976" y1="-8.9662" x2="6.1976" y2="-8.7122" width="0.1524" layer="49"/>
<wire x1="6.1976" y1="-8.7122" x2="5.8166" y2="-8.7122" width="0.1524" layer="49"/>
<wire x1="8.9662" y1="-1.3208" x2="8.9662" y2="-1.7018" width="0.1524" layer="49"/>
<wire x1="8.9662" y1="-1.7018" x2="8.7122" y2="-1.7018" width="0.1524" layer="49"/>
<wire x1="8.7122" y1="-1.7018" x2="8.7122" y2="-1.3208" width="0.1524" layer="49"/>
<wire x1="8.7122" y1="-1.3208" x2="8.9662" y2="-1.3208" width="0.1524" layer="49"/>
<wire x1="8.9662" y1="3.683" x2="8.9662" y2="3.302" width="0.1524" layer="49"/>
<wire x1="8.9662" y1="3.302" x2="8.7122" y2="3.302" width="0.1524" layer="49"/>
<wire x1="8.7122" y1="3.302" x2="8.7122" y2="3.683" width="0.1524" layer="49"/>
<wire x1="8.7122" y1="3.683" x2="8.9662" y2="3.683" width="0.1524" layer="49"/>
<wire x1="3.81" y1="8.7122" x2="3.81" y2="8.9662" width="0.1524" layer="49"/>
<wire x1="3.81" y1="8.9662" x2="4.191" y2="8.9662" width="0.1524" layer="49"/>
<wire x1="4.191" y1="8.9662" x2="4.191" y2="8.7122" width="0.1524" layer="49"/>
<wire x1="4.191" y1="8.7122" x2="3.81" y2="8.7122" width="0.1524" layer="49"/>
<wire x1="-1.1938" y1="8.7122" x2="-1.1938" y2="8.9662" width="0.1524" layer="49"/>
<wire x1="-1.1938" y1="8.9662" x2="-0.8128" y2="8.9662" width="0.1524" layer="49"/>
<wire x1="-0.8128" y1="8.9662" x2="-0.8128" y2="8.7122" width="0.1524" layer="49"/>
<wire x1="-0.8128" y1="8.7122" x2="-1.1938" y2="8.7122" width="0.1524" layer="49"/>
<wire x1="-6.1976" y1="8.7122" x2="-6.1976" y2="8.9662" width="0.1524" layer="49"/>
<wire x1="-6.1976" y1="8.9662" x2="-5.8166" y2="8.9662" width="0.1524" layer="49"/>
<wire x1="-5.8166" y1="8.9662" x2="-5.8166" y2="8.7122" width="0.1524" layer="49"/>
<wire x1="-5.8166" y1="8.7122" x2="-6.1976" y2="8.7122" width="0.1524" layer="49"/>
<wire x1="-8.7122" y1="-8.7122" x2="-8.7122" y2="8.7122" width="0.1524" layer="39"/>
<wire x1="-8.7122" y1="8.7122" x2="8.7122" y2="8.7122" width="0.1524" layer="39"/>
<wire x1="8.7122" y1="8.7122" x2="8.7122" y2="-8.7122" width="0.1524" layer="39"/>
<wire x1="8.7122" y1="-8.7122" x2="-8.7122" y2="-8.7122" width="0.1524" layer="39"/>
<wire x1="-8.7122" y1="-8.7122" x2="-8.7122" y2="8.7122" width="0.1524" layer="39"/>
<wire x1="-8.7122" y1="8.7122" x2="8.7122" y2="8.7122" width="0.1524" layer="39"/>
<wire x1="8.7122" y1="8.7122" x2="8.7122" y2="-8.7122" width="0.1524" layer="39"/>
<wire x1="8.7122" y1="-8.7122" x2="-8.7122" y2="-8.7122" width="0.1524" layer="39"/>
<wire x1="-6.477" y1="7.112" x2="-7.112" y2="7.112" width="0.1524" layer="21"/>
<wire x1="7.112" y1="6.477" x2="7.112" y2="7.112" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-7.112" x2="7.112" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="-6.6802" y1="6.2484" x2="-6.2484" y2="6.6802" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="-7.112" x2="-6.477" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="7.112" y1="-7.112" x2="7.112" y2="-6.477" width="0.1524" layer="21"/>
<wire x1="7.112" y1="7.112" x2="6.477" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="7.112" x2="-7.112" y2="6.477" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="-6.477" x2="-7.112" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="5.8674" y1="7.112" x2="6.1468" y2="7.112" width="0.1524" layer="51"/>
<wire x1="6.1468" y1="7.112" x2="6.1468" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="6.1468" y1="8.1026" x2="5.8674" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="5.8674" y1="8.1026" x2="5.8674" y2="7.112" width="0.1524" layer="51"/>
<wire x1="5.3594" y1="7.112" x2="5.6388" y2="7.112" width="0.1524" layer="51"/>
<wire x1="5.6388" y1="7.112" x2="5.6388" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="5.6388" y1="8.1026" x2="5.3594" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="5.3594" y1="8.1026" x2="5.3594" y2="7.112" width="0.1524" layer="51"/>
<wire x1="4.8768" y1="7.112" x2="5.1308" y2="7.112" width="0.1524" layer="51"/>
<wire x1="5.1308" y1="7.112" x2="5.1308" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="5.1308" y1="8.1026" x2="4.8768" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="4.8768" y1="8.1026" x2="4.8768" y2="7.112" width="0.1524" layer="51"/>
<wire x1="4.3688" y1="7.112" x2="4.6228" y2="7.112" width="0.1524" layer="51"/>
<wire x1="4.6228" y1="7.112" x2="4.6228" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="4.6228" y1="8.1026" x2="4.3688" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="4.3688" y1="8.1026" x2="4.3688" y2="7.112" width="0.1524" layer="51"/>
<wire x1="3.8608" y1="7.112" x2="4.1402" y2="7.112" width="0.1524" layer="51"/>
<wire x1="4.1402" y1="7.112" x2="4.1402" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="4.1402" y1="8.1026" x2="3.8608" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="3.8608" y1="8.1026" x2="3.8608" y2="7.112" width="0.1524" layer="51"/>
<wire x1="3.3528" y1="7.112" x2="3.6322" y2="7.112" width="0.1524" layer="51"/>
<wire x1="3.6322" y1="7.112" x2="3.6322" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="3.6322" y1="8.1026" x2="3.3528" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="3.3528" y1="8.1026" x2="3.3528" y2="7.112" width="0.1524" layer="51"/>
<wire x1="2.8702" y1="7.112" x2="3.1242" y2="7.112" width="0.1524" layer="51"/>
<wire x1="3.1242" y1="7.112" x2="3.1242" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="3.1242" y1="8.1026" x2="2.8702" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="2.8702" y1="8.1026" x2="2.8702" y2="7.112" width="0.1524" layer="51"/>
<wire x1="2.3622" y1="7.112" x2="2.6416" y2="7.112" width="0.1524" layer="51"/>
<wire x1="2.6416" y1="7.112" x2="2.6416" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="2.6416" y1="8.1026" x2="2.3622" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="2.3622" y1="8.1026" x2="2.3622" y2="7.112" width="0.1524" layer="51"/>
<wire x1="1.8542" y1="7.112" x2="2.1336" y2="7.112" width="0.1524" layer="51"/>
<wire x1="2.1336" y1="7.112" x2="2.1336" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="2.1336" y1="8.1026" x2="1.8542" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="1.8542" y1="8.1026" x2="1.8542" y2="7.112" width="0.1524" layer="51"/>
<wire x1="1.3716" y1="7.112" x2="1.6256" y2="7.112" width="0.1524" layer="51"/>
<wire x1="1.6256" y1="7.112" x2="1.6256" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="1.6256" y1="8.1026" x2="1.3716" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="1.3716" y1="8.1026" x2="1.3716" y2="7.112" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="7.112" x2="1.143" y2="7.112" width="0.1524" layer="51"/>
<wire x1="1.143" y1="7.112" x2="1.143" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="1.143" y1="8.1026" x2="0.8636" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="8.1026" x2="0.8636" y2="7.112" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="7.112" x2="0.635" y2="7.112" width="0.1524" layer="51"/>
<wire x1="0.635" y1="7.112" x2="0.635" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="0.635" y1="8.1026" x2="0.3556" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="8.1026" x2="0.3556" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-0.127" y1="7.112" x2="0.127" y2="7.112" width="0.1524" layer="51"/>
<wire x1="0.127" y1="7.112" x2="0.127" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="0.127" y1="8.1026" x2="-0.127" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-0.127" y1="8.1026" x2="-0.127" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="7.112" x2="-0.3556" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="7.112" x2="-0.3556" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="8.1026" x2="-0.635" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="8.1026" x2="-0.635" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="7.112" x2="-0.8636" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="7.112" x2="-0.8636" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="8.1026" x2="-1.143" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="8.1026" x2="-1.143" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-1.6256" y1="7.112" x2="-1.3716" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-1.3716" y1="7.112" x2="-1.3716" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-1.3716" y1="8.1026" x2="-1.6256" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-1.6256" y1="8.1026" x2="-1.6256" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-2.1336" y1="7.112" x2="-1.8542" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="7.112" x2="-1.8542" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="8.1026" x2="-2.1336" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-2.1336" y1="8.1026" x2="-2.1336" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-2.6416" y1="7.112" x2="-2.3622" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-2.3622" y1="7.112" x2="-2.3622" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-2.3622" y1="8.1026" x2="-2.6416" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-2.6416" y1="8.1026" x2="-2.6416" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-3.1242" y1="7.112" x2="-2.8702" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-2.8702" y1="7.112" x2="-2.8702" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-2.8702" y1="8.1026" x2="-3.1242" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-3.1242" y1="8.1026" x2="-3.1242" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-3.6322" y1="7.112" x2="-3.3528" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-3.3528" y1="7.112" x2="-3.3528" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-3.3528" y1="8.1026" x2="-3.6322" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-3.6322" y1="8.1026" x2="-3.6322" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-4.1402" y1="7.112" x2="-3.8608" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-3.8608" y1="7.112" x2="-3.8608" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-3.8608" y1="8.1026" x2="-4.1402" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-4.1402" y1="8.1026" x2="-4.1402" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-4.6228" y1="7.112" x2="-4.3688" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-4.3688" y1="7.112" x2="-4.3688" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-4.3688" y1="8.1026" x2="-4.6228" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-4.6228" y1="8.1026" x2="-4.6228" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-5.1308" y1="7.112" x2="-4.8768" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-4.8768" y1="7.112" x2="-4.8768" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-4.8768" y1="8.1026" x2="-5.1308" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-5.1308" y1="8.1026" x2="-5.1308" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-5.6388" y1="7.112" x2="-5.3594" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-5.3594" y1="7.112" x2="-5.3594" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-5.3594" y1="8.1026" x2="-5.6388" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-5.6388" y1="8.1026" x2="-5.6388" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-6.1468" y1="7.112" x2="-5.8674" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-5.8674" y1="7.112" x2="-5.8674" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-5.8674" y1="8.1026" x2="-6.1468" y2="8.1026" width="0.1524" layer="51"/>
<wire x1="-6.1468" y1="8.1026" x2="-6.1468" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="5.8674" x2="-7.112" y2="6.1468" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="6.1468" x2="-8.1026" y2="6.1468" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="6.1468" x2="-8.1026" y2="5.8674" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="5.8674" x2="-7.112" y2="5.8674" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="5.3594" x2="-7.112" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="5.6388" x2="-8.1026" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="5.6388" x2="-8.1026" y2="5.3594" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="5.3594" x2="-7.112" y2="5.3594" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="4.8768" x2="-7.112" y2="5.1308" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="5.1308" x2="-8.1026" y2="5.1308" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="5.1308" x2="-8.1026" y2="4.8768" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="4.8768" x2="-7.112" y2="4.8768" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="4.3688" x2="-7.112" y2="4.6228" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="4.6228" x2="-8.1026" y2="4.6228" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="4.6228" x2="-8.1026" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="4.3688" x2="-7.112" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="3.8608" x2="-7.112" y2="4.1402" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="4.1402" x2="-8.1026" y2="4.1402" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="4.1402" x2="-8.1026" y2="3.8608" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="3.8608" x2="-7.112" y2="3.8608" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="3.3528" x2="-7.112" y2="3.6322" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="3.6322" x2="-8.1026" y2="3.6322" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="3.6322" x2="-8.1026" y2="3.3528" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="3.3528" x2="-7.112" y2="3.3528" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="2.8702" x2="-7.112" y2="3.1242" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="3.1242" x2="-8.1026" y2="3.1242" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="3.1242" x2="-8.1026" y2="2.8702" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="2.8702" x2="-7.112" y2="2.8702" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="2.3622" x2="-7.112" y2="2.6416" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="2.6416" x2="-8.1026" y2="2.6416" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="2.6416" x2="-8.1026" y2="2.3622" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="2.3622" x2="-7.112" y2="2.3622" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="1.8542" x2="-7.112" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="2.1336" x2="-8.1026" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="2.1336" x2="-8.1026" y2="1.8542" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="1.8542" x2="-7.112" y2="1.8542" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="1.3716" x2="-7.112" y2="1.6256" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="1.6256" x2="-8.1026" y2="1.6256" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="1.6256" x2="-8.1026" y2="1.3716" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="1.3716" x2="-7.112" y2="1.3716" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="0.8636" x2="-7.112" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="1.143" x2="-8.1026" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="1.143" x2="-8.1026" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="0.8636" x2="-7.112" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="0.3556" x2="-7.112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="0.635" x2="-8.1026" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="0.635" x2="-8.1026" y2="0.3556" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="0.3556" x2="-7.112" y2="0.3556" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-0.127" x2="-7.112" y2="0.127" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="0.127" x2="-8.1026" y2="0.127" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="0.127" x2="-8.1026" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-0.127" x2="-7.112" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-0.635" x2="-7.112" y2="-0.3556" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-0.3556" x2="-8.1026" y2="-0.3556" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-0.3556" x2="-8.1026" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-0.635" x2="-7.112" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-1.143" x2="-7.112" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-0.8636" x2="-8.1026" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-0.8636" x2="-8.1026" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-1.143" x2="-7.112" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-1.6256" x2="-7.112" y2="-1.3716" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-1.3716" x2="-8.1026" y2="-1.3716" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-1.3716" x2="-8.1026" y2="-1.6256" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-1.6256" x2="-7.112" y2="-1.6256" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-2.1336" x2="-7.112" y2="-1.8542" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-1.8542" x2="-8.1026" y2="-1.8542" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-1.8542" x2="-8.1026" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-2.1336" x2="-7.112" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-2.6416" x2="-7.112" y2="-2.3622" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-2.3622" x2="-8.1026" y2="-2.3622" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-2.3622" x2="-8.1026" y2="-2.6416" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-2.6416" x2="-7.112" y2="-2.6416" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-3.1242" x2="-7.112" y2="-2.8702" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-2.8702" x2="-8.1026" y2="-2.8702" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-2.8702" x2="-8.1026" y2="-3.1242" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-3.1242" x2="-7.112" y2="-3.1242" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-3.6322" x2="-7.112" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-3.3528" x2="-8.1026" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-3.3528" x2="-8.1026" y2="-3.6322" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-3.6322" x2="-7.112" y2="-3.6322" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-4.1402" x2="-7.112" y2="-3.8608" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-3.8608" x2="-8.1026" y2="-3.8608" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-3.8608" x2="-8.1026" y2="-4.1402" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-4.1402" x2="-7.112" y2="-4.1402" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-4.6228" x2="-7.112" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-4.3688" x2="-8.1026" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-4.3688" x2="-8.1026" y2="-4.6228" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-4.6228" x2="-7.112" y2="-4.6228" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-5.1308" x2="-7.112" y2="-4.8768" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-4.8768" x2="-8.1026" y2="-4.8768" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-4.8768" x2="-8.1026" y2="-5.1308" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-5.1308" x2="-7.112" y2="-5.1308" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-5.6388" x2="-7.112" y2="-5.3594" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-5.3594" x2="-8.1026" y2="-5.3594" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-5.3594" x2="-8.1026" y2="-5.6388" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-5.6388" x2="-7.112" y2="-5.6388" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-6.1468" x2="-7.112" y2="-5.8674" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-5.8674" x2="-8.1026" y2="-5.8674" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-5.8674" x2="-8.1026" y2="-6.1468" width="0.1524" layer="51"/>
<wire x1="-8.1026" y1="-6.1468" x2="-7.112" y2="-6.1468" width="0.1524" layer="51"/>
<wire x1="-5.8674" y1="-7.112" x2="-6.1468" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-6.1468" y1="-7.112" x2="-6.1468" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-6.1468" y1="-8.1026" x2="-5.8674" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-5.8674" y1="-8.1026" x2="-5.8674" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-5.3594" y1="-7.112" x2="-5.6388" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-5.6388" y1="-7.112" x2="-5.6388" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-5.6388" y1="-8.1026" x2="-5.3594" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-5.3594" y1="-8.1026" x2="-5.3594" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-4.8768" y1="-7.112" x2="-5.1308" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-5.1308" y1="-7.112" x2="-5.1308" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-5.1308" y1="-8.1026" x2="-4.8768" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-4.8768" y1="-8.1026" x2="-4.8768" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-4.3688" y1="-7.112" x2="-4.6228" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-4.6228" y1="-7.112" x2="-4.6228" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-4.6228" y1="-8.1026" x2="-4.3688" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-4.3688" y1="-8.1026" x2="-4.3688" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-3.8608" y1="-7.112" x2="-4.1402" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-4.1402" y1="-7.112" x2="-4.1402" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-4.1402" y1="-8.1026" x2="-3.8608" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-3.8608" y1="-8.1026" x2="-3.8608" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-3.3528" y1="-7.112" x2="-3.6322" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-3.6322" y1="-7.112" x2="-3.6322" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-3.6322" y1="-8.1026" x2="-3.3528" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-3.3528" y1="-8.1026" x2="-3.3528" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-2.8702" y1="-7.112" x2="-3.1242" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-3.1242" y1="-7.112" x2="-3.1242" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-3.1242" y1="-8.1026" x2="-2.8702" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-2.8702" y1="-8.1026" x2="-2.8702" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-2.3622" y1="-7.112" x2="-2.6416" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-2.6416" y1="-7.112" x2="-2.6416" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-2.6416" y1="-8.1026" x2="-2.3622" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-2.3622" y1="-8.1026" x2="-2.3622" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="-7.112" x2="-2.1336" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-2.1336" y1="-7.112" x2="-2.1336" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-2.1336" y1="-8.1026" x2="-1.8542" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="-8.1026" x2="-1.8542" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-1.3716" y1="-7.112" x2="-1.6256" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-1.6256" y1="-7.112" x2="-1.6256" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-1.6256" y1="-8.1026" x2="-1.3716" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-1.3716" y1="-8.1026" x2="-1.3716" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="-7.112" x2="-1.143" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-7.112" x2="-1.143" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-8.1026" x2="-0.8636" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-0.8636" y1="-8.1026" x2="-0.8636" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="-7.112" x2="-0.635" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-7.112" x2="-0.635" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-8.1026" x2="-0.3556" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-0.3556" y1="-8.1026" x2="-0.3556" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="0.127" y1="-7.112" x2="-0.127" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-0.127" y1="-7.112" x2="-0.127" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="-0.127" y1="-8.1026" x2="0.127" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="0.127" y1="-8.1026" x2="0.127" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-7.112" x2="0.3556" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="-7.112" x2="0.3556" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="0.3556" y1="-8.1026" x2="0.635" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-8.1026" x2="0.635" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-7.112" x2="0.8636" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="-7.112" x2="0.8636" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="0.8636" y1="-8.1026" x2="1.143" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-8.1026" x2="1.143" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="1.6256" y1="-7.112" x2="1.3716" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="1.3716" y1="-7.112" x2="1.3716" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="1.3716" y1="-8.1026" x2="1.6256" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="1.6256" y1="-8.1026" x2="1.6256" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="2.1336" y1="-7.112" x2="1.8542" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="1.8542" y1="-7.112" x2="1.8542" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="1.8542" y1="-8.1026" x2="2.1336" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="2.1336" y1="-8.1026" x2="2.1336" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="2.6416" y1="-7.112" x2="2.3622" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="2.3622" y1="-7.112" x2="2.3622" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="2.3622" y1="-8.1026" x2="2.6416" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="2.6416" y1="-8.1026" x2="2.6416" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="3.1242" y1="-7.112" x2="2.8702" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="2.8702" y1="-7.112" x2="2.8702" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="2.8702" y1="-8.1026" x2="3.1242" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="3.1242" y1="-8.1026" x2="3.1242" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="3.6322" y1="-7.112" x2="3.3528" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="3.3528" y1="-7.112" x2="3.3528" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="3.3528" y1="-8.1026" x2="3.6322" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="3.6322" y1="-8.1026" x2="3.6322" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="4.1402" y1="-7.112" x2="3.8608" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="3.8608" y1="-7.112" x2="3.8608" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="3.8608" y1="-8.1026" x2="4.1402" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="4.1402" y1="-8.1026" x2="4.1402" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="4.6228" y1="-7.112" x2="4.3688" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="4.3688" y1="-7.112" x2="4.3688" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="4.3688" y1="-8.1026" x2="4.6228" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="4.6228" y1="-8.1026" x2="4.6228" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="5.1308" y1="-7.112" x2="4.8768" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="4.8768" y1="-7.112" x2="4.8768" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="4.8768" y1="-8.1026" x2="5.1308" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="5.1308" y1="-8.1026" x2="5.1308" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="5.6388" y1="-7.112" x2="5.3594" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="5.3594" y1="-7.112" x2="5.3594" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="5.3594" y1="-8.1026" x2="5.6388" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="5.6388" y1="-8.1026" x2="5.6388" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="6.1468" y1="-7.112" x2="5.8674" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="5.8674" y1="-7.112" x2="5.8674" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="5.8674" y1="-8.1026" x2="6.1468" y2="-8.1026" width="0.1524" layer="51"/>
<wire x1="6.1468" y1="-8.1026" x2="6.1468" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-5.8674" x2="7.112" y2="-6.1468" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-6.1468" x2="8.1026" y2="-6.1468" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-6.1468" x2="8.1026" y2="-5.8674" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-5.8674" x2="7.112" y2="-5.8674" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-5.3594" x2="7.112" y2="-5.6388" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-5.6388" x2="8.1026" y2="-5.6388" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-5.6388" x2="8.1026" y2="-5.3594" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-5.3594" x2="7.112" y2="-5.3594" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-4.8768" x2="7.112" y2="-5.1308" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-5.1308" x2="8.1026" y2="-5.1308" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-5.1308" x2="8.1026" y2="-4.8768" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-4.8768" x2="7.112" y2="-4.8768" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-4.3688" x2="7.112" y2="-4.6228" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-4.6228" x2="8.1026" y2="-4.6228" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-4.6228" x2="8.1026" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-4.3688" x2="7.112" y2="-4.3688" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-3.8608" x2="7.112" y2="-4.1402" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-4.1402" x2="8.1026" y2="-4.1402" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-4.1402" x2="8.1026" y2="-3.8608" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-3.8608" x2="7.112" y2="-3.8608" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-3.3528" x2="7.112" y2="-3.6322" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-3.6322" x2="8.1026" y2="-3.6322" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-3.6322" x2="8.1026" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-3.3528" x2="7.112" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-2.8702" x2="7.112" y2="-3.1242" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-3.1242" x2="8.1026" y2="-3.1242" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-3.1242" x2="8.1026" y2="-2.8702" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-2.8702" x2="7.112" y2="-2.8702" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-2.3622" x2="7.112" y2="-2.6416" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-2.6416" x2="8.1026" y2="-2.6416" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-2.6416" x2="8.1026" y2="-2.3622" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-2.3622" x2="7.112" y2="-2.3622" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-1.8542" x2="7.112" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-2.1336" x2="8.1026" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-2.1336" x2="8.1026" y2="-1.8542" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-1.8542" x2="7.112" y2="-1.8542" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-1.3716" x2="7.112" y2="-1.6256" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-1.6256" x2="8.1026" y2="-1.6256" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-1.6256" x2="8.1026" y2="-1.3716" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-1.3716" x2="7.112" y2="-1.3716" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-0.8636" x2="7.112" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-1.143" x2="8.1026" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-1.143" x2="8.1026" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-0.8636" x2="7.112" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-0.3556" x2="7.112" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-0.635" x2="8.1026" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-0.635" x2="8.1026" y2="-0.3556" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-0.3556" x2="7.112" y2="-0.3556" width="0.1524" layer="51"/>
<wire x1="7.112" y1="0.127" x2="7.112" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-0.127" x2="8.1026" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="-0.127" x2="8.1026" y2="0.127" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="0.127" x2="7.112" y2="0.127" width="0.1524" layer="51"/>
<wire x1="7.112" y1="0.635" x2="7.112" y2="0.3556" width="0.1524" layer="51"/>
<wire x1="7.112" y1="0.3556" x2="8.1026" y2="0.3556" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="0.3556" x2="8.1026" y2="0.635" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="0.635" x2="7.112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="7.112" y1="1.143" x2="7.112" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="7.112" y1="0.8636" x2="8.1026" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="0.8636" x2="8.1026" y2="1.143" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="1.143" x2="7.112" y2="1.143" width="0.1524" layer="51"/>
<wire x1="7.112" y1="1.6256" x2="7.112" y2="1.3716" width="0.1524" layer="51"/>
<wire x1="7.112" y1="1.3716" x2="8.1026" y2="1.3716" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="1.3716" x2="8.1026" y2="1.6256" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="1.6256" x2="7.112" y2="1.6256" width="0.1524" layer="51"/>
<wire x1="7.112" y1="2.1336" x2="7.112" y2="1.8542" width="0.1524" layer="51"/>
<wire x1="7.112" y1="1.8542" x2="8.1026" y2="1.8542" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="1.8542" x2="8.1026" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="2.1336" x2="7.112" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="7.112" y1="2.6416" x2="7.112" y2="2.3622" width="0.1524" layer="51"/>
<wire x1="7.112" y1="2.3622" x2="8.1026" y2="2.3622" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="2.3622" x2="8.1026" y2="2.6416" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="2.6416" x2="7.112" y2="2.6416" width="0.1524" layer="51"/>
<wire x1="7.112" y1="3.1242" x2="7.112" y2="2.8702" width="0.1524" layer="51"/>
<wire x1="7.112" y1="2.8702" x2="8.1026" y2="2.8702" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="2.8702" x2="8.1026" y2="3.1242" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="3.1242" x2="7.112" y2="3.1242" width="0.1524" layer="51"/>
<wire x1="7.112" y1="3.6322" x2="7.112" y2="3.3528" width="0.1524" layer="51"/>
<wire x1="7.112" y1="3.3528" x2="8.1026" y2="3.3528" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="3.3528" x2="8.1026" y2="3.6322" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="3.6322" x2="7.112" y2="3.6322" width="0.1524" layer="51"/>
<wire x1="7.112" y1="4.1402" x2="7.112" y2="3.8608" width="0.1524" layer="51"/>
<wire x1="7.112" y1="3.8608" x2="8.1026" y2="3.8608" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="3.8608" x2="8.1026" y2="4.1402" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="4.1402" x2="7.112" y2="4.1402" width="0.1524" layer="51"/>
<wire x1="7.112" y1="4.6228" x2="7.112" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="7.112" y1="4.3688" x2="8.1026" y2="4.3688" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="4.3688" x2="8.1026" y2="4.6228" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="4.6228" x2="7.112" y2="4.6228" width="0.1524" layer="51"/>
<wire x1="7.112" y1="5.1308" x2="7.112" y2="4.8768" width="0.1524" layer="51"/>
<wire x1="7.112" y1="4.8768" x2="8.1026" y2="4.8768" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="4.8768" x2="8.1026" y2="5.1308" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="5.1308" x2="7.112" y2="5.1308" width="0.1524" layer="51"/>
<wire x1="7.112" y1="5.6388" x2="7.112" y2="5.3594" width="0.1524" layer="51"/>
<wire x1="7.112" y1="5.3594" x2="8.1026" y2="5.3594" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="5.3594" x2="8.1026" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="5.6388" x2="7.112" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="7.112" y1="6.1468" x2="7.112" y2="5.8674" width="0.1524" layer="51"/>
<wire x1="7.112" y1="5.8674" x2="8.1026" y2="5.8674" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="5.8674" x2="8.1026" y2="6.1468" width="0.1524" layer="51"/>
<wire x1="8.1026" y1="6.1468" x2="7.112" y2="6.1468" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="5.842" x2="-5.842" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-7.112" x2="7.112" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="7.112" y1="-7.112" x2="7.112" y2="7.112" width="0.1524" layer="51"/>
<wire x1="7.112" y1="7.112" x2="-7.112" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="7.112" x2="-7.112" y2="-7.112" width="0.1524" layer="51"/>
<text x="-4.5466" y="9.7282" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.3594" y="-11.303" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<circle x="-8" y="6.6" radius="0.1" width="0.4064" layer="21"/>
</package>
<package name="QFN24">
<description>&lt;b&gt;QFN 4x4 mm&lt;/b&gt;&lt;p&gt;
Source: http://www.hittite.com/product_info/product_specs/dividersdetectors/hmc394lp4.pdf</description>
<wire x1="-1.95" y1="-1.95" x2="-1.95" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="1.95" x2="1.95" y2="1.95" width="0.2032" layer="51"/>
<wire x1="1.95" y1="1.95" x2="1.95" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="1.95" y1="-1.95" x2="-1.95" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.95" x2="-1.95" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="-1.95" x2="-1.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.6" x2="-1.95" y2="1.95" width="0.2032" layer="21"/>
<wire x1="-1.95" y1="1.95" x2="-1.6" y2="1.95" width="0.2032" layer="21"/>
<wire x1="1.6" y1="1.95" x2="1.95" y2="1.95" width="0.2032" layer="21"/>
<wire x1="1.95" y1="1.95" x2="1.95" y2="1.6" width="0.2032" layer="21"/>
<wire x1="1.95" y1="-1.6" x2="1.95" y2="-1.95" width="0.2032" layer="21"/>
<wire x1="1.95" y1="-1.95" x2="1.6" y2="-1.95" width="0.2032" layer="21"/>
<circle x="-2.44" y="1.77" radius="0.208" width="0" layer="21"/>
<smd name="1" x="-2.105" y="1.27" dx="1" dy="0.3" layer="1" roundness="25"/>
<smd name="2" x="-2.105" y="0.762" dx="1" dy="0.3" layer="1" roundness="25"/>
<smd name="3" x="-2.105" y="0.254" dx="1" dy="0.3" layer="1" roundness="25"/>
<smd name="4" x="-2.105" y="-0.254" dx="1" dy="0.3" layer="1" roundness="25"/>
<smd name="5" x="-2.105" y="-0.762" dx="1" dy="0.3" layer="1" roundness="25"/>
<smd name="6" x="-2.105" y="-1.27" dx="1" dy="0.3" layer="1" roundness="25"/>
<smd name="7" x="-1.27" y="-2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R90"/>
<smd name="8" x="-0.762" y="-2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R90"/>
<smd name="9" x="-0.254" y="-2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R90"/>
<smd name="10" x="0.254" y="-2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R90"/>
<smd name="11" x="0.762" y="-2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R90"/>
<smd name="12" x="1.27" y="-2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R90"/>
<smd name="13" x="2.105" y="-1.27" dx="1" dy="0.3" layer="1" roundness="25" rot="R180"/>
<smd name="14" x="2.105" y="-0.762" dx="1" dy="0.3" layer="1" roundness="25" rot="R180"/>
<smd name="15" x="2.105" y="-0.254" dx="1" dy="0.3" layer="1" roundness="25" rot="R180"/>
<smd name="16" x="2.105" y="0.254" dx="1" dy="0.3" layer="1" roundness="25" rot="R180"/>
<smd name="17" x="2.105" y="0.762" dx="1" dy="0.3" layer="1" roundness="25" rot="R180"/>
<smd name="18" x="2.105" y="1.27" dx="1" dy="0.3" layer="1" roundness="25" rot="R180"/>
<smd name="19" x="1.27" y="2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R270"/>
<smd name="20" x="0.762" y="2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R270"/>
<smd name="21" x="0.254" y="2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R270"/>
<smd name="22" x="-0.254" y="2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R270"/>
<smd name="23" x="-0.762" y="2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R270"/>
<smd name="24" x="-1.27" y="2.105" dx="1" dy="0.3" layer="1" roundness="25" rot="R270"/>
<smd name="EXP" x="0" y="0" dx="2.794" dy="2.794" layer="1" roundness="25"/>
<text x="-2.286" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.032" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<pad name="THRU" x="0" y="0" drill="0.8" shape="square"/>
</package>
<package name="SOT95P270X145-5N">
<smd name="1" x="-1.3716" y="0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="2" x="-1.3716" y="0" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="3" x="-1.3716" y="-0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="4" x="1.3716" y="-0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="5" x="1.3716" y="0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<wire x1="2.286" y1="-1.8034" x2="-2.286" y2="-1.8034" width="0.1524" layer="39"/>
<wire x1="-2.286" y1="-1.8034" x2="-2.286" y2="1.8034" width="0.1524" layer="39"/>
<wire x1="-2.286" y1="1.8034" x2="2.286" y2="1.8034" width="0.1524" layer="39"/>
<wire x1="2.286" y1="1.8034" x2="2.286" y2="-1.8034" width="0.1524" layer="39"/>
<wire x1="-0.635" y1="-1.5494" x2="0.635" y2="-1.5494" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.3302" x2="0.889" y2="0.3302" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.5494" x2="0.3048" y2="1.5494" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="1.5494" x2="-0.635" y2="1.5494" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.889" y1="-1.5494" x2="0.889" y2="-1.5494" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.5494" x2="0.889" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.1938" x2="0.889" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.6858" x2="0.889" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="0.889" y1="1.5494" x2="0.3048" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="1.5494" x2="-0.889" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="1.5494" x2="-0.889" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="1.1938" x2="-0.889" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.6858" x2="-0.889" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.254" x2="-0.889" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-0.254" x2="-0.889" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="1.1938" x2="-1.6002" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="1.1938" x2="-1.6002" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="0.6858" x2="-0.889" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.254" x2="-1.6002" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="0.254" x2="-1.6002" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-0.254" x2="-0.889" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.5494" x2="-0.889" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.1938" x2="-0.889" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-0.6858" x2="-1.6002" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-0.6858" x2="-1.6002" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-1.1938" x2="-0.889" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.1938" x2="1.6002" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="-1.1938" x2="1.6002" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="-0.6858" x2="0.889" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="0.889" y1="1.5494" x2="0.889" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="0.889" y1="1.1938" x2="0.889" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.6858" x2="1.6002" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="0.6858" x2="1.6002" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="1.1938" x2="0.889" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0.1524" layer="51" curve="-180"/>
<text x="-4.1656" y="2.159" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.5626" y="-4.2418" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<circle x="-2.7" y="1" radius="0.1" width="0.4064" layer="21"/>
</package>
<package name="W237-132">
<description>&lt;b&gt;WAGO SREW CLAMP&lt;/b&gt;</description>
<wire x1="-3.5306" y1="-1.651" x2="-1.524" y2="0.3556" width="0.254" layer="51"/>
<wire x1="1.6256" y1="-1.6764" x2="3.5306" y2="0.3556" width="0.254" layer="51"/>
<wire x1="-5.08" y1="4.191" x2="5.08" y2="4.191" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-4.826" x2="5.08" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-4.826" x2="-5.08" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.191" x2="-5.08" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.413" x2="-3.429" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.413" x2="-1.651" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-2.413" x2="1.651" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-2.413" x2="5.08" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.413" x2="-5.08" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-2.413" x2="5.08" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.191" x2="-5.08" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="5.08" y2="4.445" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.191" x2="5.08" y2="4.445" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-2.413" x2="3.429" y2="-2.413" width="0.1524" layer="51"/>
<circle x="-2.54" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-2.54" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<circle x="2.54" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="2.54" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<text x="-4.445" y="1.27" size="1.27" layer="51" ratio="10">1</text>
<text x="0.6858" y="1.2192" size="1.27" layer="51" ratio="10">2</text>
<text x="-3.81" y="-6.985" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.81" y="-4.318" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="W237-3E">
<description>&lt;b&gt;WAGO SREW CLAMP&lt;/b&gt;</description>
<wire x1="-6.0706" y1="-1.651" x2="-4.064" y2="0.3556" width="0.254" layer="51"/>
<wire x1="-0.9144" y1="-1.6764" x2="0.9906" y2="0.3556" width="0.254" layer="51"/>
<wire x1="4.0894" y1="-1.651" x2="6.096" y2="0.3556" width="0.254" layer="51"/>
<wire x1="-7.62" y1="-4.826" x2="7.62" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-4.826" x2="-7.62" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-2.413" x2="-7.62" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-2.413" x2="-6.096" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-2.413" x2="-4.064" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="-2.413" x2="-1.016" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-2.413" x2="4.064" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.413" x2="7.62" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.191" x2="7.62" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.191" x2="-7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="7.62" y1="4.445" x2="7.62" y2="4.191" width="0.1524" layer="21"/>
<wire x1="7.62" y1="4.445" x2="-7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-2.413" x2="7.62" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="7.62" y1="4.191" x2="7.62" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-2.413" x2="6.096" y2="-2.413" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="-2.413" x2="1.016" y2="-2.413" width="0.1524" layer="51"/>
<circle x="-5.08" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-5.08" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<circle x="0" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="0" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<circle x="5.08" y="-0.635" radius="1.4986" width="0.1524" layer="51"/>
<circle x="5.08" y="2.8448" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="0" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="-0.635" drill="1.1938" shape="long" rot="R90"/>
<text x="-6.985" y="1.27" size="1.27" layer="51" ratio="10">1</text>
<text x="-1.8542" y="1.2192" size="1.27" layer="51" ratio="10">2</text>
<text x="-6.35" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.35" y="-6.858" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="1.27" size="1.27" layer="51" ratio="10">3</text>
</package>
<package name="0805">
<wire x1="-0.3556" y1="0.4572" x2="0.3556" y2="0.4572" width="0.127" layer="51"/>
<wire x1="0.3556" y1="-0.4572" x2="-0.3556" y2="-0.4572" width="0.127" layer="51"/>
<wire x1="0.3556" y1="0.4572" x2="0.3556" y2="-0.4572" width="0.127" layer="51"/>
<wire x1="-0.3556" y1="0.4572" x2="-0.3556" y2="-0.4572" width="0.127" layer="51"/>
<wire x1="-1.7463" y1="0.2381" x2="-1.7463" y2="0.9526" width="0.127" layer="21"/>
<wire x1="-1.7463" y1="0.9526" x2="1.7462" y2="0.9525" width="0.127" layer="21"/>
<wire x1="1.7462" y1="0.9525" x2="1.7462" y2="0.2381" width="0.127" layer="21"/>
<wire x1="1.7463" y1="-0.2381" x2="1.7463" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="1.7463" y1="-0.9525" x2="-1.7463" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="-1.7463" y1="-0.9525" x2="-1.7463" y2="-0.2381" width="0.127" layer="21"/>
<smd name="2" x="0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<smd name="1" x="-0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<text x="-1.7621" y="1.1067" size="0.762" layer="27" ratio="12">&gt;VALUE</text>
<text x="-1.5621" y="-0.3047" size="0.762" layer="25" ratio="12">&gt;NAME</text>
<rectangle x1="0.4064" y1="-0.6096" x2="0.9144" y2="0.6096" layer="51"/>
<rectangle x1="-0.9144" y1="-0.6096" x2="-0.4064" y2="0.6096" layer="51"/>
</package>
<package name="LMJ1998824110DL1T39J">
<description>http://www.tme.eu/cz/Document/15bb7e3635e181d90305ca3af3a29105/rjtrafol.PDF</description>
<wire x1="-7.527" y1="10.819" x2="7.527" y2="10.819" width="0.2032" layer="21"/>
<wire x1="8.4455" y1="-8.0518" x2="-8.4455" y2="-8.0518" width="0.01" layer="51"/>
<wire x1="7.527" y1="-10.322" x2="-7.527" y2="-10.322" width="0.2032" layer="51"/>
<wire x1="-7.527" y1="-10.322" x2="-7.527" y2="10.819" width="0.2032" layer="51"/>
<wire x1="7.527" y1="10.819" x2="7.527" y2="-10.322" width="0.2032" layer="51"/>
<pad name="4" x="-0.635" y="8.89" drill="0.9" diameter="1.4"/>
<pad name="3" x="-1.905" y="6.35" drill="0.9" diameter="1.4"/>
<pad name="2" x="-3.175" y="8.89" drill="0.9" diameter="1.4"/>
<pad name="5" x="0.635" y="6.35" drill="0.9" diameter="1.4"/>
<pad name="1" x="-4.445" y="6.35" drill="0.9" diameter="1.4" shape="square"/>
<pad name="6" x="1.905" y="8.89" drill="0.9" diameter="1.4"/>
<pad name="7" x="3.175" y="6.35" drill="0.9" diameter="1.4"/>
<pad name="8" x="4.445" y="8.89" drill="0.9" diameter="1.4"/>
<text x="-5.715" y="12.065" size="1.778" layer="25">&gt;NAME</text>
<text x="-5.715" y="2.54" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="7.6" y1="-8.025" x2="8.875" y2="-6.882" layer="51"/>
<rectangle x1="-8.875" y1="-8.025" x2="-7.625" y2="-6.882" layer="51"/>
<hole x="-5.715" y="0" drill="3.2"/>
<hole x="5.715" y="0" drill="3.2"/>
<pad name="SHIELD1" x="-8" y="3.05" drill="1.6"/>
<pad name="SHIELD2" x="8" y="3.05" drill="1.6"/>
<pad name="A1" x="-6.63" y="-4.06" drill="1"/>
<pad name="K2" x="6.63" y="-4.06" drill="1"/>
<pad name="K1" x="-4.09" y="-4.06" drill="1"/>
<pad name="A2" x="4.09" y="-4.06" drill="1"/>
<wire x1="-7.6" y1="1.7" x2="-7.6" y2="-5.4" width="0.127" layer="21"/>
<wire x1="-7.6" y1="-5.4" x2="-7.6" y2="-7.9" width="0.127" layer="21"/>
<wire x1="-7.6" y1="4.5" x2="-7.6" y2="10.8" width="0.127" layer="21"/>
<wire x1="7.6" y1="-7.9" x2="7.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="7.6" y1="1.5" x2="7.6" y2="1.6" width="0.127" layer="21"/>
<wire x1="7.6" y1="4.5" x2="7.6" y2="10.8" width="0.127" layer="21"/>
<wire x1="7.5" y1="10.8" x2="7.6" y2="10.8" width="0.127" layer="21"/>
</package>
<package name="SOT230P700X180-4N">
<smd name="1" x="-3.2004" y="2.3114" dx="1.6256" dy="0.889" layer="1"/>
<smd name="2" x="-3.2004" y="0" dx="1.6256" dy="0.889" layer="1"/>
<smd name="3" x="-3.2004" y="-2.3114" dx="1.6256" dy="0.889" layer="1"/>
<smd name="4" x="3.2004" y="0" dx="1.6256" dy="3.175" layer="1"/>
<wire x1="-1.8542" y1="1.8796" x2="-1.8542" y2="2.7432" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="2.7432" x2="-3.6576" y2="2.7432" width="0.1524" layer="51"/>
<wire x1="-3.6576" y1="2.7432" x2="-3.6576" y2="1.8796" width="0.1524" layer="51"/>
<wire x1="-3.6576" y1="1.8796" x2="-1.8542" y2="1.8796" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="-0.4318" x2="-1.8542" y2="0.4318" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="0.4318" x2="-3.6576" y2="0.4318" width="0.1524" layer="51"/>
<wire x1="-3.6576" y1="0.4318" x2="-3.6576" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="-3.6576" y1="-0.4318" x2="-1.8542" y2="-0.4318" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="-2.7432" x2="-1.8542" y2="-1.8796" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="-1.8796" x2="-3.6576" y2="-1.8796" width="0.1524" layer="51"/>
<wire x1="-3.6576" y1="-1.8796" x2="-3.6576" y2="-2.7432" width="0.1524" layer="51"/>
<wire x1="-3.6576" y1="-2.7432" x2="-1.8542" y2="-2.7432" width="0.1524" layer="51"/>
<wire x1="1.8542" y1="1.5748" x2="1.8542" y2="-1.5748" width="0.1524" layer="51"/>
<wire x1="1.8542" y1="-1.5748" x2="3.6576" y2="-1.5748" width="0.1524" layer="51"/>
<wire x1="3.6576" y1="-1.5748" x2="3.6576" y2="1.6002" width="0.1524" layer="51"/>
<wire x1="3.6576" y1="1.6002" x2="1.8542" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="-3.3528" x2="1.8542" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="1.8542" y1="-3.3528" x2="1.8542" y2="3.3528" width="0.1524" layer="51"/>
<wire x1="1.8542" y1="3.3528" x2="-1.8542" y2="3.3528" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="3.3528" x2="-1.8542" y2="-3.3528" width="0.1524" layer="51"/>
<wire x1="-1.8542" y1="-3.3528" x2="1.8542" y2="-3.3528" width="0.1524" layer="21"/>
<wire x1="1.8542" y1="-3.3528" x2="1.8542" y2="3.3528" width="0.1524" layer="21"/>
<wire x1="1.8542" y1="3.3528" x2="-1.8542" y2="3.3528" width="0.1524" layer="21"/>
<wire x1="-1.8542" y1="3.3528" x2="-1.8542" y2="-3.3528" width="0.1524" layer="21"/>
<text x="-4.8006" y="3.556" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.715" y="-5.8674" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<circle x="-4.6" y="2.3" radius="0.1" width="0.4064" layer="21"/>
</package>
<package name="32005-201">
<description>&lt;b&gt;MINI USB-B R/A SMT W/ REAR&lt;/b&gt;&lt;p&gt;
Source: http://www.cypressindustries.com/pdf/32005-201.pdf</description>
<wire x1="-5.9182" y1="3.8416" x2="-3.6879" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="3.8416" x2="-3.6879" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="4.8799" x2="-3.3245" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.8799" x2="-3.3245" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.4646" x2="-2.7015" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.4646" x2="-2.7015" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.8799" x2="-2.3093" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="4.8799" x2="-2.3093" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-1.5825" y1="3.8416" x2="0.7266" y2="3.8416" width="0.1016" layer="21"/>
<wire x1="2.8032" y1="3.8416" x2="0.7266" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="0.7266" y1="3.8416" x2="0.519" y2="4.0492" width="0.1016" layer="21" curve="-90"/>
<wire x1="0.519" y1="4.0492" x2="0.519" y2="4.205" width="0.1016" layer="21"/>
<wire x1="0.519" y1="4.205" x2="2.907" y2="4.205" width="0.1016" layer="51"/>
<wire x1="2.907" y1="4.205" x2="3.4781" y2="3.6339" width="0.1016" layer="51" curve="-90"/>
<wire x1="-5.9182" y1="-3.8415" x2="-5.9182" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-5.9182" y2="3.8416" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.9591" x2="-4.5685" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="2.7514" x2="-4.828" y2="2.5438" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.828" y1="2.5438" x2="-4.828" y2="1.9727" width="0.1016" layer="21" curve="34.099487"/>
<wire x1="-4.828" y1="1.9727" x2="-4.5685" y2="1.7651" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.5685" y1="1.7651" x2="-1.8171" y2="1.5055" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.5055" x2="-1.8171" y2="1.7132" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.7132" x2="-4.2051" y2="1.9727" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="1.9727" x2="-4.2051" y2="2.4919" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="2.4919" x2="-1.8171" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.7514" x2="-1.8171" y2="2.9591" width="0.1016" layer="21"/>
<wire x1="2.8032" y1="3.8416" x2="3.0627" y2="3.5821" width="0.1016" layer="51" curve="-90"/>
<wire x1="3.0627" y1="3.5821" x2="3.0627" y2="3.011" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="3.011" x2="3.4261" y2="3.011" width="0.1016" layer="21"/>
<wire x1="1.713" y1="4.2569" x2="1.713" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="1.713" y1="4.8799" x2="2.1283" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.8799" x2="2.1283" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.4646" x2="2.6474" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.4646" x2="2.6474" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.8799" x2="3.0627" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="4.8799" x2="3.0627" y2="4.2569" width="0.1016" layer="51"/>
<wire x1="0.5709" y1="1.7651" x2="0.5709" y2="-1.765" width="0.1016" layer="21"/>
<wire x1="1.0381" y1="-1.8169" x2="1.0381" y2="1.817" width="0.1016" layer="21"/>
<wire x1="1.0381" y1="1.817" x2="0.8305" y2="2.0246" width="0.1016" layer="21" curve="90.055225"/>
<wire x1="0.8305" y1="2.0246" x2="0.8304" y2="2.0246" width="0.1016" layer="21"/>
<wire x1="0.8304" y1="2.0246" x2="0.5709" y2="1.7651" width="0.1016" layer="21" curve="89.955858"/>
<wire x1="1.5573" y1="-2.0246" x2="3.4261" y2="-2.0246" width="0.1016" layer="21"/>
<wire x1="3.0627" y1="-1.9726" x2="3.0627" y2="1.9727" width="0.1016" layer="51"/>
<wire x1="-4.5684" y1="1.2459" x2="-0.5192" y2="1.0383" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="1.0383" x2="-0.3116" y2="0.8306" width="0.1016" layer="21" curve="-83.771817"/>
<wire x1="-4.5685" y1="1.2459" x2="-4.7761" y2="1.0383" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.7761" y1="1.0383" x2="-4.7761" y2="1.0382" width="0.1016" layer="21"/>
<wire x1="-4.7761" y1="1.0382" x2="-4.5685" y2="0.8306" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.5685" y1="0.8306" x2="-1.1422" y2="0.623" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-3.6879" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-3.8414" x2="-3.6879" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-4.8797" x2="-3.3245" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.8797" x2="-3.3245" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.4644" x2="-2.7015" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.4644" x2="-2.7015" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.8797" x2="-2.3093" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="-4.8797" x2="-2.3093" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="-3.8414" x2="2.8032" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="0.7266" y1="-3.8414" x2="0.519" y2="-4.049" width="0.1016" layer="21" curve="90"/>
<wire x1="0.519" y1="-4.049" x2="0.519" y2="-4.2048" width="0.1016" layer="21"/>
<wire x1="0.519" y1="-4.2048" x2="2.907" y2="-4.2048" width="0.1016" layer="51"/>
<wire x1="2.907" y1="-4.2048" x2="3.4781" y2="-3.6337" width="0.1016" layer="51" curve="90.020069"/>
<wire x1="-1.8171" y1="-2.9589" x2="-4.5685" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-2.7512" x2="-4.828" y2="-2.5436" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.828" y1="-2.5436" x2="-4.828" y2="-1.9725" width="0.1016" layer="21" curve="-34.099487"/>
<wire x1="-4.828" y1="-1.9725" x2="-4.5685" y2="-1.7649" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.5685" y1="-1.7649" x2="-1.8171" y2="-1.5053" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.5053" x2="-1.8171" y2="-1.713" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.713" x2="-4.2051" y2="-1.9725" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-1.9725" x2="-4.2051" y2="-2.4917" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-2.4917" x2="-1.8171" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-2.7512" x2="-1.8171" y2="-2.9589" width="0.1016" layer="21"/>
<wire x1="2.8032" y1="-3.8414" x2="3.0627" y2="-3.5819" width="0.1016" layer="51" curve="90.044176"/>
<wire x1="3.0627" y1="-3.5819" x2="3.0627" y2="-3.0108" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="-3.0108" x2="3.4261" y2="-3.0108" width="0.1016" layer="21"/>
<wire x1="1.713" y1="-4.2567" x2="1.713" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="1.713" y1="-4.8797" x2="2.1283" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.8797" x2="2.1283" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.4644" x2="2.6474" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.4644" x2="2.6474" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.8797" x2="3.0627" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="3.0627" y1="-4.8797" x2="3.0627" y2="-4.2567" width="0.1016" layer="51"/>
<wire x1="1.0381" y1="-1.8168" x2="0.8305" y2="-2.0244" width="0.1016" layer="21" curve="-90.055225"/>
<wire x1="0.8304" y1="-2.0244" x2="0.5709" y2="-1.7649" width="0.1016" layer="21" curve="-89.867677"/>
<wire x1="1.5573" y1="-1.9725" x2="1.5573" y2="2.0248" width="0.1016" layer="51"/>
<wire x1="1.5573" y1="2.0248" x2="3.4261" y2="2.0248" width="0.1016" layer="21"/>
<wire x1="-4.5684" y1="-1.2457" x2="-0.5192" y2="-1.0381" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="-1.0381" x2="-0.3116" y2="-0.8304" width="0.1016" layer="21" curve="83.722654"/>
<wire x1="-0.3116" y1="-0.8304" x2="-0.3116" y2="0.8307" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-1.2457" x2="-4.7761" y2="-1.0381" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.7761" y1="-1.038" x2="-4.5685" y2="-0.8304" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.5685" y1="-0.8304" x2="-1.1422" y2="-0.6228" width="0.1016" layer="21"/>
<wire x1="-1.1422" y1="-0.6228" x2="-1.1422" y2="0.6232" width="0.1016" layer="21"/>
<wire x1="-1.5826" y1="-3.8414" x2="0.7267" y2="-3.8415" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-4.4146" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="3.8416" x2="-4.4147" y2="3.8415" width="0.1016" layer="21"/>
<wire x1="-2.3093" y1="3.8416" x2="0.7265" y2="3.8415" width="0.1016" layer="51"/>
<wire x1="3.4781" y1="-2.0245" x2="3.4781" y2="-3.0109" width="0.1016" layer="21"/>
<wire x1="3.4781" y1="3.634" x2="3.478" y2="-3.0109" width="0.1016" layer="51"/>
<wire x1="3.4782" y1="3.011" x2="3.4782" y2="2.0246" width="0.1016" layer="21"/>
<smd name="M1" x="-3" y="-4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M2" x="-3" y="4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M4" x="2.9" y="-4.45" dx="3.3" dy="2" layer="1"/>
<smd name="M3" x="2.9" y="4.45" dx="3.3" dy="2" layer="1"/>
<smd name="1" x="3" y="1.6" dx="3.1" dy="0.5" layer="1"/>
<smd name="2" x="3" y="0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="3" x="3" y="0" dx="3.1" dy="0.5" layer="1"/>
<smd name="4" x="3" y="-0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="5" x="3" y="-1.6" dx="3.1" dy="0.5" layer="1"/>
<text x="-4.445" y="5.715" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="-6.985" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="2.2" drill="0.9"/>
<hole x="0" y="-2.2" drill="0.9"/>
</package>
<package name="32005-301">
<description>&lt;b&gt;MINI USB-B R/A SMT W/O REAR&lt;/b&gt;&lt;p&gt;
Source: http://www.cypressindustries.com/pdf/32005-301.pdf</description>
<wire x1="-5.9228" y1="3.8473" x2="3.1598" y2="3.8473" width="0.1016" layer="51"/>
<wire x1="2.9404" y1="3.7967" x2="2.9404" y2="2.5986" width="0.1016" layer="51"/>
<wire x1="2.9404" y1="2.5986" x2="1.8098" y2="2.5986" width="0.1016" layer="21"/>
<wire x1="1.8098" y1="3.7798" x2="1.8098" y2="-3.8473" width="0.1016" layer="51"/>
<wire x1="3.1597" y1="-3.8473" x2="-5.9228" y2="-3.8473" width="0.1016" layer="51"/>
<wire x1="-5.9228" y1="-3.8473" x2="-5.9228" y2="3.8473" width="0.1016" layer="21"/>
<wire x1="2.9573" y1="-3.8217" x2="2.9573" y2="-2.6998" width="0.1016" layer="51"/>
<wire x1="2.9573" y1="-2.6998" x2="1.8098" y2="-2.6998" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="3.8416" x2="-3.6879" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="3.8416" x2="-3.6879" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="4.8799" x2="-3.3245" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.8799" x2="-3.3245" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="4.4646" x2="-2.7015" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.4646" x2="-2.7015" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="4.8799" x2="-2.3093" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="4.8799" x2="-2.3093" y2="3.8416" width="0.1016" layer="51"/>
<wire x1="-5.9182" y1="-3.8415" x2="-5.9182" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-5.9182" y2="3.8416" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.9591" x2="-4.5685" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="2.7514" x2="-4.828" y2="2.5438" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.828" y1="2.5438" x2="-4.828" y2="1.9727" width="0.1016" layer="21" curve="34.099487"/>
<wire x1="-4.828" y1="1.9727" x2="-4.5685" y2="1.7651" width="0.1016" layer="21" curve="68.629849"/>
<wire x1="-4.5685" y1="1.7651" x2="-1.8171" y2="1.5055" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.5055" x2="-1.8171" y2="1.7132" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="1.7132" x2="-4.2051" y2="1.9727" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="1.9727" x2="-4.2051" y2="2.4919" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="2.4919" x2="-1.8171" y2="2.7514" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="2.7514" x2="-1.8171" y2="2.9591" width="0.1016" layer="21"/>
<wire x1="1.713" y1="3.8856" x2="1.713" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="1.713" y1="4.8799" x2="2.1283" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.8799" x2="2.1283" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="4.4646" x2="2.6474" y2="4.4646" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.4646" x2="2.6474" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="4.8799" x2="3.1639" y2="4.8799" width="0.1016" layer="51"/>
<wire x1="3.1639" y1="4.8799" x2="3.1639" y2="3.8519" width="0.1016" layer="51"/>
<wire x1="-4.5684" y1="1.2459" x2="-0.5192" y2="1.0383" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="1.0383" x2="-0.3116" y2="0.8306" width="0.1016" layer="21" curve="-83.771817"/>
<wire x1="-4.5685" y1="1.2459" x2="-4.7761" y2="1.0383" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.7761" y1="1.0383" x2="-4.7761" y2="1.0382" width="0.1016" layer="21"/>
<wire x1="-4.7761" y1="1.0382" x2="-4.5685" y2="0.8306" width="0.1016" layer="21" curve="90"/>
<wire x1="-4.5685" y1="0.8306" x2="-1.1422" y2="0.623" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-3.6879" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-3.8414" x2="-3.6879" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.6879" y1="-4.8797" x2="-3.3245" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.8797" x2="-3.3245" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-3.3245" y1="-4.4644" x2="-2.7015" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.4644" x2="-2.7015" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.7015" y1="-4.8797" x2="-2.3093" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="-2.3093" y1="-4.8797" x2="-2.3093" y2="-3.8414" width="0.1016" layer="51"/>
<wire x1="-1.8171" y1="-2.9589" x2="-4.5685" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-2.7512" x2="-4.828" y2="-2.5436" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.828" y1="-2.5436" x2="-4.828" y2="-1.9725" width="0.1016" layer="21" curve="-34.099487"/>
<wire x1="-4.828" y1="-1.9725" x2="-4.5685" y2="-1.7649" width="0.1016" layer="21" curve="-68.629849"/>
<wire x1="-4.5685" y1="-1.7649" x2="-1.8171" y2="-1.5053" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.5053" x2="-1.8171" y2="-1.713" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-1.713" x2="-4.2051" y2="-1.9725" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-1.9725" x2="-4.2051" y2="-2.4917" width="0.1016" layer="21"/>
<wire x1="-4.2051" y1="-2.4917" x2="-1.8171" y2="-2.7512" width="0.1016" layer="21"/>
<wire x1="-1.8171" y1="-2.7512" x2="-1.8171" y2="-2.9589" width="0.1016" layer="21"/>
<wire x1="1.713" y1="-3.8855" x2="1.713" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="1.713" y1="-4.8797" x2="2.1283" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.8797" x2="2.1283" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.1283" y1="-4.4644" x2="2.6474" y2="-4.4644" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.4644" x2="2.6474" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="2.6474" y1="-4.8797" x2="3.1627" y2="-4.8797" width="0.1016" layer="51"/>
<wire x1="3.1627" y1="-4.8797" x2="3.1627" y2="-3.8518" width="0.1016" layer="51"/>
<wire x1="-4.5684" y1="-1.2457" x2="-0.5192" y2="-1.0381" width="0.1016" layer="21"/>
<wire x1="-0.5192" y1="-1.0381" x2="-0.3116" y2="-0.8304" width="0.1016" layer="21" curve="83.722654"/>
<wire x1="-0.3116" y1="-0.8304" x2="-0.3116" y2="0.8307" width="0.1016" layer="21"/>
<wire x1="-4.5685" y1="-1.2457" x2="-4.7761" y2="-1.0381" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.7761" y1="-1.038" x2="-4.5685" y2="-0.8304" width="0.1016" layer="21" curve="-90"/>
<wire x1="-4.5685" y1="-0.8304" x2="-1.1422" y2="-0.6228" width="0.1016" layer="21"/>
<wire x1="-1.1422" y1="-0.6228" x2="-1.1422" y2="0.6232" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="-3.8414" x2="-4.4146" y2="-3.8414" width="0.1016" layer="21"/>
<wire x1="-5.9182" y1="3.8416" x2="-4.4147" y2="3.8415" width="0.1016" layer="21"/>
<wire x1="1.0842" y1="-3.8472" x2="-1.6031" y2="-3.8472" width="0.1016" layer="21"/>
<wire x1="-1.5523" y1="3.8472" x2="0.9831" y2="3.8473" width="0.1016" layer="21"/>
<wire x1="2.9404" y1="3.3243" x2="2.9404" y2="2.5986" width="0.1016" layer="21"/>
<wire x1="1.8098" y1="2.5986" x2="1.8099" y2="3.3243" width="0.1016" layer="21"/>
<wire x1="1.8098" y1="-2.6999" x2="1.8098" y2="-3.3242" width="0.1016" layer="21"/>
<wire x1="2.9573" y1="-3.3324" x2="2.9573" y2="-2.6998" width="0.1016" layer="21"/>
<smd name="M1" x="-3" y="-4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M2" x="-3" y="4.45" dx="2.5" dy="2" layer="1"/>
<smd name="M4" x="2.9" y="-4.45" dx="3.3" dy="2" layer="1"/>
<smd name="M3" x="2.9" y="4.45" dx="3.3" dy="2" layer="1"/>
<smd name="1" x="3" y="1.6" dx="3.1" dy="0.5" layer="1"/>
<smd name="2" x="3" y="0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="3" x="3" y="0" dx="3.1" dy="0.5" layer="1"/>
<smd name="4" x="3" y="-0.8" dx="3.1" dy="0.5" layer="1"/>
<smd name="5" x="3" y="-1.6" dx="3.1" dy="0.5" layer="1"/>
<text x="-4.445" y="5.715" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="-6.985" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="2.2" drill="0.9"/>
<hole x="0" y="-2.2" drill="0.9"/>
</package>
<package name="JACK-PMS">
<wire x1="-5" y1="7.19" x2="-0.5" y2="7.19" width="0.127" layer="21"/>
<wire x1="-0.5" y1="7.19" x2="4" y2="7.19" width="0.127" layer="21"/>
<wire x1="4" y1="-3.81" x2="4" y2="-3.8" width="0.127" layer="21"/>
<wire x1="4" y1="-3.8" x2="4" y2="-3.7" width="0.127" layer="21"/>
<wire x1="4" y1="-3.7" x2="4" y2="7.19" width="0.127" layer="21"/>
<wire x1="-5" y1="7.19" x2="-5" y2="9.69" width="0.127" layer="21"/>
<wire x1="-5" y1="9.69" x2="4" y2="9.69" width="0.127" layer="21"/>
<wire x1="4" y1="9.69" x2="4" y2="7.19" width="0.127" layer="21"/>
<wire x1="-5" y1="-3.81" x2="-0.5" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-3.81" x2="4" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-3.81" x2="-0.5" y2="7.19" width="0.127" layer="51"/>
<wire x1="-5" y1="7.19" x2="-5" y2="-3.81" width="0.127" layer="51"/>
<pad name="3" x="-0.5" y="2.8093" drill="3.2004" diameter="4.9"/>
<pad name="1" x="-0.5" y="-3.31" drill="3.2004" diameter="4.9"/>
<pad name="2" x="-5" y="-0.31" drill="3.2004" diameter="4.9"/>
<text x="2.25" y="-3.31" size="1.27" layer="21">1</text>
<text x="-4.5" y="2.69" size="1.27" layer="21">2</text>
<text x="1.25" y="5.19" size="1.27" layer="21">3</text>
<text x="3.81" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-2.6" y="7.7" size="0.762" layer="27" ratio="12">&gt;value</text>
<wire x1="-5" y1="9.7" x2="-5" y2="2.4" width="0.127" layer="21"/>
<wire x1="-5" y1="-3" x2="-5" y2="-3.8" width="0.127" layer="21"/>
<wire x1="-5" y1="-3.8" x2="-3.1" y2="-3.8" width="0.127" layer="21"/>
<wire x1="2.1" y1="-3.8" x2="4" y2="-3.8" width="0.127" layer="21"/>
<wire x1="4" y1="-3.8" x2="4" y2="-3.7" width="0.127" layer="21"/>
<wire x1="-0.5" y1="7.2" x2="-0.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="0.1" x2="-0.5" y2="-0.6" width="0.127" layer="21"/>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="WIFI-LPT100">
<description>http://www.hi-flying.com/products_detail/&amp;productId=d7fa2397-c1ee-4e4f-89c3-600a864861de&amp;comp_stats=comp-FrontProducts_list01-111.html</description>
<pad name="1" x="8.9" y="5.6" drill="0.8" shape="square" rot="R180"/>
<pad name="2" x="6.9" y="5.6" drill="0.8" rot="R180"/>
<pad name="3" x="4.9" y="5.6" drill="0.8" rot="R180"/>
<pad name="4" x="2.9" y="5.6" drill="0.8" rot="R180"/>
<pad name="5" x="0.9" y="5.6" drill="0.8" rot="R180"/>
<pad name="6" x="-1.1" y="5.6" drill="0.8" rot="R180"/>
<pad name="7" x="-3.1" y="5.6" drill="0.8" rot="R180"/>
<pad name="8" x="-5.1" y="5.6" drill="0.8" rot="R180"/>
<pad name="9" x="-7.1" y="5.6" drill="0.8" rot="R180"/>
<pad name="10" x="-9.1" y="5.6" drill="0.8" rot="R180"/>
<wire x1="10.8" y1="6.9" x2="-11.2" y2="6.9" width="0.127" layer="22"/>
<wire x1="-11.2" y1="6.9" x2="-11.2" y2="-6.6" width="0.127" layer="22"/>
<wire x1="-11.2" y1="-6.6" x2="10.8" y2="-6.6" width="0.127" layer="22"/>
<wire x1="10.8" y1="-6.6" x2="10.8" y2="6.9" width="0.127" layer="22"/>
<text x="-9.9" y="-5.6" size="1.27" layer="21" ratio="10">ANT</text>
<text x="-9.6" y="2.2" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-9.6" y="0" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="1X04">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.1562" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="XTAL-1">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-1.5875" y="2.286" size="1.524" layer="95">&gt;NAME</text>
<text x="-3.4925" y="-3.81" size="1.524" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="MCP2551-I/SN">
<pin name="VDD" x="-17.78" y="-10.16" length="middle" direction="pwr"/>
<pin name="TXD" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="RS" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="VSS" x="-17.78" y="-17.78" length="middle" direction="pas"/>
<pin name="RXD" x="-17.78" y="5.08" length="middle" direction="out"/>
<pin name="VREF" x="17.78" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="CANL" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="CANH" x="17.78" y="0" length="middle" rot="R180"/>
<wire x1="-12.7" y1="7.62" x2="-12.7" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-20.32" x2="12.7" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-20.32" x2="12.7" y2="7.62" width="0.4064" layer="94"/>
<wire x1="12.7" y1="7.62" x2="-12.7" y2="7.62" width="0.4064" layer="94"/>
<text x="-5.0292" y="10.033" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.6896" y="-24.7904" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="STM32F207VFT6_A">
<pin name="VDDA" x="-22.86" y="20.32" length="middle" direction="pwr"/>
<pin name="VDD_5" x="-22.86" y="43.18" length="middle" direction="pwr"/>
<pin name="VDD_12" x="-22.86" y="40.64" length="middle" direction="pwr"/>
<pin name="VDD_4" x="-22.86" y="38.1" length="middle" direction="pwr"/>
<pin name="VDD_1" x="-22.86" y="35.56" length="middle" direction="pwr"/>
<pin name="VBAT" x="-22.86" y="33.02" length="middle" direction="pwr"/>
<pin name="VCAP_1" x="22.86" y="-48.26" length="middle" direction="pwr" rot="R180"/>
<pin name="VREF+" x="-22.86" y="17.78" length="middle" direction="pwr"/>
<pin name="NRST" x="22.86" y="-33.02" length="middle" rot="R180"/>
<pin name="PA0-WKUP" x="-22.86" y="5.08" length="middle"/>
<pin name="PA1" x="-22.86" y="2.54" length="middle"/>
<pin name="PA2" x="-22.86" y="0" length="middle"/>
<pin name="PA3" x="-22.86" y="-2.54" length="middle"/>
<pin name="PA4" x="-22.86" y="-5.08" length="middle"/>
<pin name="PA5" x="-22.86" y="-7.62" length="middle"/>
<pin name="PA6" x="-22.86" y="-10.16" length="middle"/>
<pin name="PA7" x="-22.86" y="-12.7" length="middle"/>
<pin name="PB0" x="-22.86" y="-17.78" length="middle"/>
<pin name="PB1" x="-22.86" y="-20.32" length="middle"/>
<pin name="PB2" x="-22.86" y="-22.86" length="middle"/>
<pin name="PB10" x="-22.86" y="-25.4" length="middle"/>
<pin name="PB11" x="-22.86" y="-27.94" length="middle"/>
<pin name="PC13-RTC_AF1" x="-22.86" y="-33.02" length="middle"/>
<pin name="PC14-OSC32_IN" x="-22.86" y="-35.56" length="middle"/>
<pin name="PC15-OSC32_OUT" x="-22.86" y="-38.1" length="middle"/>
<pin name="PH0-OSC_IN" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PH1-OSC_OUT" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="VSS_5" x="-22.86" y="-43.18" length="middle" direction="pas"/>
<pin name="VSSA" x="-22.86" y="-45.72" length="middle" direction="pas"/>
<pin name="VSS_4" x="-22.86" y="-48.26" length="middle" direction="pas"/>
<pin name="PC0" x="22.86" y="40.64" length="middle" rot="R180"/>
<pin name="PC1" x="22.86" y="38.1" length="middle" rot="R180"/>
<pin name="PC2" x="22.86" y="35.56" length="middle" rot="R180"/>
<pin name="PC3" x="22.86" y="33.02" length="middle" rot="R180"/>
<pin name="PC4" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="PC5" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="PE2" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="PE3" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="PE4" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="PE5" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="PE6" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="PE7" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="PE8" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="PE9" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PE10" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="PE11" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="PE12" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="PE13" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PE14" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="PE15" x="22.86" y="-10.16" length="middle" rot="R180"/>
<wire x1="-17.78" y1="45.72" x2="-17.78" y2="-50.8" width="0.4064" layer="94"/>
<wire x1="-17.78" y1="-50.8" x2="17.78" y2="-50.8" width="0.4064" layer="94"/>
<wire x1="17.78" y1="-50.8" x2="17.78" y2="45.72" width="0.4064" layer="94"/>
<wire x1="17.78" y1="45.72" x2="-17.78" y2="45.72" width="0.4064" layer="94"/>
<text x="-5.08" y="47.4472" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-8.763" y="-54.8894" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="STM32F207VFT6_B">
<pin name="VDD_2" x="-20.32" y="43.18" length="middle" direction="pwr"/>
<pin name="VDD_3" x="-20.32" y="40.64" length="middle" direction="pwr"/>
<pin name="VCAP_2" x="20.32" y="-48.26" length="middle" direction="pwr" rot="R180"/>
<pin name="BOOT0" x="20.32" y="-20.32" length="middle" direction="in" rot="R180"/>
<pin name="RFU" x="20.32" y="-33.02" length="middle" direction="pas" rot="R180"/>
<pin name="PA_8" x="-20.32" y="22.86" length="middle"/>
<pin name="PA_9" x="-20.32" y="20.32" length="middle"/>
<pin name="PA_10" x="-20.32" y="17.78" length="middle"/>
<pin name="PA_11" x="-20.32" y="15.24" length="middle"/>
<pin name="PA_12" x="-20.32" y="12.7" length="middle"/>
<pin name="PA_13" x="-20.32" y="10.16" length="middle"/>
<pin name="PA14" x="-20.32" y="7.62" length="middle"/>
<pin name="PA15" x="-20.32" y="5.08" length="middle"/>
<pin name="PB3" x="-20.32" y="0" length="middle"/>
<pin name="PB4" x="-20.32" y="-2.54" length="middle"/>
<pin name="PB5" x="-20.32" y="-5.08" length="middle"/>
<pin name="PB6" x="-20.32" y="-7.62" length="middle"/>
<pin name="PB7" x="-20.32" y="-10.16" length="middle"/>
<pin name="PB8" x="-20.32" y="-12.7" length="middle"/>
<pin name="PB9" x="-20.32" y="-15.24" length="middle"/>
<pin name="PB12" x="-20.32" y="-17.78" length="middle"/>
<pin name="PB13" x="-20.32" y="-20.32" length="middle"/>
<pin name="PB14" x="-20.32" y="-22.86" length="middle"/>
<pin name="PB15" x="-20.32" y="-25.4" length="middle"/>
<pin name="PC6" x="-20.32" y="-30.48" length="middle"/>
<pin name="PC7" x="-20.32" y="-33.02" length="middle"/>
<pin name="PC8" x="-20.32" y="-35.56" length="middle"/>
<pin name="PC9" x="-20.32" y="-38.1" length="middle"/>
<pin name="PC10" x="-20.32" y="-40.64" length="middle"/>
<pin name="PC11" x="-20.32" y="-43.18" length="middle"/>
<pin name="PC12" x="-20.32" y="-45.72" length="middle"/>
<pin name="VSS_2" x="-20.32" y="-50.8" length="middle" direction="pas"/>
<pin name="PD0" x="20.32" y="43.18" length="middle" rot="R180"/>
<pin name="PD1" x="20.32" y="40.64" length="middle" rot="R180"/>
<pin name="PD2" x="20.32" y="38.1" length="middle" rot="R180"/>
<pin name="PD3" x="20.32" y="35.56" length="middle" rot="R180"/>
<pin name="PD4" x="20.32" y="33.02" length="middle" rot="R180"/>
<pin name="PD5" x="20.32" y="30.48" length="middle" rot="R180"/>
<pin name="PD6" x="20.32" y="27.94" length="middle" rot="R180"/>
<pin name="PD7" x="20.32" y="25.4" length="middle" rot="R180"/>
<pin name="PD8" x="20.32" y="22.86" length="middle" rot="R180"/>
<pin name="PD9" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="PD10" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="PD11" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="PD12" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="PD13" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="PD14" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="PD15" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="PE0" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="PE1" x="20.32" y="-2.54" length="middle" rot="R180"/>
<wire x1="-15.24" y1="48.26" x2="-15.24" y2="-55.88" width="0.4064" layer="94"/>
<wire x1="-15.24" y1="-55.88" x2="15.24" y2="-55.88" width="0.4064" layer="94"/>
<wire x1="15.24" y1="-55.88" x2="15.24" y2="48.26" width="0.4064" layer="94"/>
<wire x1="15.24" y1="48.26" x2="-15.24" y2="48.26" width="0.4064" layer="94"/>
<text x="-5.0546" y="50.4698" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-6.3754" y="-59.2582" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="SN75176BD">
<pin name="VCC" x="17.78" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="D" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="DE" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="~RE" x="-17.78" y="-10.16" length="middle" direction="in"/>
<pin name="GND" x="17.78" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="A" x="17.78" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="B" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="R" x="-17.78" y="2.54" length="middle" direction="out"/>
<wire x1="-12.7" y1="12.7" x2="-12.7" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="12.7" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="12.7" width="0.4064" layer="94"/>
<wire x1="12.7" y1="12.7" x2="-12.7" y2="12.7" width="0.4064" layer="94"/>
<text x="-5.8166" y="15.0114" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-4.953" y="-21.971" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="KSZ8081RNACA">
<description>MICREL SEMICONDUCTOR - KSZ8081RNACA TR - IC, TRX, PHY 10/100 PHY, 24MLF</description>
<wire x1="-20.32" y1="40.64" x2="-20.32" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="-20.32" y1="-33.02" x2="20.32" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="20.32" y1="-33.02" x2="20.32" y2="40.64" width="0.4064" layer="94"/>
<wire x1="20.32" y1="40.64" x2="-20.32" y2="40.64" width="0.4064" layer="94"/>
<text x="-21.0566" y="42.9514" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-20.193" y="-37.211" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="VDD_1.2" x="-25.4" y="-17.78" length="middle"/>
<pin name="VDDA_3.3" x="25.4" y="-17.78" length="middle" rot="R180"/>
<pin name="RXM" x="25.4" y="30.48" length="middle" rot="R180"/>
<pin name="RXP" x="25.4" y="33.02" length="middle" rot="R180"/>
<pin name="TXM" x="25.4" y="35.56" length="middle" rot="R180"/>
<pin name="TXP" x="25.4" y="38.1" length="middle" rot="R180"/>
<pin name="XO" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="XI" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="REXT" x="-25.4" y="-27.94" length="middle"/>
<pin name="MDIO" x="-25.4" y="38.1" length="middle"/>
<pin name="MDC" x="-25.4" y="35.56" length="middle"/>
<pin name="RXD1" x="-25.4" y="25.4" length="middle"/>
<pin name="RXD0" x="-25.4" y="27.94" length="middle"/>
<pin name="VDDIO" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="CRS_DV/ADD10" x="-25.4" y="30.48" length="middle"/>
<pin name="REF_CLK" x="-25.4" y="7.62" length="middle"/>
<pin name="RXER" x="-25.4" y="22.86" length="middle"/>
<pin name="INTRP" x="-25.4" y="0" length="middle"/>
<pin name="TXEN" x="-25.4" y="17.78" length="middle"/>
<pin name="TXD0" x="-25.4" y="15.24" length="middle"/>
<pin name="TXD1" x="-25.4" y="12.7" length="middle"/>
<pin name="GND" x="25.4" y="-30.48" length="middle" rot="R180"/>
<pin name="LED0/ANEN_SPEED" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="RST#" x="-25.4" y="-7.62" length="middle"/>
</symbol>
<symbol name="24AA02E48T-I/OT">
<pin name="VCC" x="12.7" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="SCL" x="-15.24" y="5.08" length="middle" direction="in"/>
<pin name="VSS" x="12.7" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="NC" x="-15.24" y="-10.16" length="middle" direction="nc"/>
<pin name="SDA" x="-15.24" y="0" length="middle"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="7.62" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="7.62" width="0.4064" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-10.16" y2="7.62" width="0.4064" layer="94"/>
<text x="-5.0292" y="8.7122" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.842" y="-15.494" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="KL">
<wire x1="1.778" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<circle x="0.889" y="0" radius="0.898" width="0.254" layer="94"/>
<text x="-1.27" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="KL+V">
<wire x1="1.778" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<circle x="0.889" y="0" radius="0.898" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.762" x2="0" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="0.762" x2="1.524" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="1.778" x2="0" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="1.524" y2="1.778" width="0.254" layer="94"/>
<text x="0.3175" y="2.2225" size="1.524" layer="95">&gt;NAME</text>
<text x="0.3175" y="-1.27" size="1.524" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="JACK8">
<wire x1="1.524" y1="8.128" x2="0" y2="8.128" width="0.254" layer="94"/>
<wire x1="0" y1="8.128" x2="0" y2="7.112" width="0.254" layer="94"/>
<wire x1="0" y1="7.112" x2="1.524" y2="7.112" width="0.254" layer="94"/>
<wire x1="1.524" y1="5.588" x2="0" y2="5.588" width="0.254" layer="94"/>
<wire x1="0" y1="5.588" x2="0" y2="4.572" width="0.254" layer="94"/>
<wire x1="0" y1="4.572" x2="1.524" y2="4.572" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.048" x2="0" y2="3.048" width="0.254" layer="94"/>
<wire x1="0" y1="3.048" x2="0" y2="2.032" width="0.254" layer="94"/>
<wire x1="0" y1="2.032" x2="1.524" y2="2.032" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="1.524" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.032" x2="0" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.524" y2="-3.048" width="0.254" layer="94"/>
<wire x1="1.524" y1="-4.572" x2="0" y2="-4.572" width="0.254" layer="94"/>
<wire x1="0" y1="-4.572" x2="0" y2="-5.588" width="0.254" layer="94"/>
<wire x1="0" y1="-5.588" x2="1.524" y2="-5.588" width="0.254" layer="94"/>
<wire x1="3.048" y1="-2.032" x2="5.588" y2="-2.032" width="0.1998" layer="94"/>
<wire x1="5.588" y1="-2.032" x2="5.588" y2="-0.254" width="0.1998" layer="94"/>
<wire x1="5.588" y1="-0.254" x2="6.604" y2="-0.254" width="0.1998" layer="94"/>
<wire x1="6.604" y1="-0.254" x2="6.604" y2="1.778" width="0.1998" layer="94"/>
<wire x1="6.604" y1="1.778" x2="5.588" y2="1.778" width="0.1998" layer="94"/>
<wire x1="5.588" y1="1.778" x2="5.588" y2="3.556" width="0.1998" layer="94"/>
<wire x1="5.588" y1="3.556" x2="3.048" y2="3.556" width="0.1998" layer="94"/>
<wire x1="3.048" y1="3.556" x2="3.048" y2="2.54" width="0.1998" layer="94"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="2.032" width="0.1998" layer="94"/>
<wire x1="3.048" y1="2.032" x2="3.048" y2="1.524" width="0.1998" layer="94"/>
<wire x1="3.048" y1="1.524" x2="3.048" y2="1.016" width="0.1998" layer="94"/>
<wire x1="3.048" y1="1.016" x2="3.048" y2="0.508" width="0.1998" layer="94"/>
<wire x1="3.048" y1="0.508" x2="3.048" y2="0" width="0.1998" layer="94"/>
<wire x1="3.048" y1="0" x2="3.048" y2="-0.508" width="0.1998" layer="94"/>
<wire x1="3.048" y1="-0.508" x2="3.048" y2="-1.016" width="0.1998" layer="94"/>
<wire x1="3.048" y1="-1.016" x2="3.048" y2="-2.032" width="0.1998" layer="94"/>
<wire x1="3.048" y1="2.54" x2="3.81" y2="2.54" width="0.1998" layer="94"/>
<wire x1="3.048" y1="2.032" x2="3.81" y2="2.032" width="0.1998" layer="94"/>
<wire x1="3.048" y1="1.524" x2="3.81" y2="1.524" width="0.1998" layer="94"/>
<wire x1="3.048" y1="1.016" x2="3.81" y2="1.016" width="0.1998" layer="94"/>
<wire x1="3.048" y1="0.508" x2="3.81" y2="0.508" width="0.1998" layer="94"/>
<wire x1="3.048" y1="0" x2="3.81" y2="0" width="0.1998" layer="94"/>
<wire x1="1.524" y1="-7.112" x2="0" y2="-7.112" width="0.254" layer="94"/>
<wire x1="0" y1="-7.112" x2="0" y2="-8.128" width="0.254" layer="94"/>
<wire x1="0" y1="-8.128" x2="1.524" y2="-8.128" width="0.254" layer="94"/>
<wire x1="1.524" y1="-9.652" x2="0" y2="-9.652" width="0.254" layer="94"/>
<wire x1="0" y1="-9.652" x2="0" y2="-10.668" width="0.254" layer="94"/>
<wire x1="0" y1="-10.668" x2="1.524" y2="-10.668" width="0.254" layer="94"/>
<wire x1="3.048" y1="-0.508" x2="3.81" y2="-0.508" width="0.1998" layer="94"/>
<wire x1="3.048" y1="-1.016" x2="3.81" y2="-1.016" width="0.1998" layer="94"/>
<text x="-2.54" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-13.208" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="4" x="-2.54" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="7" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="8" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" swaplevel="1"/>
<wire x1="2.54" y1="-26.67" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="0" y1="-25.4" x2="2.54" y2="-24.13" width="0.254" layer="94"/>
<wire x1="0" y1="-26.67" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="0" y1="-25.4" x2="0" y2="-24.13" width="0.254" layer="94"/>
<wire x1="2.54" y1="-26.67" x2="2.54" y2="-25.4" width="0.254" layer="94"/>
<wire x1="2.54" y1="-25.4" x2="2.54" y2="-24.13" width="0.254" layer="94"/>
<wire x1="2.54" y1="-25.4" x2="0" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-23.368" x2="0.381" y2="-21.971" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-21.971" x2="0.762" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-22.86" x2="1.27" y2="-22.352" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-22.352" x2="0.381" y2="-21.971" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-22.098" x2="-0.381" y2="-22.987" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="-22.987" x2="0.127" y2="-22.479" width="0.1524" layer="94"/>
<wire x1="0.127" y1="-22.479" x2="-0.762" y2="-22.098" width="0.1524" layer="94"/>
<wire x1="0.635" y1="-23.495" x2="-0.762" y2="-22.098" width="0.1524" layer="94"/>
<pin name="K" x="-2.54" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="A" x="5.08" y="-25.4" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="2.54" y1="-31.75" x2="0" y2="-30.48" width="0.254" layer="94"/>
<wire x1="0" y1="-30.48" x2="2.54" y2="-29.21" width="0.254" layer="94"/>
<wire x1="0" y1="-31.75" x2="0" y2="-30.48" width="0.254" layer="94"/>
<wire x1="0" y1="-30.48" x2="0" y2="-29.21" width="0.254" layer="94"/>
<wire x1="2.54" y1="-31.75" x2="2.54" y2="-30.48" width="0.254" layer="94"/>
<wire x1="2.54" y1="-30.48" x2="2.54" y2="-29.21" width="0.254" layer="94"/>
<wire x1="2.54" y1="-30.48" x2="0" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-28.448" x2="0.381" y2="-27.051" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-27.051" x2="0.762" y2="-27.94" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-27.94" x2="1.27" y2="-27.432" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-27.432" x2="0.381" y2="-27.051" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-27.178" x2="-0.381" y2="-28.067" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="-28.067" x2="0.127" y2="-27.559" width="0.1524" layer="94"/>
<wire x1="0.127" y1="-27.559" x2="-0.762" y2="-27.178" width="0.1524" layer="94"/>
<wire x1="0.635" y1="-28.575" x2="-0.762" y2="-27.178" width="0.1524" layer="94"/>
<pin name="K1" x="-2.54" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="A1" x="5.08" y="-30.48" visible="off" length="short" direction="pas" rot="R180"/>
<text x="4.064" y="-22.606" size="1.27" layer="94" rot="R180">G</text>
<text x="4.064" y="-28.194" size="1.27" layer="94" rot="R180">Y</text>
</symbol>
<symbol name="TLV1117-33CDCY">
<pin name="INPUT" x="-20.32" y="0" length="middle" direction="in"/>
<pin name="ADJ/GND" x="-20.32" y="-7.62" length="middle" direction="in"/>
<pin name="OUTPUT" x="20.32" y="-2.54" length="middle" direction="out" rot="R180"/>
<wire x1="-15.24" y1="5.08" x2="-15.24" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-15.24" y1="-12.7" x2="15.24" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="5.08" width="0.4064" layer="94"/>
<wire x1="15.24" y1="5.08" x2="-15.24" y2="5.08" width="0.4064" layer="94"/>
<text x="-4.5974" y="6.8326" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-6.1468" y="-15.2908" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="MINI-USB-5">
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-1.27" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="1.016" y2="-8.128" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="1.016" y1="-8.128" x2="2.54" y2="-8.89" width="0.254" layer="94" curve="53.130102"/>
<wire x1="2.54" y1="-8.89" x2="5.08" y2="-8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="-8.89" x2="6.35" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-1.27" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="1.016" y2="8.128" width="0.254" layer="94" curve="53.130102"/>
<wire x1="1.016" y1="8.128" x2="2.54" y2="8.89" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="2.54" y1="8.89" x2="5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="8.89" x2="6.35" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="3.81" y2="-6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="-6.35" x2="3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="6.35" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="11.43" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="10.16" y="-7.62" size="1.778" layer="96" font="vector" rot="R90">&gt;VALUE</text>
<pin name="1" x="-5.08" y="5.08" visible="pin" direction="pas"/>
<pin name="2" x="-5.08" y="2.54" visible="pin" direction="pas"/>
<pin name="3" x="-5.08" y="0" visible="pin" direction="pas"/>
<pin name="4" x="-5.08" y="-2.54" visible="pin" direction="pas"/>
<pin name="5" x="-5.08" y="-5.08" visible="pin" direction="pas"/>
</symbol>
<symbol name="SHIELD_4">
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94" style="shortdash"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94" style="shortdash"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94" style="shortdash"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="7.62" y2="2.54" width="0.254" layer="94" style="shortdash" curve="90"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="17.78" width="0.254" layer="94" style="shortdash"/>
<wire x1="7.62" y1="17.78" x2="5.08" y2="20.32" width="0.254" layer="94" style="shortdash" curve="90"/>
<wire x1="5.08" y1="20.32" x2="0" y2="20.32" width="0.254" layer="94" style="shortdash"/>
<text x="7.62" y="-2.54" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="S1" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S2" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S3" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S4" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="J-PMS">
<wire x1="0.5" y1="-0.5" x2="5.5" y2="-0.5" width="0.254" layer="94"/>
<wire x1="5.5" y1="-0.5" x2="5.5" y2="0.5" width="0.254" layer="94"/>
<wire x1="5.5" y1="0.5" x2="0.5" y2="0.5" width="0.254" layer="94"/>
<wire x1="0.5" y1="0.5" x2="0.5" y2="-0.5" width="0.254" layer="94" curve="180"/>
<wire x1="5.5" y1="0.5" x2="5.5" y2="1" width="0.254" layer="94"/>
<wire x1="5.5" y1="1" x2="6.5" y2="1" width="0.254" layer="94"/>
<wire x1="6.5" y1="1" x2="6.5" y2="0" width="0.254" layer="94"/>
<wire x1="6.5" y1="0" x2="6.5" y2="-1" width="0.254" layer="94"/>
<wire x1="6.5" y1="-1" x2="5.5" y2="-1" width="0.254" layer="94"/>
<wire x1="5.5" y1="-1" x2="5.5" y2="-0.5" width="0.254" layer="94"/>
<wire x1="2.1138" y1="-5.0794" x2="0.6707" y2="-1.8414" width="0.1" layer="94"/>
<wire x1="0.6707" y1="-1.8414" x2="0.2276" y2="-3.5974" width="0.1" layer="94"/>
<wire x1="3.6137" y1="-2.539" x2="3.6138" y2="-5.0794" width="0.1" layer="94"/>
<wire x1="3.6138" y1="-5.0794" x2="3.8638" y2="-4.5526" width="0.1" layer="94"/>
<wire x1="3.6138" y1="-5.0794" x2="3.3638" y2="-4.5526" width="0.1" layer="94"/>
<wire x1="3.3638" y1="-4.5526" x2="3.8638" y2="-4.5526" width="0.1" layer="94"/>
<wire x1="3.6137" y1="-2.539" x2="7.5526" y2="-2.539" width="0.127" layer="94"/>
<wire x1="2.1138" y1="-5.0794" x2="3.6138" y2="-5.0794" width="0.127" layer="94"/>
<wire x1="3.6138" y1="-5.0794" x2="6.6407" y2="-5.0794" width="0.127" layer="94"/>
<wire x1="6.5" y1="0" x2="7.5" y2="0" width="0.125" layer="94"/>
<wire x1="6.6407" y1="-5.0794" x2="7.5554" y2="-5.0793" width="0.125" layer="94"/>
<circle x="7.5" y="0" radius="0.25" width="0" layer="94"/>
<circle x="7.5285" y="-2.5121" radius="0.25" width="0" layer="94"/>
<circle x="7.5285" y="-5.0809" radius="0.25" width="0" layer="94"/>
<text x="3.1538" y="-6.6326" size="1.016" layer="95">&gt;NAME</text>
<pin name="1" x="10.16" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="2" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="PWR_JACK">
<wire x1="-1.4" y1="0.6" x2="-1.4" y2="-0.6" width="0.254" layer="94" curve="-313.602819" cap="flat"/>
<wire x1="0" y1="0" x2="-4.3" y2="0" width="0.254" layer="94"/>
<wire x1="1.6" y1="0" x2="5.4" y2="0" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.3" width="0.4064" layer="94"/>
<text x="-6" y="-0.8" size="1.524" layer="94">+</text>
<text x="5.9" y="-0.8" size="1.524" layer="94">-</text>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="WIFI-LPT100">
<wire x1="0" y1="0" x2="33.02" y2="0" width="0.254" layer="94"/>
<wire x1="33.02" y1="0" x2="33.02" y2="30.48" width="0.254" layer="94"/>
<wire x1="33.02" y1="30.48" x2="0" y2="30.48" width="0.254" layer="94"/>
<wire x1="0" y1="30.48" x2="0" y2="0" width="0.254" layer="94"/>
<text x="27.94" y="25.4" size="2.0828" layer="94" ratio="10" rot="R270">802.11b/g/n</text>
<text x="2.54" y="-5.08" size="2.0828" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="33.02" size="2.0828" layer="96" ratio="10">&gt;VALUE</text>
<pin name="GND" x="-5.08" y="27.94" length="middle" direction="pwr"/>
<pin name="DVDD" x="-5.08" y="25.4" length="middle" direction="pwr"/>
<pin name="NRELOAD" x="-5.08" y="22.86" length="middle"/>
<pin name="EXT_RESETN" x="-5.08" y="20.32" length="middle"/>
<pin name="UART0_RX" x="-5.08" y="17.78" length="middle" direction="in"/>
<pin name="UART0_TX" x="-5.08" y="15.24" length="middle" direction="out"/>
<pin name="PWR_SW" x="-5.08" y="12.7" length="middle"/>
<pin name="WPS" x="-5.08" y="10.16" length="middle"/>
<pin name="NREADY" x="-5.08" y="7.62" length="middle"/>
<pin name="NLINK" x="-5.08" y="5.08" length="middle"/>
</symbol>
<symbol name="PINHD4">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="9C-*MAAJ-T" prefix="Q">
<description>Crystal in HC49 package</description>
<gates>
<gate name="G$1" symbol="XTAL-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="12.000"/>
</technologies>
</device>
<device name="U-V" package="HC49U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP2551-I/SN" prefix="IC">
<description>High-Speed CAN Transceiver
&lt;br&gt;
http://cz.farnell.com/jsp/search/productdetail.jsp?id=9758569&amp;durl=cad1&amp;_requestid=511421</description>
<gates>
<gate name="A" symbol="MCP2551-I/SN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-8N">
<connects>
<connect gate="A" pin="CANH" pad="7"/>
<connect gate="A" pin="CANL" pad="6"/>
<connect gate="A" pin="RS" pad="8"/>
<connect gate="A" pin="RXD" pad="4"/>
<connect gate="A" pin="TXD" pad="1"/>
<connect gate="A" pin="VDD" pad="3"/>
<connect gate="A" pin="VREF" pad="5"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="MCP2551-I/SN" constant="no"/>
<attribute name="OC_FARNELL" value="9758569" constant="no"/>
<attribute name="OC_NEWARK" value="69K7604" constant="no"/>
<attribute name="PACKAGE" value="SOIC-8" constant="no"/>
<attribute name="SUPPLIER" value="Microchip" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F207VFT6" prefix="IC">
<description>ARM-based 32-bit MCU,LQFP-100</description>
<gates>
<gate name="A" symbol="STM32F207VFT6_A" x="0" y="0"/>
<gate name="B" symbol="STM32F207VFT6_B" x="-53.34" y="-2.54"/>
</gates>
<devices>
<device name="" package="QFP50P1600X1600X160-100N">
<connects>
<connect gate="A" pin="NRST" pad="14"/>
<connect gate="A" pin="PA0-WKUP" pad="23"/>
<connect gate="A" pin="PA1" pad="24"/>
<connect gate="A" pin="PA2" pad="25"/>
<connect gate="A" pin="PA3" pad="26"/>
<connect gate="A" pin="PA4" pad="29"/>
<connect gate="A" pin="PA5" pad="30"/>
<connect gate="A" pin="PA6" pad="31"/>
<connect gate="A" pin="PA7" pad="32"/>
<connect gate="A" pin="PB0" pad="35"/>
<connect gate="A" pin="PB1" pad="36"/>
<connect gate="A" pin="PB10" pad="47"/>
<connect gate="A" pin="PB11" pad="48"/>
<connect gate="A" pin="PB2" pad="37"/>
<connect gate="A" pin="PC0" pad="15"/>
<connect gate="A" pin="PC1" pad="16"/>
<connect gate="A" pin="PC13-RTC_AF1" pad="7"/>
<connect gate="A" pin="PC14-OSC32_IN" pad="8"/>
<connect gate="A" pin="PC15-OSC32_OUT" pad="9"/>
<connect gate="A" pin="PC2" pad="17"/>
<connect gate="A" pin="PC3" pad="18"/>
<connect gate="A" pin="PC4" pad="33"/>
<connect gate="A" pin="PC5" pad="34"/>
<connect gate="A" pin="PE10" pad="41"/>
<connect gate="A" pin="PE11" pad="42"/>
<connect gate="A" pin="PE12" pad="43"/>
<connect gate="A" pin="PE13" pad="44"/>
<connect gate="A" pin="PE14" pad="45"/>
<connect gate="A" pin="PE15" pad="46"/>
<connect gate="A" pin="PE2" pad="1"/>
<connect gate="A" pin="PE3" pad="2"/>
<connect gate="A" pin="PE4" pad="3"/>
<connect gate="A" pin="PE5" pad="4"/>
<connect gate="A" pin="PE6" pad="5"/>
<connect gate="A" pin="PE7" pad="38"/>
<connect gate="A" pin="PE8" pad="39"/>
<connect gate="A" pin="PE9" pad="40"/>
<connect gate="A" pin="PH0-OSC_IN" pad="12"/>
<connect gate="A" pin="PH1-OSC_OUT" pad="13"/>
<connect gate="A" pin="VBAT" pad="6"/>
<connect gate="A" pin="VCAP_1" pad="49"/>
<connect gate="A" pin="VDDA" pad="22"/>
<connect gate="A" pin="VDD_1" pad="50"/>
<connect gate="A" pin="VDD_12" pad="19"/>
<connect gate="A" pin="VDD_4" pad="28"/>
<connect gate="A" pin="VDD_5" pad="11"/>
<connect gate="A" pin="VREF+" pad="21"/>
<connect gate="A" pin="VSSA" pad="20"/>
<connect gate="A" pin="VSS_4" pad="27"/>
<connect gate="A" pin="VSS_5" pad="10"/>
<connect gate="B" pin="BOOT0" pad="94"/>
<connect gate="B" pin="PA14" pad="76"/>
<connect gate="B" pin="PA15" pad="77"/>
<connect gate="B" pin="PA_10" pad="69"/>
<connect gate="B" pin="PA_11" pad="70"/>
<connect gate="B" pin="PA_12" pad="71"/>
<connect gate="B" pin="PA_13" pad="72"/>
<connect gate="B" pin="PA_8" pad="67"/>
<connect gate="B" pin="PA_9" pad="68"/>
<connect gate="B" pin="PB12" pad="51"/>
<connect gate="B" pin="PB13" pad="52"/>
<connect gate="B" pin="PB14" pad="53"/>
<connect gate="B" pin="PB15" pad="54"/>
<connect gate="B" pin="PB3" pad="89"/>
<connect gate="B" pin="PB4" pad="90"/>
<connect gate="B" pin="PB5" pad="91"/>
<connect gate="B" pin="PB6" pad="92"/>
<connect gate="B" pin="PB7" pad="93"/>
<connect gate="B" pin="PB8" pad="95"/>
<connect gate="B" pin="PB9" pad="96"/>
<connect gate="B" pin="PC10" pad="78"/>
<connect gate="B" pin="PC11" pad="79"/>
<connect gate="B" pin="PC12" pad="80"/>
<connect gate="B" pin="PC6" pad="63"/>
<connect gate="B" pin="PC7" pad="64"/>
<connect gate="B" pin="PC8" pad="65"/>
<connect gate="B" pin="PC9" pad="66"/>
<connect gate="B" pin="PD0" pad="81"/>
<connect gate="B" pin="PD1" pad="82"/>
<connect gate="B" pin="PD10" pad="57"/>
<connect gate="B" pin="PD11" pad="58"/>
<connect gate="B" pin="PD12" pad="59"/>
<connect gate="B" pin="PD13" pad="60"/>
<connect gate="B" pin="PD14" pad="61"/>
<connect gate="B" pin="PD15" pad="62"/>
<connect gate="B" pin="PD2" pad="83"/>
<connect gate="B" pin="PD3" pad="84"/>
<connect gate="B" pin="PD4" pad="85"/>
<connect gate="B" pin="PD5" pad="86"/>
<connect gate="B" pin="PD6" pad="87"/>
<connect gate="B" pin="PD7" pad="88"/>
<connect gate="B" pin="PD8" pad="55"/>
<connect gate="B" pin="PD9" pad="56"/>
<connect gate="B" pin="PE0" pad="97"/>
<connect gate="B" pin="PE1" pad="98"/>
<connect gate="B" pin="RFU" pad="99"/>
<connect gate="B" pin="VCAP_2" pad="73"/>
<connect gate="B" pin="VDD_2" pad="75"/>
<connect gate="B" pin="VDD_3" pad="100"/>
<connect gate="B" pin="VSS_2" pad="74"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="STM32F207VFT6" constant="no"/>
<attribute name="OC_FARNELL" value="2060915" constant="no"/>
<attribute name="OC_NEWARK" value="47T9344" constant="no"/>
<attribute name="PACKAGE" value="LQFP-100" constant="no"/>
<attribute name="SUPPLIER" value="STMICROELECTRONICS" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN75176BD" prefix="IC">
<description>DIFFERENTIAL BUS TRANSCEIVER
&lt;br&gt;
http://cz.farnell.com/texas-instruments/sn75176bd/bus-transceiver-rs422-rs485-soic8/dp/1103176</description>
<gates>
<gate name="A" symbol="SN75176BD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-8N">
<connects>
<connect gate="A" pin="A" pad="6"/>
<connect gate="A" pin="B" pad="7"/>
<connect gate="A" pin="D" pad="4"/>
<connect gate="A" pin="DE" pad="3"/>
<connect gate="A" pin="GND" pad="5"/>
<connect gate="A" pin="R" pad="1"/>
<connect gate="A" pin="VCC" pad="8"/>
<connect gate="A" pin="~RE" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="SN75176BD" constant="no"/>
<attribute name="OC_FARNELL" value="1103176" constant="no"/>
<attribute name="OC_NEWARK" value="08F8127" constant="no"/>
<attribute name="PACKAGE" value="SOIC-8" constant="no"/>
<attribute name="SUPPLIER" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KSZ8081RNACA" prefix="IC">
<description>MICREL SEMICONDUCTOR - KSZ8081RNACA TR - IC, TRX, PHY 10/100 PHY, 24MLF
&lt;br&gt;
http://cz.farnell.com/micrel-semiconductor/ksz8081rnaca-tr/ic-trx-phy-10-100-phy-24mlf/dp/2345483</description>
<gates>
<gate name="G$1" symbol="KSZ8081RNACA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN24">
<connects>
<connect gate="G$1" pin="CRS_DV/ADD10" pad="15"/>
<connect gate="G$1" pin="GND" pad="22 EXP THRU"/>
<connect gate="G$1" pin="INTRP" pad="18"/>
<connect gate="G$1" pin="LED0/ANEN_SPEED" pad="23"/>
<connect gate="G$1" pin="MDC" pad="11"/>
<connect gate="G$1" pin="MDIO" pad="10"/>
<connect gate="G$1" pin="REF_CLK" pad="16"/>
<connect gate="G$1" pin="REXT" pad="9"/>
<connect gate="G$1" pin="RST#" pad="24"/>
<connect gate="G$1" pin="RXD0" pad="13"/>
<connect gate="G$1" pin="RXD1" pad="12"/>
<connect gate="G$1" pin="RXER" pad="17"/>
<connect gate="G$1" pin="RXM" pad="3"/>
<connect gate="G$1" pin="RXP" pad="4"/>
<connect gate="G$1" pin="TXD0" pad="20"/>
<connect gate="G$1" pin="TXD1" pad="21"/>
<connect gate="G$1" pin="TXEN" pad="19"/>
<connect gate="G$1" pin="TXM" pad="5"/>
<connect gate="G$1" pin="TXP" pad="6"/>
<connect gate="G$1" pin="VDDA_3.3" pad="2"/>
<connect gate="G$1" pin="VDDIO" pad="14"/>
<connect gate="G$1" pin="VDD_1.2" pad="1"/>
<connect gate="G$1" pin="XI" pad="8"/>
<connect gate="G$1" pin="XO" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="24AA02E48T-I/OT" prefix="IC">
<description>2K I2C™ Serial EEPROMs, SOT23-5</description>
<gates>
<gate name="A" symbol="24AA02E48T-I/OT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P270X145-5N">
<connects>
<connect gate="A" pin="NC" pad="5"/>
<connect gate="A" pin="SCL" pad="1"/>
<connect gate="A" pin="SDA" pad="3"/>
<connect gate="A" pin="VCC" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="24AA02E48T-I/OT " constant="no"/>
<attribute name="OC_FARNELL" value="1688855" constant="no"/>
<attribute name="OC_NEWARK" value="45P4895" constant="no"/>
<attribute name="PACKAGE" value="SOT23-5" constant="no"/>
<attribute name="SUPPLIER" value="Microchip" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="W237-02P" prefix="X" uservalue="yes">
<description>&lt;b&gt;WAGO SREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="0" addlevel="always"/>
<gate name="-2" symbol="KL+V" x="0" y="-5.08" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-132">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="W237-3E" prefix="X" uservalue="yes">
<description>&lt;b&gt;WAGO SREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="7.62" addlevel="always"/>
<gate name="-2" symbol="KL" x="0" y="2.54" addlevel="always"/>
<gate name="-3" symbol="KL+V" x="0" y="-2.54" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-3E">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
<connect gate="-3" pin="KL" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-0805" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LMJ1998824110DL1T39J" prefix="CONN">
<description>http://www.tme.eu/cz/details/rj45-trafo/konektory-rj/amphenol/lmj1598824110dt39/#</description>
<gates>
<gate name="A" symbol="JACK8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LMJ1998824110DL1T39J">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8 SHIELD1 SHIELD2"/>
<connect gate="A" pin="A" pad="A1"/>
<connect gate="A" pin="A1" pad="A2"/>
<connect gate="A" pin="K" pad="K1"/>
<connect gate="A" pin="K1" pad="K2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TLV1117-33CDCY" prefix="U">
<description>V REG, LDO, 3.3V, 0.8A 
&lt;br&gt;
http://cz.farnell.com/jsp/search/productdetail.jsp?id=1494942&amp;durl=cad1&amp;_requestid=254029</description>
<gates>
<gate name="A" symbol="TLV1117-33CDCY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT230P700X180-4N">
<connects>
<connect gate="A" pin="ADJ/GND" pad="1"/>
<connect gate="A" pin="INPUT" pad="3"/>
<connect gate="A" pin="OUTPUT" pad="2 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="TLV1117-33CDCY" constant="no"/>
<attribute name="OC_FARNELL" value="1494942" constant="no"/>
<attribute name="OC_NEWARK" value="67K8520" constant="no"/>
<attribute name="PACKAGE" value="SOT-223-3" constant="no"/>
<attribute name="SUPPLIER" value="TEXAS INSTRUMENTS" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MINI-USB-SCHIELD-" prefix="X">
<description>&lt;b&gt;MINI USB-B Conector&lt;/b&gt;&lt;p&gt;
Source: www.cypressindustries.com</description>
<gates>
<gate name="G$1" symbol="MINI-USB-5" x="0" y="0"/>
<gate name="S" symbol="SHIELD_4" x="0" y="-10.16" addlevel="always"/>
</gates>
<devices>
<device name="32005-201" package="32005-201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="S" pin="S1" pad="M1"/>
<connect gate="S" pin="S2" pad="M2"/>
<connect gate="S" pin="S3" pad="M3"/>
<connect gate="S" pin="S4" pad="M4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="32005-301" package="32005-301">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="S" pin="S1" pad="M1"/>
<connect gate="S" pin="S2" pad="M2"/>
<connect gate="S" pin="S3" pad="M3"/>
<connect gate="S" pin="S4" pad="M4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JACK-PMS" prefix="J" uservalue="yes">
<gates>
<gate name="P" symbol="J-PMS" x="-5.08" y="2.54"/>
<gate name="S" symbol="PWR_JACK" x="0" y="-7.62"/>
</gates>
<devices>
<device name="" package="JACK-PMS">
<connects>
<connect gate="P" pin="1" pad="1"/>
<connect gate="P" pin="2" pad="2"/>
<connect gate="P" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WIFI-LPT100" prefix="MOD">
<description>http://www.hi-flying.com/products_detail/&amp;productId=d7fa2397-c1ee-4e4f-89c3-600a864861de&amp;comp_stats=comp-FrontProducts_list01-111.html</description>
<gates>
<gate name="A" symbol="WIFI-LPT100" x="-15.24" y="-12.7"/>
</gates>
<devices>
<device name="" package="WIFI-LPT100">
<connects>
<connect gate="A" pin="DVDD" pad="2"/>
<connect gate="A" pin="EXT_RESETN" pad="4"/>
<connect gate="A" pin="GND" pad="1"/>
<connect gate="A" pin="NLINK" pad="10"/>
<connect gate="A" pin="NREADY" pad="9"/>
<connect gate="A" pin="NRELOAD" pad="3"/>
<connect gate="A" pin="PWR_SW" pad="7"/>
<connect gate="A" pin="UART0_RX" pad="5"/>
<connect gate="A" pin="UART0_TX" pad="6"/>
<connect gate="A" pin="WPS" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X4" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X04">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="EthernetConverter">
<packages>
<package name="0805-POL">
<wire x1="-1.7463" y1="0.2381" x2="-1.7463" y2="0.9526" width="0.127" layer="21"/>
<wire x1="-1.7463" y1="0.9526" x2="1.7462" y2="0.9525" width="0.127" layer="21"/>
<wire x1="1.7462" y1="0.9525" x2="1.7462" y2="0.2381" width="0.127" layer="21"/>
<wire x1="1.7463" y1="-0.2381" x2="1.7463" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="1.7463" y1="-0.9525" x2="-1.7463" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="-1.7463" y1="-0.9525" x2="-1.7463" y2="-0.2381" width="0.127" layer="21"/>
<wire x1="-2.0638" y1="0.7144" x2="-2.0638" y2="-0.7144" width="0.2032" layer="21"/>
<smd name="2" x="0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<smd name="1+" x="-0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<text x="-1.7621" y="2.2067" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.7621" y="1.0953" size="1.016" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="0603">
<description>Standard 0603 package</description>
<wire x1="-0.2286" y1="0.254" x2="0.2286" y2="0.254" width="0.127" layer="51"/>
<wire x1="0.2286" y1="-0.254" x2="-0.2286" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.2286" y1="0.254" x2="0.2286" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.2286" y1="0.254" x2="-0.2286" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-1.4288" y1="0.2381" x2="-1.4288" y2="0.7144" width="0.127" layer="21"/>
<wire x1="-1.4288" y1="0.7144" x2="1.4288" y2="0.7144" width="0.127" layer="21"/>
<wire x1="1.4288" y1="0.7144" x2="1.4288" y2="0.2381" width="0.127" layer="21"/>
<wire x1="-1.4288" y1="-0.2381" x2="-1.4288" y2="-0.7144" width="0.127" layer="21"/>
<wire x1="-1.4288" y1="-0.7144" x2="1.4288" y2="-0.7144" width="0.127" layer="21"/>
<wire x1="1.4288" y1="-0.7144" x2="1.4288" y2="-0.2381" width="0.127" layer="21"/>
<smd name="2" x="0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<smd name="1" x="-0.7874" y="0" dx="0.762" dy="0.9652" layer="1"/>
<text x="-1.4605" y="0.8844" size="0.762" layer="27" ratio="12">&gt;VALUE</text>
<text x="-1.3205" y="-0.3268" size="0.762" layer="25" ratio="12">&gt;NAME</text>
<rectangle x1="-0.635" y1="-0.4064" x2="-0.2794" y2="0.4064" layer="51"/>
<rectangle x1="0.2794" y1="-0.4064" x2="0.635" y2="0.4064" layer="51"/>
</package>
<package name="TO254P1524X460-3N">
<smd name="1" x="-2.54" y="-8.89" dx="1.143" dy="3.9116" layer="1"/>
<smd name="2" x="0" y="0.1524" dx="10.3886" dy="11.049" layer="1"/>
<smd name="3" x="2.54" y="-8.89" dx="1.143" dy="3.9116" layer="1"/>
<wire x1="-1.9812" y1="-5.3848" x2="-3.0988" y2="-5.3848" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-5.3848" x2="-3.0988" y2="-10.4648" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-10.4648" x2="-1.9812" y2="-10.4648" width="0.1524" layer="51"/>
<wire x1="-1.9812" y1="-10.4648" x2="-1.9812" y2="-5.3848" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-5.3848" x2="1.9812" y2="-5.3848" width="0.1524" layer="51"/>
<wire x1="1.9812" y1="-5.3848" x2="1.9812" y2="-10.4648" width="0.1524" layer="51"/>
<wire x1="1.9812" y1="-10.4648" x2="3.0988" y2="-10.4648" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-10.4648" x2="3.0988" y2="-5.3848" width="0.1524" layer="51"/>
<wire x1="-0.7112" y1="-5.3848" x2="-0.7112" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="-0.7112" y1="-7.112" x2="0.7112" y2="-7.112" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-7.112" x2="0.7112" y2="-5.3848" width="0.1524" layer="51"/>
<wire x1="-5.1816" y1="-5.3848" x2="5.1816" y2="-5.3848" width="0.1524" layer="51"/>
<wire x1="5.1816" y1="-5.3848" x2="5.1816" y2="5.3848" width="0.1524" layer="51"/>
<wire x1="5.1816" y1="5.3848" x2="-5.1816" y2="5.3848" width="0.1524" layer="51"/>
<wire x1="-5.1816" y1="5.3848" x2="-5.1816" y2="-5.3848" width="0.1524" layer="51"/>
<text x="-6.2992" y="-9.4996" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<wire x1="-3.0988" y1="-5.9944" x2="-3.0988" y2="-6.5532" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="-5.9944" x2="1.9812" y2="-6.5532" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="-5.9944" x2="-1.9812" y2="-6.5532" width="0.1524" layer="21"/>
<wire x1="3.0988" y1="-5.9944" x2="3.0988" y2="-6.5532" width="0.1524" layer="21"/>
<text x="-6.2992" y="-9.4996" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="-5.4356" y1="-11.0744" x2="-5.4356" y2="5.9436" width="0.1524" layer="39"/>
<wire x1="-5.4356" y1="5.9436" x2="5.4356" y2="5.9436" width="0.1524" layer="39"/>
<wire x1="5.4356" y1="5.9436" x2="5.4356" y2="-11.0744" width="0.1524" layer="39"/>
<wire x1="5.4356" y1="-11.0744" x2="-5.4356" y2="-11.0744" width="0.1524" layer="39"/>
<wire x1="-5.4356" y1="-11.0744" x2="-5.4356" y2="5.9436" width="0.1524" layer="39"/>
<wire x1="-5.4356" y1="5.9436" x2="5.4356" y2="5.9436" width="0.1524" layer="39"/>
<wire x1="5.4356" y1="5.9436" x2="5.4356" y2="-11.0744" width="0.1524" layer="39"/>
<text x="-4.5974" y="6.2992" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.969" y="-13.4874" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-3.429" y1="0.381" x2="-2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.762" x2="-3.048" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="1.27" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="-0.762" x2="-2.413" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-2.413" y1="-0.381" x2="-2.921" y2="0.127" width="0.1524" layer="94"/>
<wire x1="-2.921" y1="0.127" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="1.5875" y="0.9525" size="1.524" layer="95">&gt;NAME</text>
<text x="1.5875" y="-0.9525" size="1.524" layer="96">&gt;VALUE</text>
<pin name="K" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.762" x2="0" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="0.762" x2="1.524" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="1.778" x2="0" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="1.778" x2="1.524" y2="1.778" width="0.254" layer="94"/>
<text x="0.3175" y="2.2225" size="1.524" layer="95">&gt;NAME</text>
<text x="0.3175" y="-1.27" size="1.524" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R">
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.048" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.048" y1="0.762" x2="-1.524" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-0.762" x2="0" y2="0.762" width="0.254" layer="94"/>
<wire x1="0" y1="0.762" x2="1.524" y2="-0.762" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.762" x2="3.048" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.048" y1="0.762" x2="3.81" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="1.3716" size="1.524" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.6035" size="1.524" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="L7805CD2T-TR">
<pin name="INPUT" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="OUTPUT" x="12.7" y="0" length="middle" direction="out" rot="R180"/>
<pin name="GND" x="-17.78" y="-2.54" length="middle" direction="pas"/>
<wire x1="-12.7" y1="5.08" x2="-12.7" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-12.7" y2="5.08" width="0.4064" layer="94"/>
<text x="-12.7508" y="6.1976" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-10.6426" y="-7.9502" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED-0805" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805-POL">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-0603" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-0603" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L7805CD2T-TR" prefix="IC">
<description>V REG, 2VDO, 1.5A, 5V, 4%, 3D2PAK</description>
<gates>
<gate name="A" symbol="L7805CD2T-TR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO254P1524X460-3N">
<connects>
<connect gate="A" pin="GND" pad="2"/>
<connect gate="A" pin="INPUT" pad="1"/>
<connect gate="A" pin="OUTPUT" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="L7805CD2T-TR" constant="no"/>
<attribute name="OC_FARNELL" value="2253445" constant="no"/>
<attribute name="OC_NEWARK" value="89K1379" constant="no"/>
<attribute name="PACKAGE" value="D2PAK" constant="no"/>
<attribute name="SUPPLIER" value="STMicroelectronics" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Supply">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.27" y="-2.6988" size="1.016" layer="94" ratio="10">GND</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-2.8575" y="3.175" size="1.524" layer="94">+3.3V</text>
<pin name="+3.3V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+05V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.524" layer="94">+5V</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND">
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3.3V" uservalue="yes">
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V">
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="Q1" library="NetworkGateway" deviceset="9C-*MAAJ-T" device="" technology="12.000" value="9C-25.000MAAJ-T"/>
<part name="IC2" library="NetworkGateway" deviceset="MCP2551-I/SN" device=""/>
<part name="IC4" library="NetworkGateway" deviceset="STM32F207VFT6" device=""/>
<part name="IC3" library="NetworkGateway" deviceset="SN75176BD" device=""/>
<part name="IC1" library="NetworkGateway" deviceset="KSZ8081RNACA" device=""/>
<part name="IC7" library="NetworkGateway" deviceset="24AA02E48T-I/OT" device=""/>
<part name="X2" library="NetworkGateway" deviceset="W237-3E" device=""/>
<part name="X3" library="NetworkGateway" deviceset="W237-02P" device=""/>
<part name="D1" library="EthernetConverter" deviceset="LED-0805" device=""/>
<part name="R3" library="EthernetConverter" deviceset="R-0603" device="" value="330"/>
<part name="C3" library="EthernetConverter" deviceset="C-0603" device="" value="22p"/>
<part name="C4" library="EthernetConverter" deviceset="C-0603" device="" value="22p"/>
<part name="U$3" library="Supply" deviceset="GND" device=""/>
<part name="U$7" library="Supply" deviceset="GND" device=""/>
<part name="IC11" library="EthernetConverter" deviceset="L7805CD2T-TR" device=""/>
<part name="U$35" library="Supply" deviceset="+5V" device=""/>
<part name="U$36" library="Supply" deviceset="GND" device=""/>
<part name="U$38" library="Supply" deviceset="GND" device=""/>
<part name="U$39" library="Supply" deviceset="+3.3V" device=""/>
<part name="C25" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="U$40" library="Supply" deviceset="+5V" device=""/>
<part name="U$41" library="Supply" deviceset="GND" device=""/>
<part name="U$42" library="Supply" deviceset="GND" device=""/>
<part name="U$43" library="Supply" deviceset="GND" device=""/>
<part name="U$21" library="Supply" deviceset="+3.3V" device=""/>
<part name="U$22" library="Supply" deviceset="+3.3V" device=""/>
<part name="U$23" library="Supply" deviceset="GND" device=""/>
<part name="C1" library="EthernetConverter" deviceset="C-0603" device="" value="2.2u"/>
<part name="U$24" library="Supply" deviceset="GND" device=""/>
<part name="R25" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R26" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="C20" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="C27" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="U$31" library="Supply" deviceset="GND" device=""/>
<part name="C28" library="EthernetConverter" deviceset="C-0603" device="" value="2.2u"/>
<part name="U$33" library="Supply" deviceset="GND" device=""/>
<part name="U$34" library="Supply" deviceset="GND" device=""/>
<part name="R28" library="EthernetConverter" deviceset="R-0603" device="" value="10k"/>
<part name="C29" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="U$45" library="Supply" deviceset="GND" device=""/>
<part name="U$46" library="Supply" deviceset="+3.3V" device=""/>
<part name="C2" library="EthernetConverter" deviceset="C-0603" device="" value="2.2u"/>
<part name="C13" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="U$1" library="Supply" deviceset="GND" device=""/>
<part name="U$2" library="Supply" deviceset="GND" device=""/>
<part name="C15" library="NetworkGateway" deviceset="C-0805" device="" value="22u"/>
<part name="R2" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="C19" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="C30" library="NetworkGateway" deviceset="C-0805" device="" value="22u"/>
<part name="C31" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="U$25" library="Supply" deviceset="GND" device=""/>
<part name="R29" library="EthernetConverter" deviceset="R-0603" device="" value="6k49"/>
<part name="C33" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="R14" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="U$12" library="Supply" deviceset="GND" device=""/>
<part name="R30" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R31" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R32" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R33" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R34" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R35" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R36" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R37" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R38" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="U$47" library="Supply" deviceset="+3.3V" device=""/>
<part name="CONN1" library="NetworkGateway" deviceset="LMJ1998824110DL1T39J" device=""/>
<part name="R15" library="EthernetConverter" deviceset="R-0603" device="" value="330"/>
<part name="U$13" library="Supply" deviceset="+3.3V" device=""/>
<part name="R16" library="EthernetConverter" deviceset="R-0603" device="" value="1k5"/>
<part name="R17" library="EthernetConverter" deviceset="R-0603" device="" value="10k"/>
<part name="U$14" library="Supply" deviceset="+3.3V" device=""/>
<part name="R4" library="EthernetConverter" deviceset="R-0603" device="" value="330"/>
<part name="C5" library="EthernetConverter" deviceset="C-0603" device="" value="2.2u"/>
<part name="C8" library="EthernetConverter" deviceset="C-0603" device="" value="2.2u"/>
<part name="R7" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R8" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R9" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="R6" library="EthernetConverter" deviceset="R-0603" device="" value="10k"/>
<part name="R10" library="EthernetConverter" deviceset="R-0603" device="" value="10k"/>
<part name="U$4" library="Supply" deviceset="+3.3V" device=""/>
<part name="U$5" library="Supply" deviceset="+3.3V" device=""/>
<part name="U$9" library="Supply" deviceset="GND" device=""/>
<part name="C9" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="D3" library="EthernetConverter" deviceset="LED-0805" device=""/>
<part name="R12" library="EthernetConverter" deviceset="R-0603" device="" value="330R"/>
<part name="U$15" library="Supply" deviceset="GND" device=""/>
<part name="D5" library="EthernetConverter" deviceset="LED-0805" device=""/>
<part name="R18" library="EthernetConverter" deviceset="R-0603" device="" value="330R"/>
<part name="U$10" library="Supply" deviceset="GND" device=""/>
<part name="C10" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="IC6" library="NetworkGateway" deviceset="SN75176BD" device=""/>
<part name="X4" library="NetworkGateway" deviceset="W237-02P" device=""/>
<part name="U$17" library="Supply" deviceset="GND" device=""/>
<part name="C11" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="R19" library="EthernetConverter" deviceset="R-0603" device="" value="10k"/>
<part name="R20" library="EthernetConverter" deviceset="R-0603" device="" value="49R9"/>
<part name="U$26" library="Supply" deviceset="GND" device=""/>
<part name="C14" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="U$28" library="Supply" deviceset="GND" device=""/>
<part name="U$30" library="Supply" deviceset="GND" device=""/>
<part name="R21" library="EthernetConverter" deviceset="R-0603" device="" value="10k"/>
<part name="R22" library="EthernetConverter" deviceset="R-0603" device="" value="10k"/>
<part name="U$32" library="Supply" deviceset="GND" device=""/>
<part name="R1" library="EthernetConverter" deviceset="R-0603" device="" value="330"/>
<part name="U$44" library="Supply" deviceset="+5V" device=""/>
<part name="U$8" library="Supply" deviceset="+5V" device=""/>
<part name="U$16" library="Supply" deviceset="+5V" device=""/>
<part name="U$27" library="Supply" deviceset="+5V" device=""/>
<part name="IC8" library="NetworkGateway" deviceset="TLV1117-33CDCY" device=""/>
<part name="C17" library="NetworkGateway" deviceset="C-0805" device="" value="22u"/>
<part name="CONN2" library="NetworkGateway" deviceset="MINI-USB-SCHIELD-" device="32005-201" value="MINI-USB-SCHIELD-"/>
<part name="U$37" library="Supply" deviceset="GND" device=""/>
<part name="R11" library="EthernetConverter" deviceset="R-0603" device="" value="22R0"/>
<part name="R13" library="EthernetConverter" deviceset="R-0603" device="" value="22R0"/>
<part name="C18" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="C21" library="EthernetConverter" deviceset="C-0603" device="" value="100n"/>
<part name="U$48" library="Supply" deviceset="GND" device=""/>
<part name="C22" library="NetworkGateway" deviceset="C-0805" device="" value="22u/16V"/>
<part name="C16" library="NetworkGateway" deviceset="C-0805" device="" value="22u"/>
<part name="J1" library="NetworkGateway" deviceset="JACK-PMS" device=""/>
<part name="U$49" library="Supply" deviceset="GND" device=""/>
<part name="U$50" library="Supply" deviceset="+3.3V" device=""/>
<part name="U$51" library="Supply" deviceset="GND" device=""/>
<part name="JP3" library="NetworkGateway" deviceset="PINHD-1X2" device=""/>
<part name="R23" library="EthernetConverter" deviceset="R-0603" device="" value="10k"/>
<part name="U$52" library="Supply" deviceset="+3.3V" device=""/>
<part name="MOD1" library="NetworkGateway" deviceset="WIFI-LPT100" device=""/>
<part name="JP1" library="NetworkGateway" deviceset="PINHD-1X4" device=""/>
<part name="JP2" library="NetworkGateway" deviceset="PINHD-1X4" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="Q1" gate="G$1" x="50.8" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="48.514" y="117.7925" size="1.524" layer="95" rot="R90"/>
<attribute name="VALUE" x="54.61" y="108.2675" size="1.524" layer="96" rot="R90"/>
</instance>
<instance part="IC2" gate="A" x="-160.02" y="58.42"/>
<instance part="IC4" gate="A" x="12.7" y="139.7"/>
<instance part="IC4" gate="B" x="132.08" y="139.7"/>
<instance part="IC3" gate="A" x="-55.88" y="53.34"/>
<instance part="IC1" gate="G$1" x="-147.32" y="127"/>
<instance part="IC7" gate="A" x="-144.78" y="200.66" smashed="yes">
<attribute name="NAME" x="-154.8892" y="209.3722" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="-160.782" y="185.166" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="X2" gate="-1" x="-114.3" y="66.04" rot="R180"/>
<instance part="X2" gate="-2" x="-114.3" y="60.96" rot="R180"/>
<instance part="X2" gate="-3" x="-114.3" y="55.88" rot="R180"/>
<instance part="X3" gate="-1" x="-27.94" y="63.5" rot="R180"/>
<instance part="X3" gate="-2" x="-27.94" y="58.42" rot="R180"/>
<instance part="D1" gate="G$1" x="43.18" y="162.56" rot="R90"/>
<instance part="R3" gate="G$1" x="43.18" y="124.46" rot="R180"/>
<instance part="C3" gate="G$1" x="60.96" y="114.3" rot="R90"/>
<instance part="C4" gate="G$1" x="60.96" y="124.46" rot="R90"/>
<instance part="U$3" gate="GND" x="66.04" y="119.38" rot="R90"/>
<instance part="U$7" gate="GND" x="22.86" y="208.28" rot="R90"/>
<instance part="IC11" gate="A" x="83.82" y="220.98" smashed="yes">
<attribute name="NAME" x="71.0692" y="227.1776" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="68.0974" y="213.0298" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="U$35" gate="+5V" x="111.76" y="223.52"/>
<instance part="U$36" gate="GND" x="111.76" y="203.2"/>
<instance part="U$38" gate="GND" x="157.48" y="198.12"/>
<instance part="U$39" gate="G$1" x="157.48" y="223.52"/>
<instance part="C25" gate="G$1" x="99.06" y="215.9" rot="R180"/>
<instance part="U$40" gate="+5V" x="99.06" y="223.52"/>
<instance part="U$41" gate="GND" x="45.72" y="218.44"/>
<instance part="U$42" gate="GND" x="66.04" y="198.12"/>
<instance part="U$43" gate="GND" x="99.06" y="198.12"/>
<instance part="U$21" gate="G$1" x="-12.7" y="187.96"/>
<instance part="U$22" gate="G$1" x="109.22" y="187.96"/>
<instance part="U$23" gate="GND" x="-12.7" y="88.9"/>
<instance part="C1" gate="G$1" x="154.94" y="86.36"/>
<instance part="U$24" gate="GND" x="154.94" y="81.28"/>
<instance part="R25" gate="G$1" x="-27.94" y="157.48" rot="R180"/>
<instance part="R26" gate="G$1" x="-12.7" y="167.64" rot="R90"/>
<instance part="C20" gate="G$1" x="-15.24" y="152.4"/>
<instance part="C27" gate="G$1" x="-35.56" y="152.4"/>
<instance part="U$31" gate="GND" x="-27.94" y="147.32"/>
<instance part="C28" gate="G$1" x="38.1" y="86.36"/>
<instance part="U$33" gate="GND" x="38.1" y="81.28"/>
<instance part="U$34" gate="GND" x="109.22" y="86.36"/>
<instance part="R28" gate="G$1" x="58.42" y="106.68" rot="R180"/>
<instance part="C29" gate="G$1" x="53.34" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="59.8805" y="102.4255" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="60.3885" y="99.314" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="U$45" gate="GND" x="53.34" y="91.44"/>
<instance part="U$46" gate="G$1" x="68.58" y="106.68" rot="R270"/>
<instance part="C2" gate="G$1" x="-187.96" y="104.14"/>
<instance part="C13" gate="G$1" x="-177.8" y="104.14"/>
<instance part="U$1" gate="GND" x="-182.88" y="99.06"/>
<instance part="U$2" gate="GND" x="-116.84" y="91.44"/>
<instance part="C15" gate="G$1" x="-86.36" y="104.14"/>
<instance part="R2" gate="G$1" x="-101.6" y="109.22" rot="R180"/>
<instance part="C19" gate="G$1" x="-119.38" y="104.14"/>
<instance part="C30" gate="G$1" x="-111.76" y="104.14"/>
<instance part="C31" gate="G$1" x="-93.98" y="104.14"/>
<instance part="U$25" gate="GND" x="-175.26" y="81.28"/>
<instance part="R29" gate="G$1" x="-175.26" y="91.44" rot="R90"/>
<instance part="C33" gate="G$1" x="-93.98" y="152.4"/>
<instance part="R14" gate="G$1" x="-106.68" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="-108.0516" y="180.848" size="1.524" layer="95" rot="R90"/>
</instance>
<instance part="U$12" gate="GND" x="-86.36" y="147.32"/>
<instance part="R30" gate="G$1" x="-182.88" y="157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="-186.69" y="159.9184" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-178.054" y="160.3375" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="R31" gate="G$1" x="-182.88" y="154.94" smashed="yes" rot="R180"/>
<instance part="R32" gate="G$1" x="-182.88" y="152.4" smashed="yes" rot="R180"/>
<instance part="R33" gate="G$1" x="-182.88" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="-186.436" y="148.2344" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="R34" gate="G$1" x="-182.88" y="134.62" rot="R180"/>
<instance part="R35" gate="G$1" x="-104.14" y="182.88" smashed="yes" rot="R90"/>
<instance part="R36" gate="G$1" x="-101.6" y="182.88" smashed="yes" rot="R90"/>
<instance part="R37" gate="G$1" x="-99.06" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="-96.3676" y="181.102" size="1.524" layer="95" rot="R90"/>
<attribute name="VALUE" x="-110.4265" y="180.34" size="1.524" layer="96" rot="R90"/>
</instance>
<instance part="R38" gate="G$1" x="-93.98" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="-90.2716" y="185.42" size="1.524" layer="95" rot="R90"/>
<attribute name="VALUE" x="-91.3765" y="180.34" size="1.524" layer="96" rot="R90"/>
</instance>
<instance part="U$47" gate="G$1" x="-101.6" y="193.04"/>
<instance part="CONN1" gate="A" x="-81.28" y="167.64"/>
<instance part="R15" gate="G$1" x="-96.52" y="137.16" smashed="yes" rot="R180">
<attribute name="NAME" x="-97.028" y="135.7884" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-90.932" y="135.6995" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="U$13" gate="G$1" x="-73.66" y="139.7" rot="R270"/>
<instance part="R16" gate="G$1" x="-180.34" y="175.26" rot="R90"/>
<instance part="R17" gate="G$1" x="-187.96" y="175.26" rot="R270"/>
<instance part="U$14" gate="G$1" x="-182.88" y="187.96"/>
<instance part="R4" gate="G$1" x="-96.52" y="142.24" smashed="yes" rot="R180">
<attribute name="NAME" x="-98.806" y="145.1864" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-92.202" y="145.0975" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="C5" gate="G$1" x="-40.64" y="152.4" smashed="yes">
<attribute name="NAME" x="-43.8785" y="154.6225" size="1.524" layer="95"/>
<attribute name="VALUE" x="-45.6565" y="150.876" size="1.524" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="-20.32" y="152.4" smashed="yes">
<attribute name="NAME" x="-23.3045" y="154.6225" size="1.524" layer="95"/>
<attribute name="VALUE" x="-25.0825" y="151.13" size="1.524" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="-20.32" y="111.76" rot="R180"/>
<instance part="R8" gate="G$1" x="104.14" y="121.92" rot="R180"/>
<instance part="R9" gate="G$1" x="104.14" y="119.38" rot="R180"/>
<instance part="R6" gate="G$1" x="-162.56" y="213.36" rot="R270"/>
<instance part="R10" gate="G$1" x="-172.72" y="213.36" rot="R270"/>
<instance part="U$4" gate="G$1" x="-167.64" y="220.98"/>
<instance part="U$5" gate="G$1" x="-129.54" y="203.2"/>
<instance part="U$9" gate="GND" x="-129.54" y="185.42"/>
<instance part="C9" gate="G$1" x="-129.54" y="195.58" smashed="yes" rot="R180">
<attribute name="NAME" x="-124.7775" y="196.9135" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-122.7455" y="193.294" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="D3" gate="G$1" x="43.18" y="157.48" rot="R90"/>
<instance part="R12" gate="G$1" x="58.42" y="157.48" smashed="yes" rot="R180"/>
<instance part="U$15" gate="GND" x="68.58" y="157.48" rot="R90"/>
<instance part="D5" gate="G$1" x="43.18" y="152.4" rot="R90"/>
<instance part="R18" gate="G$1" x="58.42" y="152.4" smashed="yes" rot="R180"/>
<instance part="U$10" gate="GND" x="-33.02" y="33.02"/>
<instance part="C10" gate="G$1" x="-33.02" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="-28.2575" y="44.5135" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-26.2255" y="40.894" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="IC6" gate="A" x="30.48" y="55.88"/>
<instance part="X4" gate="-1" x="58.42" y="66.04" rot="R180"/>
<instance part="X4" gate="-2" x="58.42" y="60.96" rot="R180"/>
<instance part="U$17" gate="GND" x="53.34" y="35.56"/>
<instance part="C11" gate="G$1" x="53.34" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="58.1025" y="47.0535" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="60.1345" y="43.434" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="R19" gate="G$1" x="-193.04" y="55.88" rot="R180"/>
<instance part="R20" gate="G$1" x="-132.08" y="60.96" rot="R270"/>
<instance part="U$26" gate="GND" x="-198.12" y="50.8"/>
<instance part="C14" gate="G$1" x="-185.42" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="-180.6575" y="47.0535" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="-178.6255" y="43.434" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="U$28" gate="GND" x="-185.42" y="38.1"/>
<instance part="U$30" gate="GND" x="-139.7" y="27.94"/>
<instance part="R21" gate="G$1" x="-134.62" y="43.18"/>
<instance part="R22" gate="G$1" x="-139.7" y="35.56" rot="R270"/>
<instance part="U$32" gate="GND" x="-124.46" y="60.96" rot="R270"/>
<instance part="R1" gate="G$1" x="58.42" y="162.56" smashed="yes" rot="R180">
<attribute name="VALUE" x="60.96" y="165.1635" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="U$44" gate="+5V" x="53.34" y="50.8"/>
<instance part="U$8" gate="+5V" x="-129.54" y="45.72"/>
<instance part="U$16" gate="+5V" x="-185.42" y="50.8"/>
<instance part="U$27" gate="+5V" x="-33.02" y="48.26"/>
<instance part="IC8" gate="A" x="137.16" y="215.9"/>
<instance part="C17" gate="G$1" x="157.48" y="205.74"/>
<instance part="CONN2" gate="G$1" x="198.12" y="53.34" smashed="yes">
<attribute name="NAME" x="187.96" y="64.77" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="208.28" y="45.72" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="CONN2" gate="S" x="198.12" y="43.18"/>
<instance part="U$37" gate="GND" x="200.66" y="35.56"/>
<instance part="R11" gate="G$1" x="185.42" y="55.88" rot="R180"/>
<instance part="R13" gate="G$1" x="177.8" y="53.34" rot="R180"/>
<instance part="C18" gate="G$1" x="-33.02" y="172.72"/>
<instance part="C21" gate="G$1" x="-22.86" y="172.72"/>
<instance part="U$48" gate="GND" x="-27.94" y="167.64"/>
<instance part="C22" gate="G$1" x="60.96" y="218.44" smashed="yes">
<attribute name="NAME" x="61.2775" y="220.6625" size="1.524" layer="95"/>
<attribute name="VALUE" x="51.1175" y="217.17" size="1.524" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="111.76" y="210.82"/>
<instance part="J1" gate="P" x="33.02" y="226.06"/>
<instance part="U$49" gate="GND" x="165.1" y="101.6"/>
<instance part="U$50" gate="G$1" x="-86.36" y="111.76"/>
<instance part="U$51" gate="GND" x="-25.4" y="116.84" rot="R270"/>
<instance part="JP3" gate="G$1" x="175.26" y="121.92"/>
<instance part="R23" gate="G$1" x="165.1" y="111.76" rot="R90"/>
<instance part="U$52" gate="G$1" x="165.1" y="132.08"/>
<instance part="MOD1" gate="A" x="121.92" y="35.56"/>
<instance part="JP1" gate="A" x="2.54" y="210.82" rot="R180"/>
<instance part="JP2" gate="A" x="81.28" y="60.96" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="U$3" gate="GND" pin="GND"/>
<wire x1="63.5" y1="114.3" x2="63.5" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="63.5" y1="119.38" x2="63.5" y2="124.46" width="0.1524" layer="91"/>
<junction x="63.5" y="119.38"/>
</segment>
<segment>
<pinref part="U$7" gate="GND" pin="GND"/>
<wire x1="5.08" y1="208.28" x2="20.32" y2="208.28" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="U$36" gate="GND" pin="GND"/>
<wire x1="111.76" y1="208.28" x2="111.76" y2="205.74" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="ADJ/GND"/>
<wire x1="116.84" y1="208.28" x2="111.76" y2="208.28" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<junction x="111.76" y="208.28"/>
</segment>
<segment>
<pinref part="U$38" gate="GND" pin="GND"/>
<wire x1="157.48" y1="203.2" x2="157.48" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U$41" gate="GND" pin="GND"/>
<wire x1="43.18" y1="220.98" x2="45.72" y2="220.98" width="0.1524" layer="91"/>
<wire x1="43.18" y1="223.52" x2="43.18" y2="220.98" width="0.1524" layer="91"/>
<pinref part="J1" gate="P" pin="2"/>
<pinref part="J1" gate="P" pin="3"/>
<junction x="43.18" y="220.98"/>
</segment>
<segment>
<pinref part="IC11" gate="A" pin="GND"/>
<pinref part="U$42" gate="GND" pin="GND"/>
<wire x1="66.04" y1="218.44" x2="66.04" y2="215.9" width="0.1524" layer="91"/>
<wire x1="66.04" y1="215.9" x2="66.04" y2="210.82" width="0.1524" layer="91"/>
<wire x1="66.04" y1="210.82" x2="66.04" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="60.96" y1="215.9" x2="66.04" y2="215.9" width="0.1524" layer="91"/>
<junction x="66.04" y="215.9"/>
</segment>
<segment>
<pinref part="U$43" gate="GND" pin="GND"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="99.06" y1="200.66" x2="99.06" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$24" gate="GND" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-40.64" y1="149.86" x2="-38.1" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="-35.56" y1="149.86" x2="-38.1" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="149.86" x2="-27.94" y2="149.86" width="0.1524" layer="91"/>
<junction x="-35.56" y="149.86"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="149.86" x2="-35.56" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="149.86" x2="-20.32" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U$31" gate="GND" pin="GND"/>
<junction x="-27.94" y="149.86"/>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="C8" gate="G$1" pin="2"/>
<junction x="-20.32" y="149.86"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="U$33" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="VSS_2"/>
<pinref part="U$34" gate="GND" pin="GND"/>
<wire x1="111.76" y1="88.9" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="VSS_5"/>
<wire x1="-10.16" y1="96.52" x2="-12.7" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U$23" gate="GND" pin="GND"/>
<wire x1="-12.7" y1="96.52" x2="-12.7" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC4" gate="A" pin="VSSA"/>
<wire x1="-12.7" y1="93.98" x2="-12.7" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="93.98" x2="-12.7" y2="93.98" width="0.1524" layer="91"/>
<junction x="-12.7" y="93.98"/>
<pinref part="IC4" gate="A" pin="VSS_4"/>
<wire x1="-10.16" y1="91.44" x2="-12.7" y2="91.44" width="0.1524" layer="91"/>
<junction x="-12.7" y="91.44"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="1"/>
<pinref part="U$45" gate="GND" pin="GND"/>
<wire x1="53.34" y1="96.52" x2="53.34" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="U$1" gate="GND" pin="GND"/>
<wire x1="-187.96" y1="101.6" x2="-182.88" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="101.6" x2="-177.8" y2="101.6" width="0.1524" layer="91"/>
<junction x="-182.88" y="101.6"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="-86.36" y1="101.6" x2="-93.98" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="-93.98" y1="101.6" x2="-111.76" y2="101.6" width="0.1524" layer="91"/>
<junction x="-93.98" y="101.6"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="-111.76" y1="101.6" x2="-116.84" y2="101.6" width="0.1524" layer="91"/>
<junction x="-111.76" y="101.6"/>
<pinref part="U$2" gate="GND" pin="GND"/>
<wire x1="-116.84" y1="101.6" x2="-119.38" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="93.98" x2="-116.84" y2="96.52" width="0.1524" layer="91"/>
<junction x="-116.84" y="101.6"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="-116.84" y1="96.52" x2="-116.84" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="96.52" x2="-116.84" y2="96.52" width="0.1524" layer="91"/>
<junction x="-116.84" y="96.52"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="U$25" gate="GND" pin="GND"/>
<wire x1="-175.26" y1="86.36" x2="-175.26" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="U$12" gate="GND" pin="GND"/>
<wire x1="-93.98" y1="149.86" x2="-86.36" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="157.48" x2="-86.36" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="157.48" x2="-86.36" y2="149.86" width="0.1524" layer="91"/>
<junction x="-86.36" y="149.86"/>
<pinref part="CONN1" gate="A" pin="8"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="VSS"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-132.08" y1="190.5" x2="-129.54" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U$9" gate="GND" pin="GND"/>
<wire x1="-129.54" y1="190.5" x2="-129.54" y2="187.96" width="0.1524" layer="91"/>
<junction x="-129.54" y="190.5"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="U$15" gate="GND" pin="GND"/>
<wire x1="63.5" y1="157.48" x2="66.04" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="63.5" y1="152.4" x2="66.04" y2="152.4" width="0.1524" layer="91"/>
<wire x1="66.04" y1="152.4" x2="66.04" y2="154.94" width="0.1524" layer="91"/>
<wire x1="66.04" y1="154.94" x2="66.04" y2="157.48" width="0.1524" layer="91"/>
<junction x="66.04" y="157.48"/>
<wire x1="66.04" y1="160.02" x2="66.04" y2="157.48" width="0.1524" layer="91"/>
<wire x1="63.5" y1="162.56" x2="66.04" y2="162.56" width="0.1524" layer="91"/>
<wire x1="66.04" y1="162.56" x2="66.04" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="GND"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="38.1" x2="-33.02" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U$10" gate="GND" pin="GND"/>
<wire x1="-33.02" y1="38.1" x2="-33.02" y2="35.56" width="0.1524" layer="91"/>
<junction x="-33.02" y="38.1"/>
</segment>
<segment>
<pinref part="IC6" gate="A" pin="GND"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="48.26" y1="40.64" x2="53.34" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$17" gate="GND" pin="GND"/>
<wire x1="53.34" y1="40.64" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<junction x="53.34" y="40.64"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="U$26" gate="GND" pin="GND"/>
<wire x1="-198.12" y1="53.34" x2="-198.12" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="A" pin="VSS"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="-185.42" y1="40.64" x2="-177.8" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$28" gate="GND" pin="GND"/>
<junction x="-185.42" y="40.64"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="U$30" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="X2" gate="-2" pin="KL"/>
<pinref part="U$32" gate="GND" pin="GND"/>
<wire x1="-121.92" y1="60.96" x2="-119.38" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CONN2" gate="S" pin="S1"/>
<pinref part="CONN2" gate="S" pin="S2"/>
<wire x1="195.58" y1="40.64" x2="198.12" y2="40.64" width="0.1524" layer="91"/>
<pinref part="CONN2" gate="S" pin="S3"/>
<wire x1="198.12" y1="40.64" x2="200.66" y2="40.64" width="0.1524" layer="91"/>
<junction x="198.12" y="40.64"/>
<pinref part="CONN2" gate="S" pin="S4"/>
<wire x1="200.66" y1="40.64" x2="203.2" y2="40.64" width="0.1524" layer="91"/>
<junction x="200.66" y="40.64"/>
<pinref part="U$37" gate="GND" pin="GND"/>
<wire x1="200.66" y1="40.64" x2="200.66" y2="38.1" width="0.1524" layer="91"/>
<pinref part="CONN2" gate="G$1" pin="5"/>
<wire x1="193.04" y1="48.26" x2="193.04" y2="40.64" width="0.1524" layer="91"/>
<wire x1="193.04" y1="40.64" x2="195.58" y2="40.64" width="0.1524" layer="91"/>
<junction x="195.58" y="40.64"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="U$48" gate="GND" pin="GND"/>
<wire x1="-33.02" y1="170.18" x2="-27.94" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="170.18" x2="-22.86" y2="170.18" width="0.1524" layer="91"/>
<junction x="-27.94" y="170.18"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PB2"/>
<pinref part="U$51" gate="GND" pin="GND"/>
<wire x1="-10.16" y1="116.84" x2="-22.86" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$49" gate="GND" pin="GND"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="165.1" y1="104.14" x2="165.1" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="116.84" y1="63.5" x2="99.06" y2="63.5" width="0.1524" layer="91"/>
<pinref part="MOD1" gate="A" pin="GND"/>
<label x="99.06" y="63.5" size="1.778" layer="95"/>
<pinref part="JP2" gate="A" pin="4"/>
<wire x1="83.82" y1="63.5" x2="99.06" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3.3V" class="0">
<segment>
<pinref part="U$39" gate="G$1" pin="+3.3V"/>
<wire x1="157.48" y1="215.9" x2="157.48" y2="220.98" width="0.1524" layer="91"/>
<wire x1="157.48" y1="215.9" x2="157.48" y2="213.36" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="OUTPUT"/>
<wire x1="157.48" y1="213.36" x2="157.48" y2="210.82" width="0.1524" layer="91"/>
<junction x="157.48" y="213.36"/>
<pinref part="C17" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U$22" gate="G$1" pin="+3.3V"/>
<wire x1="109.22" y1="185.42" x2="109.22" y2="182.88" width="0.1524" layer="91"/>
<pinref part="IC4" gate="B" pin="VDD_2"/>
<wire x1="109.22" y1="182.88" x2="111.76" y2="182.88" width="0.1524" layer="91"/>
<wire x1="109.22" y1="182.88" x2="109.22" y2="180.34" width="0.1524" layer="91"/>
<junction x="109.22" y="182.88"/>
<pinref part="IC4" gate="B" pin="VDD_3"/>
<wire x1="109.22" y1="180.34" x2="111.76" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="VDD_5"/>
<wire x1="-10.16" y1="182.88" x2="-12.7" y2="182.88" width="0.1524" layer="91"/>
<pinref part="U$21" gate="G$1" pin="+3.3V"/>
<wire x1="-12.7" y1="182.88" x2="-12.7" y2="185.42" width="0.1524" layer="91"/>
<pinref part="IC4" gate="A" pin="VBAT"/>
<wire x1="-10.16" y1="172.72" x2="-12.7" y2="172.72" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="172.72" x2="-12.7" y2="175.26" width="0.1524" layer="91"/>
<junction x="-12.7" y="182.88"/>
<pinref part="IC4" gate="A" pin="VDD_12"/>
<wire x1="-12.7" y1="175.26" x2="-12.7" y2="177.8" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="177.8" x2="-12.7" y2="180.34" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="180.34" x2="-12.7" y2="182.88" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="180.34" x2="-12.7" y2="180.34" width="0.1524" layer="91"/>
<junction x="-12.7" y="180.34"/>
<pinref part="IC4" gate="A" pin="VDD_4"/>
<wire x1="-10.16" y1="177.8" x2="-12.7" y2="177.8" width="0.1524" layer="91"/>
<junction x="-12.7" y="177.8"/>
<pinref part="IC4" gate="A" pin="VDD_1"/>
<wire x1="-10.16" y1="175.26" x2="-12.7" y2="175.26" width="0.1524" layer="91"/>
<junction x="-12.7" y="175.26"/>
<pinref part="R26" gate="G$1" pin="2"/>
<junction x="-12.7" y="172.72"/>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="177.8" x2="-33.02" y2="177.8" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="177.8" x2="-12.7" y2="177.8" width="0.1524" layer="91"/>
<junction x="-22.86" y="177.8"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<pinref part="U$46" gate="G$1" pin="+3.3V"/>
<wire x1="66.04" y1="106.68" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="-106.68" y1="187.96" x2="-106.68" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U$47" gate="G$1" pin="+3.3V"/>
<wire x1="-106.68" y1="190.5" x2="-104.14" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="190.5" x2="-101.6" y2="190.5" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="187.96" x2="-104.14" y2="190.5" width="0.1524" layer="91"/>
<junction x="-104.14" y="190.5"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="-101.6" y1="187.96" x2="-101.6" y2="190.5" width="0.1524" layer="91"/>
<junction x="-101.6" y="190.5"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="-99.06" y1="187.96" x2="-99.06" y2="190.5" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="190.5" x2="-101.6" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="-93.98" y1="187.96" x2="-93.98" y2="190.5" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="190.5" x2="-99.06" y2="190.5" width="0.1524" layer="91"/>
<junction x="-99.06" y="190.5"/>
</segment>
<segment>
<pinref part="CONN1" gate="A" pin="A"/>
<pinref part="U$13" gate="G$1" pin="+3.3V"/>
<wire x1="-76.2" y1="142.24" x2="-76.2" y2="139.7" width="0.1524" layer="91"/>
<pinref part="CONN1" gate="A" pin="A1"/>
<wire x1="-76.2" y1="139.7" x2="-76.2" y2="137.16" width="0.1524" layer="91"/>
<junction x="-76.2" y="139.7"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="+3.3V"/>
<wire x1="-182.88" y1="185.42" x2="-182.88" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="182.88" x2="-180.34" y2="182.88" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="182.88" x2="-180.34" y2="180.34" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="-182.88" y1="182.88" x2="-187.96" y2="182.88" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="182.88" x2="-187.96" y2="180.34" width="0.1524" layer="91"/>
<junction x="-182.88" y="182.88"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="+3.3V"/>
<wire x1="-172.72" y1="218.44" x2="-167.64" y2="218.44" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-162.56" y1="218.44" x2="-167.64" y2="218.44" width="0.1524" layer="91"/>
<junction x="-167.64" y="218.44"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="VCC"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="-132.08" y1="198.12" x2="-129.54" y2="198.12" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="+3.3V"/>
<wire x1="-129.54" y1="198.12" x2="-129.54" y2="200.66" width="0.1524" layer="91"/>
<junction x="-129.54" y="198.12"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VDDIO"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="-121.92" y1="111.76" x2="-93.98" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="111.76" x2="-93.98" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-96.52" y1="109.22" x2="-93.98" y2="109.22" width="0.1524" layer="91"/>
<junction x="-93.98" y="109.22"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-93.98" y1="109.22" x2="-86.36" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U$50" gate="G$1" pin="+3.3V"/>
<junction x="-86.36" y="109.22"/>
</segment>
<segment>
<pinref part="U$52" gate="G$1" pin="+3.3V"/>
<wire x1="165.1" y1="129.54" x2="165.1" y2="124.46" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="165.1" y1="124.46" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="116.84" y1="60.96" x2="99.06" y2="60.96" width="0.1524" layer="91"/>
<pinref part="MOD1" gate="A" pin="DVDD"/>
<label x="99.06" y="60.96" size="1.778" layer="95"/>
<pinref part="JP2" gate="A" pin="3"/>
<wire x1="83.82" y1="60.96" x2="99.06" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<wire x1="5.08" y1="205.74" x2="17.78" y2="205.74" width="0.1524" layer="91"/>
<label x="10.16" y="205.994" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="1"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="PA14"/>
<wire x1="111.76" y1="147.32" x2="83.82" y2="147.32" width="0.1524" layer="91"/>
<label x="83.82" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<wire x1="5.08" y1="210.82" x2="17.78" y2="210.82" width="0.1524" layer="91"/>
<label x="10.16" y="211.074" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="3"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="PA_13"/>
<wire x1="111.76" y1="149.86" x2="83.82" y2="149.86" width="0.1524" layer="91"/>
<label x="83.82" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="NRST" class="0">
<segment>
<wire x1="5.08" y1="213.36" x2="17.78" y2="213.36" width="0.1524" layer="91"/>
<label x="10.16" y="213.614" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="4"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="NRST"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="35.56" y1="106.68" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="53.34" y1="104.14" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<junction x="53.34" y="106.68"/>
<label x="38.1" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="K"/>
<wire x1="45.72" y1="162.56" x2="53.34" y2="162.56" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="U$40" gate="+5V" pin="+5V"/>
<wire x1="99.06" y1="218.44" x2="99.06" y2="220.98" width="0.1524" layer="91"/>
<pinref part="IC11" gate="A" pin="OUTPUT"/>
<wire x1="99.06" y1="220.98" x2="96.52" y2="220.98" width="0.1524" layer="91"/>
<junction x="99.06" y="220.98"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="U$8" gate="+5V" pin="+5V"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="U$16" gate="+5V" pin="+5V"/>
<pinref part="IC2" gate="A" pin="VDD"/>
<wire x1="-177.8" y1="48.26" x2="-185.42" y2="48.26" width="0.1524" layer="91"/>
<junction x="-185.42" y="48.26"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="U$27" gate="+5V" pin="+5V"/>
<pinref part="IC3" gate="A" pin="VCC"/>
<wire x1="-38.1" y1="45.72" x2="-33.02" y2="45.72" width="0.1524" layer="91"/>
<junction x="-33.02" y="45.72"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="U$44" gate="+5V" pin="+5V"/>
<pinref part="IC6" gate="A" pin="VCC"/>
<wire x1="48.26" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<junction x="53.34" y="48.26"/>
</segment>
<segment>
<pinref part="U$35" gate="+5V" pin="+5V"/>
<wire x1="111.76" y1="220.98" x2="111.76" y2="215.9" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="INPUT"/>
<wire x1="111.76" y1="215.9" x2="116.84" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<junction x="111.76" y="215.9"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="IC11" gate="A" pin="INPUT"/>
<wire x1="43.18" y1="226.06" x2="60.96" y2="226.06" width="0.1524" layer="91"/>
<wire x1="60.96" y1="226.06" x2="66.04" y2="226.06" width="0.1524" layer="91"/>
<wire x1="66.04" y1="226.06" x2="66.04" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="60.96" y1="223.52" x2="60.96" y2="226.06" width="0.1524" layer="91"/>
<junction x="60.96" y="226.06"/>
<pinref part="J1" gate="P" pin="1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC4" gate="B" pin="VCAP_2"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="152.4" y1="91.44" x2="154.94" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="157.48" x2="-15.24" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="157.48" x2="-20.32" y2="157.48" width="0.1524" layer="91"/>
<pinref part="IC4" gate="A" pin="VREF+"/>
<wire x1="-10.16" y1="157.48" x2="-15.24" y2="157.48" width="0.1524" layer="91"/>
<junction x="-15.24" y="157.48"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="-20.32" y="157.48"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="-40.64" y1="157.48" x2="-35.56" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="-35.56" y1="157.48" x2="-33.02" y2="157.48" width="0.1524" layer="91"/>
<junction x="-35.56" y="157.48"/>
<pinref part="IC4" gate="A" pin="VDDA"/>
<wire x1="-10.16" y1="160.02" x2="-12.7" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="160.02" x2="-35.56" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="160.02" x2="-35.56" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="162.56" x2="-12.7" y2="160.02" width="0.1524" layer="91"/>
<junction x="-12.7" y="160.02"/>
<pinref part="C5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="IC4" gate="A" pin="VCAP_1"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="35.56" y1="91.44" x2="38.1" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="48.26" y1="124.46" x2="50.8" y2="124.46" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="2"/>
<wire x1="50.8" y1="124.46" x2="55.88" y2="124.46" width="0.1524" layer="91"/>
<wire x1="50.8" y1="121.92" x2="50.8" y2="124.46" width="0.1524" layer="91"/>
<junction x="50.8" y="124.46"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="55.88" y1="114.3" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
<pinref part="IC4" gate="A" pin="PH0-OSC_IN"/>
<wire x1="50.8" y1="114.3" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<wire x1="35.56" y1="114.3" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="1"/>
<wire x1="50.8" y1="116.84" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
<junction x="50.8" y="114.3"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC4" gate="A" pin="PH1-OSC_OUT"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="35.56" y1="124.46" x2="38.1" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VDD_1.2"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-172.72" y1="109.22" x2="-177.8" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-177.8" y1="109.22" x2="-187.96" y2="109.22" width="0.1524" layer="91"/>
<junction x="-177.8" y="109.22"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="VDDA_3.3"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="-121.92" y1="109.22" x2="-119.38" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="-119.38" y1="109.22" x2="-111.76" y2="109.22" width="0.1524" layer="91"/>
<junction x="-119.38" y="109.22"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-111.76" y1="109.22" x2="-106.68" y2="109.22" width="0.1524" layer="91"/>
<junction x="-111.76" y="109.22"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="REXT"/>
<wire x1="-172.72" y1="99.06" x2="-175.26" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="-175.26" y1="99.06" x2="-175.26" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ETH_RST" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RST#"/>
<wire x1="-172.72" y1="119.38" x2="-187.96" y2="119.38" width="0.1524" layer="91"/>
<label x="-187.96" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PE13"/>
<wire x1="35.56" y1="134.62" x2="58.42" y2="134.62" width="0.1524" layer="91"/>
<label x="47.752" y="134.874" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TXP"/>
<wire x1="-121.92" y1="165.1" x2="-111.76" y2="165.1" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="165.1" x2="-111.76" y2="175.26" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="175.26" x2="-106.68" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="-106.68" y1="175.26" x2="-83.82" y2="175.26" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="177.8" x2="-106.68" y2="175.26" width="0.1524" layer="91"/>
<junction x="-106.68" y="175.26"/>
<pinref part="CONN1" gate="A" pin="1"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TXM"/>
<wire x1="-121.92" y1="162.56" x2="-114.3" y2="162.56" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="162.56" x2="-114.3" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="170.18" x2="-104.14" y2="170.18" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="-104.14" y1="170.18" x2="-83.82" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="177.8" x2="-104.14" y2="170.18" width="0.1524" layer="91"/>
<junction x="-104.14" y="170.18"/>
<pinref part="CONN1" gate="A" pin="3"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RXP"/>
<wire x1="-121.92" y1="160.02" x2="-109.22" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="160.02" x2="-109.22" y2="167.64" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="167.64" x2="-101.6" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="-101.6" y1="167.64" x2="-83.82" y2="167.64" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="177.8" x2="-101.6" y2="167.64" width="0.1524" layer="91"/>
<junction x="-101.6" y="167.64"/>
<pinref part="CONN1" gate="A" pin="4"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RXM"/>
<wire x1="-121.92" y1="157.48" x2="-111.76" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="157.48" x2="-111.76" y2="162.56" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="162.56" x2="-99.06" y2="162.56" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="-99.06" y1="162.56" x2="-83.82" y2="162.56" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="177.8" x2="-99.06" y2="162.56" width="0.1524" layer="91"/>
<junction x="-99.06" y="162.56"/>
<pinref part="CONN1" gate="A" pin="6"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="CRS_DV/ADD10"/>
<wire x1="-177.8" y1="157.48" x2="-172.72" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="RXD0"/>
<wire x1="-177.8" y1="154.94" x2="-172.72" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="RXD1"/>
<wire x1="-177.8" y1="152.4" x2="-172.72" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="RXER"/>
<wire x1="-177.8" y1="149.86" x2="-172.72" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="REF_CLK"/>
<wire x1="-177.8" y1="134.62" x2="-172.72" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TXEN" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TXEN"/>
<wire x1="-172.72" y1="144.78" x2="-205.74" y2="144.78" width="0.1524" layer="91"/>
<label x="-199.136" y="146.812" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="111.76" x2="-38.1" y2="111.76" width="0.1524" layer="91"/>
<label x="-38.1" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="TXD0" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TXD0"/>
<wire x1="-172.72" y1="142.24" x2="-205.74" y2="142.24" width="0.1524" layer="91"/>
<label x="-199.136" y="144.272" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="99.06" y1="121.92" x2="83.82" y2="121.92" width="0.1524" layer="91"/>
<label x="83.82" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="TXD1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="TXD1"/>
<wire x1="-172.72" y1="139.7" x2="-205.74" y2="139.7" width="0.1524" layer="91"/>
<label x="-199.136" y="141.732" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="99.06" y1="119.38" x2="83.82" y2="119.38" width="0.1524" layer="91"/>
<label x="83.82" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="REFCLK" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="-187.96" y1="134.62" x2="-205.74" y2="134.62" width="0.1524" layer="91"/>
<label x="-196.088" y="136.652" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PA1"/>
<wire x1="-10.16" y1="142.24" x2="-38.1" y2="142.24" width="0.1524" layer="91"/>
<label x="-38.1" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXER" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="-187.96" y1="149.86" x2="-205.74" y2="149.86" width="0.1524" layer="91"/>
<label x="-199.136" y="151.892" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PB10"/>
<wire x1="-10.16" y1="114.3" x2="-38.1" y2="114.3" width="0.1524" layer="91"/>
<label x="-38.1" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXD1" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="-187.96" y1="152.4" x2="-205.74" y2="152.4" width="0.1524" layer="91"/>
<label x="-199.136" y="154.432" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PC5"/>
<wire x1="35.56" y1="167.64" x2="58.42" y2="167.64" width="0.1524" layer="91"/>
<label x="52.324" y="167.894" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXD0" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="-187.96" y1="154.94" x2="-205.74" y2="154.94" width="0.1524" layer="91"/>
<label x="-199.136" y="156.972" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PC4"/>
<wire x1="35.56" y1="170.18" x2="58.42" y2="170.18" width="0.1524" layer="91"/>
<label x="52.324" y="170.434" size="1.778" layer="95"/>
</segment>
</net>
<net name="CRS_DV" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="-187.96" y1="157.48" x2="-205.74" y2="157.48" width="0.1524" layer="91"/>
<label x="-195.834" y="159.512" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PA7"/>
<wire x1="-10.16" y1="127" x2="-38.1" y2="127" width="0.1524" layer="91"/>
<label x="-38.1" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="-93.98" y1="177.8" x2="-93.98" y2="172.72" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="172.72" x2="-83.82" y2="172.72" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="165.1" x2="-93.98" y2="165.1" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="172.72" x2="-93.98" y2="165.1" width="0.1524" layer="91"/>
<junction x="-93.98" y="172.72"/>
<junction x="-93.98" y="165.1"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="-93.98" y1="165.1" x2="-93.98" y2="157.48" width="0.1524" layer="91"/>
<pinref part="CONN1" gate="A" pin="2"/>
<pinref part="CONN1" gate="A" pin="5"/>
</segment>
</net>
<net name="ETH_ACT" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="LED0/ANEN_SPEED"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="-121.92" y1="137.16" x2="-101.6" y2="137.16" width="0.1524" layer="91"/>
<label x="-116.84" y="137.414" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="CONN1" gate="A" pin="K1"/>
<wire x1="-91.44" y1="137.16" x2="-83.82" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="CONN1" gate="A" pin="K"/>
<wire x1="-91.44" y1="142.24" x2="-83.82" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ETH_LINK" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-101.6" y1="142.24" x2="-116.84" y2="142.24" width="0.1524" layer="91"/>
<label x="-116.84" y="142.494" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PE15"/>
<wire x1="35.56" y1="129.54" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
<label x="46.228" y="129.794" size="1.778" layer="95"/>
</segment>
</net>
<net name="MDIO" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="MDIO"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="-172.72" y1="165.1" x2="-180.34" y2="165.1" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="165.1" x2="-180.34" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="165.1" x2="-205.74" y2="165.1" width="0.1524" layer="91"/>
<junction x="-180.34" y="165.1"/>
<label x="-205.74" y="165.354" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PA2"/>
<wire x1="-10.16" y1="139.7" x2="-38.1" y2="139.7" width="0.1524" layer="91"/>
<label x="-38.1" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="MDC" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="MDC"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="-172.72" y1="162.56" x2="-187.96" y2="162.56" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="162.56" x2="-187.96" y2="170.18" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="162.56" x2="-205.74" y2="162.56" width="0.1524" layer="91"/>
<junction x="-187.96" y="162.56"/>
<label x="-205.74" y="162.814" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="PC1"/>
<wire x1="35.56" y1="177.8" x2="58.42" y2="177.8" width="0.1524" layer="91"/>
<label x="53.848" y="178.054" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC4" gate="A" pin="PB11"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="111.76" x2="-15.24" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="IC4" gate="B" pin="PB12"/>
<wire x1="109.22" y1="121.92" x2="111.76" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="IC4" gate="B" pin="PB13"/>
<wire x1="109.22" y1="119.38" x2="111.76" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS485_1_RX" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PA_10"/>
<wire x1="111.76" y1="157.48" x2="83.82" y2="157.48" width="0.1524" layer="91"/>
<label x="83.82" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="R"/>
<wire x1="-73.66" y1="55.88" x2="-93.98" y2="55.88" width="0.1524" layer="91"/>
<label x="-93.98" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS485_1_TX" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PA_9"/>
<wire x1="111.76" y1="160.02" x2="83.82" y2="160.02" width="0.1524" layer="91"/>
<label x="83.82" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="D"/>
<wire x1="-73.66" y1="60.96" x2="-93.98" y2="60.96" width="0.1524" layer="91"/>
<label x="-93.98" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="CAN_RX" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PA_11"/>
<wire x1="111.76" y1="154.94" x2="83.82" y2="154.94" width="0.1524" layer="91"/>
<label x="83.82" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="A" pin="RXD"/>
<wire x1="-177.8" y1="63.5" x2="-198.12" y2="63.5" width="0.1524" layer="91"/>
<label x="-198.12" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="CAN_TX" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PA_12"/>
<wire x1="111.76" y1="152.4" x2="83.82" y2="152.4" width="0.1524" layer="91"/>
<label x="83.82" y="152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="A" pin="TXD"/>
<wire x1="-177.8" y1="60.96" x2="-198.12" y2="60.96" width="0.1524" layer="91"/>
<label x="-198.12" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS485_1_CTRL" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PA_8"/>
<wire x1="111.76" y1="162.56" x2="83.82" y2="162.56" width="0.1524" layer="91"/>
<label x="83.82" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="DE"/>
<pinref part="IC3" gate="A" pin="~RE"/>
<wire x1="-73.66" y1="45.72" x2="-73.66" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="45.72" x2="-93.98" y2="45.72" width="0.1524" layer="91"/>
<junction x="-73.66" y="45.72"/>
<label x="-93.98" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS485_2_TX" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PC6"/>
<wire x1="111.76" y1="109.22" x2="83.82" y2="109.22" width="0.1524" layer="91"/>
<label x="83.82" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC6" gate="A" pin="D"/>
<wire x1="12.7" y1="63.5" x2="-7.62" y2="63.5" width="0.1524" layer="91"/>
<label x="-7.62" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS485_2_RX" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PC7"/>
<wire x1="111.76" y1="106.68" x2="83.82" y2="106.68" width="0.1524" layer="91"/>
<label x="83.82" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC6" gate="A" pin="R"/>
<wire x1="12.7" y1="58.42" x2="-7.62" y2="58.42" width="0.1524" layer="91"/>
<label x="-7.62" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS485_2_CTRL" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PC8"/>
<wire x1="111.76" y1="104.14" x2="83.82" y2="104.14" width="0.1524" layer="91"/>
<label x="83.82" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC6" gate="A" pin="DE"/>
<pinref part="IC6" gate="A" pin="~RE"/>
<wire x1="12.7" y1="48.26" x2="12.7" y2="45.72" width="0.1524" layer="91"/>
<wire x1="12.7" y1="48.26" x2="-7.62" y2="48.26" width="0.1524" layer="91"/>
<junction x="12.7" y="48.26"/>
<label x="-7.62" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB_DM" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PB14"/>
<wire x1="111.76" y1="116.84" x2="83.82" y2="116.84" width="0.1524" layer="91"/>
<label x="83.82" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="180.34" y1="55.88" x2="162.56" y2="55.88" width="0.1524" layer="91"/>
<label x="162.56" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB_DP" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PB15"/>
<wire x1="111.76" y1="114.3" x2="83.82" y2="114.3" width="0.1524" layer="91"/>
<label x="83.82" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="172.72" y1="53.34" x2="162.56" y2="53.34" width="0.1524" layer="91"/>
<label x="162.56" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PB7"/>
<wire x1="111.76" y1="129.54" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
<label x="83.82" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="SDA"/>
<wire x1="-160.02" y1="200.66" x2="-162.56" y2="200.66" width="0.1524" layer="91"/>
<label x="-182.88" y="200.66" size="1.778" layer="95"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-162.56" y1="200.66" x2="-182.88" y2="200.66" width="0.1524" layer="91"/>
<wire x1="-162.56" y1="208.28" x2="-162.56" y2="200.66" width="0.1524" layer="91"/>
<junction x="-162.56" y="200.66"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PB6"/>
<wire x1="111.76" y1="132.08" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<label x="83.82" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="SCL"/>
<wire x1="-160.02" y1="205.74" x2="-172.72" y2="205.74" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="205.74" x2="-177.8" y2="205.74" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="205.74" x2="-182.88" y2="205.74" width="0.1524" layer="91"/>
<label x="-182.88" y="205.74" size="1.778" layer="95"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="-172.72" y1="208.28" x2="-172.72" y2="205.74" width="0.1524" layer="91"/>
<junction x="-172.72" y="205.74"/>
</segment>
</net>
<net name="UART_RX" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PD2"/>
<wire x1="152.4" y1="177.8" x2="175.26" y2="177.8" width="0.1524" layer="91"/>
<label x="164.338" y="178.054" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="116.84" y1="50.8" x2="99.06" y2="50.8" width="0.1524" layer="91"/>
<pinref part="MOD1" gate="A" pin="UART0_TX"/>
<label x="99.06" y="50.8" size="1.778" layer="95"/>
<pinref part="JP2" gate="A" pin="1"/>
<wire x1="83.82" y1="55.88" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<wire x1="93.98" y1="55.88" x2="93.98" y2="50.8" width="0.1524" layer="91"/>
<wire x1="93.98" y1="50.8" x2="99.06" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART_TX" class="0">
<segment>
<pinref part="IC4" gate="B" pin="PC12"/>
<wire x1="111.76" y1="93.98" x2="83.82" y2="93.98" width="0.1524" layer="91"/>
<label x="83.82" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="116.84" y1="53.34" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
<pinref part="MOD1" gate="A" pin="UART0_RX"/>
<label x="99.06" y="53.34" size="1.778" layer="95"/>
<pinref part="JP2" gate="A" pin="2"/>
<wire x1="83.82" y1="58.42" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
<wire x1="96.52" y1="58.42" x2="96.52" y2="53.34" width="0.1524" layer="91"/>
<wire x1="96.52" y1="53.34" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="K"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="45.72" y1="157.48" x2="53.34" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="K"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="45.72" y1="152.4" x2="53.34" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="IC4" gate="A" pin="PE2"/>
<wire x1="38.1" y1="162.56" x2="35.56" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<pinref part="IC4" gate="A" pin="PE4"/>
<wire x1="38.1" y1="157.48" x2="35.56" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<pinref part="IC4" gate="A" pin="PE6"/>
<wire x1="38.1" y1="152.4" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="IC2" gate="A" pin="RS"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="-177.8" y1="55.88" x2="-187.96" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="IC2" gate="A" pin="VREF"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="-142.24" y1="43.18" x2="-139.7" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="-139.7" y1="43.18" x2="-139.7" y2="40.64" width="0.1524" layer="91"/>
<junction x="-139.7" y="43.18"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="IC2" gate="A" pin="CANH"/>
<wire x1="-142.24" y1="58.42" x2="-137.16" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="-132.08" y1="55.88" x2="-137.16" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="55.88" x2="-137.16" y2="58.42" width="0.1524" layer="91"/>
<pinref part="X2" gate="-3" pin="KL"/>
<wire x1="-132.08" y1="55.88" x2="-119.38" y2="55.88" width="0.1524" layer="91"/>
<junction x="-132.08" y="55.88"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="IC2" gate="A" pin="CANL"/>
<wire x1="-142.24" y1="63.5" x2="-137.16" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="63.5" x2="-137.16" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="-137.16" y1="66.04" x2="-132.08" y2="66.04" width="0.1524" layer="91"/>
<pinref part="X2" gate="-1" pin="KL"/>
<wire x1="-132.08" y1="66.04" x2="-119.38" y2="66.04" width="0.1524" layer="91"/>
<junction x="-132.08" y="66.04"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="IC3" gate="A" pin="A"/>
<pinref part="X3" gate="-1" pin="KL"/>
<wire x1="-38.1" y1="63.5" x2="-33.02" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="IC3" gate="A" pin="B"/>
<pinref part="X3" gate="-2" pin="KL"/>
<wire x1="-38.1" y1="58.42" x2="-33.02" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="IC6" gate="A" pin="A"/>
<pinref part="X4" gate="-1" pin="KL"/>
<wire x1="48.26" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="IC6" gate="A" pin="B"/>
<pinref part="X4" gate="-2" pin="KL"/>
<wire x1="48.26" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB_VBUS" class="0">
<segment>
<pinref part="CONN2" gate="G$1" pin="1"/>
<wire x1="193.04" y1="58.42" x2="162.56" y2="58.42" width="0.1524" layer="91"/>
<label x="162.56" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="PD8"/>
<wire x1="152.4" y1="162.56" x2="175.26" y2="162.56" width="0.1524" layer="91"/>
<label x="162.56" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="CONN2" gate="G$1" pin="2"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="193.04" y1="55.88" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="CONN2" gate="G$1" pin="3"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="193.04" y1="53.34" x2="182.88" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="IC4" gate="B" pin="BOOT0"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="152.4" y1="119.38" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<wire x1="165.1" y1="119.38" x2="165.1" y2="116.84" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="2"/>
<wire x1="172.72" y1="121.92" x2="165.1" y2="121.92" width="0.1524" layer="91"/>
<wire x1="165.1" y1="121.92" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<junction x="165.1" y="119.38"/>
</segment>
</net>
<net name="NRELOAD" class="0">
<segment>
<pinref part="MOD1" gate="A" pin="NRELOAD"/>
<wire x1="116.84" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<label x="99.06" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="PD5"/>
<wire x1="152.4" y1="170.18" x2="175.26" y2="170.18" width="0.1524" layer="91"/>
<label x="162.56" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="WIFI_RESETN" class="0">
<segment>
<pinref part="MOD1" gate="A" pin="EXT_RESETN"/>
<wire x1="116.84" y1="55.88" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
<label x="99.06" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="PD4"/>
<wire x1="152.4" y1="172.72" x2="175.26" y2="172.72" width="0.1524" layer="91"/>
<label x="157.48" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWR_SW" class="0">
<segment>
<pinref part="MOD1" gate="A" pin="PWR_SW"/>
<wire x1="116.84" y1="48.26" x2="99.06" y2="48.26" width="0.1524" layer="91"/>
<label x="99.06" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="PC11"/>
<wire x1="111.76" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<label x="83.82" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="NLINK" class="0">
<segment>
<pinref part="MOD1" gate="A" pin="NLINK"/>
<wire x1="116.84" y1="40.64" x2="99.06" y2="40.64" width="0.1524" layer="91"/>
<label x="99.06" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="PA15"/>
<wire x1="111.76" y1="144.78" x2="83.82" y2="144.78" width="0.1524" layer="91"/>
<label x="83.82" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="NREADY" class="0">
<segment>
<pinref part="MOD1" gate="A" pin="NREADY"/>
<wire x1="116.84" y1="43.18" x2="99.06" y2="43.18" width="0.1524" layer="91"/>
<label x="99.06" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="PC10"/>
<wire x1="111.76" y1="99.06" x2="83.82" y2="99.06" width="0.1524" layer="91"/>
<label x="83.82" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="25MHZ_PHY" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="XI"/>
<wire x1="-121.92" y1="129.54" x2="-101.6" y2="129.54" width="0.1524" layer="91"/>
<label x="-116.84" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC4" gate="B" pin="PC9"/>
<wire x1="111.76" y1="101.6" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
<label x="83.82" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
