#ifndef __dynamicInit_h
#define __dynamicInit_h
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_hal.h"

#include "RingBuffer.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"



typedef enum
{
    devRS485 = 0,
    devCAN = 1,
    devUSB = 2,
    devETH = 3,
    devWIFI = 4,
}devType;

typedef enum
{
    modeSTREAM = 0,
    modeFRAME = 1,

}transferMode;

typedef enum
{
    protoTCP = 0,
    protoUDP = 1,

}ETHproto;

typedef enum
{
    sockDISCONNECTED = 0,
    sockCONNECTED = 1,

}sockState;

typedef enum
{
    dirSRC = 0,
    dirDST = 1,

}EndpointDir;

typedef struct
{
    int (*rx)(uint8_t* rx, uint32_t len, void* devstruct);

    devType dev;
    void* devstruct;

}RxEndpoint;

typedef struct
{
    int (*tx)(uint8_t* tx, uint32_t len, void* devstruct);
    devType dev;

    void* devstruct;

}TxEndpoint;


typedef struct
{
    RxEndpoint RX;
    TxEndpoint TX;
}Node;

typedef struct
{
    uint8_t ifnum;
    transferMode mode;

}devRxRS485;

typedef struct
{
    uint16_t canId;
    xQueueHandle msgbox;

}devRxCAN;

typedef struct
{
    int unused;

}devRxUSB;

typedef struct
{
    ETHproto proto;
    uint16_t port;
    int sock;
    int newSock;
    sockState state;

}devRxETH;

typedef struct
{
    uint32_t header;
    uint8_t ifnum;
    transferMode mode;

}devTxRS485;

typedef struct
{
    uint16_t canId;
    uint8_t numDataBytes;
    uint8_t bufpos;
    uint8_t buffer[8];
    xQueueHandle msgbox;

}devTxCAN;

typedef struct
{
    uint32_t header;
    transferMode mode;

}devTxUSB;

typedef struct
{
    ETHproto proto;
    uint16_t port;
    struct ip_addr ipaddr;
    int sock;
    int newSock;
    sockState state;

}devTxETH;

#define CONFIG_START_ADDR   (0x08004004)
#define CONFIG_LENGTH_ADDR  (0x08004000)
#define CONFIG_LENGTH_MAX   (0x4000)
#define CONFIG_SECTOR_NUM     (FLASH_SECTOR_1)

int parseAndInit(uint32_t* data, uint16_t len);


#ifdef __cplusplus
}
#endif
#endif /*__dynamicInit_h */
