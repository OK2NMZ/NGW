#ifndef __dynamicInitHandlers_h
#define __dynamicInitHandlers_h
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_hal.h"
#include "dynamicInit.h"


int sendRS485_1_Frame(uint8_t* tx, uint32_t len, void* devstruct);
int sendRS485_1_Stream(uint8_t* tx, uint32_t len, void* devstruct);
int sendRS485_2_Frame(uint8_t* tx, uint32_t len, void* devstruct);
int sendRS485_2_Stream(uint8_t* tx, uint32_t len, void* devstruct);
int sendCAN(uint8_t* tx, uint32_t len, void* devstruct);
int sendUSBStream(uint8_t* tx, uint32_t len, void* devstruct);
int sendUSBFrame(uint8_t* tx, uint32_t len, void* devstruct);
int sendETHTCP(uint8_t* tx, uint32_t len, void* devstruct);
int sendETHUDP(uint8_t* tx, uint32_t len, void* devstruct);
int sendWIFI(uint8_t* tx, uint32_t len, void* devstruct);
int sendCAN(uint8_t* tx, uint32_t len, void* devstruct);

int recvRS485_1_Frame(uint8_t* rx, uint32_t len, void* devstruct);
int recvRS485_1_Stream(uint8_t* rx, uint32_t len, void* devstruct);
int recvRS485_2_Frame(uint8_t* rx, uint32_t len, void* devstruct);
int recvRS485_2_Stream(uint8_t* rx, uint32_t len, void* devstruct);
int recvCAN(uint8_t* rx, uint32_t len, void* devstruct);
int recvUSB(uint8_t* rx, uint32_t len, void* devstruct);
int recvETHTCP(uint8_t* rx, uint32_t len, void* devstruct);
int recvETHUDP(uint8_t* rx, uint32_t len, void* devstruct);
int recvWIFI(uint8_t* rx, uint32_t len, void* devstruct);
int recvCANstream(uint8_t* rx, uint32_t len, void* devstruct);
int recvCANframe(uint8_t* rx, uint32_t len, void* devstruct);
int recvCANthru(uint8_t* rx, uint32_t len, void* devstruct);

int initETH(EndpointDir dir, void* dev);
int initCAN(uint16_t speed);


#ifdef __cplusplus
}
#endif
#endif /*__dynamicInitHandlers_h */
