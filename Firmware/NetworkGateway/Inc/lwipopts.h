
/**
  ******************************************************************************
  * File Name          : lwipopts.h
  * Date               : 19/07/2014 19:19:41
  * Description        : This file overrides LwIP stack default configuration
  *                      done in opt.h file.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2014 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LWIPOPTS__H__
#define __LWIPOPTS__H__

#include "stm32f2xx_hal.h"

/* Within 'USER CODE' section, code will be kept by default at each generation */
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

#ifdef __cplusplus
 extern "C" {
#endif

/*----- Default Value for WITH_RTOS:  -----*/
#define WITH_RTOS   1

/**
 * SYS_LIGHTWEIGHT_PROT==1: if you want inter-task protection for certain
 * critical regions during buffer allocation, deallocation and memory
 * allocation and deallocation.
 */
#define SYS_LIGHTWEIGHT_PROT    1

#define ETHARP_TRUST_IP_MAC     0
#define IP_REASSEMBLY           1
#define IP_FRAG                 1
#define ARP_QUEUEING            0
#define TCP_LISTEN_BACKLOG      1

/**
 * NO_SYS==1: Provides VERY minimal functionality. Otherwise,
 * use lwIP facilities.
 */
#define NO_SYS                  0

/* ---------- Memory options ---------- */
/* MEM_ALIGNMENT: should be set to the alignment of the CPU for which
   lwIP is compiled. 4 byte alignment -> define MEM_ALIGNMENT to 4, 2
   byte alignment -> define MEM_ALIGNMENT to 2. */
#define MEM_ALIGNMENT           4

/* MEM_SIZE: the size of the heap memory. If the application will send
a lot of data that needs to be copied, this should be set high. */
#define MEM_SIZE                (65536)

/* MEMP_NUM_PBUF: the number of memp struct pbufs. If the application
   sends a lot of data out of ROM (or other static memory), this
   should be set high. */
#define MEMP_NUM_PBUF           128
/* MEMP_NUM_UDP_PCB: the number of UDP protocol control blocks. One
   per active UDP "connection". */
#define MEMP_NUM_UDP_PCB        8
/* MEMP_NUM_TCP_PCB_LISTEN: the number of listening TCP
   connections. */
#define MEMP_NUM_TCP_PCB_LISTEN 16
/* MEMP_NUM_TCP_SEG: the number of simultaneously queued TCP
   segments. */
#define MEMP_NUM_TCP_SEG        512


#define LWIP_SO_RCVTIMEO            1  /* support timeouts for socket recv */
#define LWIP_SO_SNDTIMEO            1  /* support timeouts for socket send */
#define LWIP_TCP_TIMESTAMPS         1
#define LWIP_SO_RCVBUF              1  /* enable SO_RCVBUF */
#define SO_REUSE                    1  /* enable SO_REUSE */
#define LWIP_WND_SCALE              1  /* enable window scaling */
#define TCP_RCV_SCALE               2  /* receive scale factor IETF RFC 1323 */


/* PBUF_POOL_BUFSIZE: the size of each pbuf in the pbuf pool. */
#define PBUF_POOL_BUFSIZE       512


/* ---------- TCP options ---------- */
#define LWIP_TCP                1
#define TCP_TTL                 255

/* Controls if TCP should queue segments that arrive out of
   order. Define to 0 if your device is low on memory. */
#define TCP_QUEUE_OOSEQ         0

/* TCP Maximum segment size. */
#define TCP_MSS                 (1460)	  /* TCP_MSS = (Ethernet MTU - IP header size - TCP header size) */

/*
 * The window scale option (http://tools.ietf.org/html/rfc1323) patch of lwIP
 * definitely works solely for the receive window, not for the send window.
 * Setting the send window size to the maximum of an 16bit value, 65535,
 * or multiple of it (x * 65536 - 1) results in the same performance.
 * Everything else decrease performance.
 */
#define TCP_SND_BUF                 (0xFFFF)

#define TCP_SND_QUEUELEN            ((32 * (TCP_SND_BUF) + (TCP_MSS - 1))/(TCP_MSS))

#define RECV_BUFSIZE_DEFAULT        128 * 1024

#define PBUF_POOL_SIZE             1024


/* TCP receive window. */
#define TCP_WND                     (4*TCP_MSS)

/* per http://savannah.nongnu.org/patch/?8312 */
#define TCP_OVERSIZE                          TCP_MSS
//! #define LWIP_NETIF_TX_SINGLE_PBUF             1

#define MEMP_MEM_MALLOC 1

/*
 * We reduce the maximum segment lifetime from one minute to one second to
 * avoid queuing up PCBs in TIME-WAIT state. This is the state, PCBs end up
 * after closing a TCP connection socket at the server side. The number of PCBs
 * in this state is apparently not limited by the value of 'MEMP_NUM_TCP_PCB'.
 * One allocation costs around 160 bytes. If clients connect to the server at a
 * high rate, those allocations accumulate quickly and thereby may exhaust the
 * memory of the server. By reducing the segment lifetime, PCBs in TIME-WAIT
 * state are cleaned up from the 'tcp_tw_pcbs' queue in a more timely fashion
 * (by 'tcp_slowtmr()').
 */
#define TCP_MSL 1000UL

#define MEMP_NUM_SYS_TIMEOUT        16
#define MEMP_NUM_TCP_PCB           128
#define MEMP_NUM_NETCONN (MEMP_NUM_TCP_PCB + MEMP_NUM_UDP_PCB + MEMP_NUM_RAW_PCB + MEMP_NUM_TCP_PCB_LISTEN - 1)


/* ---------- ICMP options ---------- */
#define LWIP_ICMP                       1


/* ---------- DHCP options ---------- */
/* Define LWIP_DHCP to 1 if you want DHCP configuration of
   interfaces. DHCP is not implemented in lwIP 0.5.1, however, so
   turning this on does currently not work. */
#define LWIP_DHCP               0 //!!! for debugging


/* ---------- UDP options ---------- */
#define LWIP_UDP                1
#define UDP_TTL                 255


/* ---------- Statistics options ---------- */
#define LWIP_STATS 0
#define LWIP_PROVIDE_ERRNO 1


/*
   --------------------------------------
   ---------- Checksum options ----------
   --------------------------------------
*/

/*
The STM32F2x7 allows computing and verifying the IP, UDP, TCP and ICMP checksums by hardware:
 - To use this feature let the following define uncommented.
 - To disable it and process by CPU comment the  the checksum.
*/
#define CHECKSUM_BY_HARDWARE


#ifdef CHECKSUM_BY_HARDWARE
/* CHECKSUM_GEN_IP==0: Generate checksums by hardware for outgoing ICMP packets.*/
  #define CHECKSUM_GEN_ICMP                0
  /* CHECKSUM_GEN_IP==0: Generate checksums by hardware for outgoing IP packets.*/
  #define CHECKSUM_GEN_IP                 0
  /* CHECKSUM_GEN_UDP==0: Generate checksums by hardware for outgoing UDP packets.*/
  #define CHECKSUM_GEN_UDP                0
  /* CHECKSUM_GEN_TCP==0: Generate checksums by hardware for outgoing TCP packets.*/
  #define CHECKSUM_GEN_TCP                0
  /* CHECKSUM_CHECK_IP==0: Check checksums by hardware for incoming IP packets.*/
  #define CHECKSUM_CHECK_IP               0
  /* CHECKSUM_CHECK_UDP==0: Check checksums by hardware for incoming UDP packets.*/
  #define CHECKSUM_CHECK_UDP              0
  /* CHECKSUM_CHECK_TCP==0: Check checksums by hardware for incoming TCP packets.*/
  #define CHECKSUM_CHECK_TCP              0
#else
/* CHECKSUM_GEN_IP==1: Generate checksums in software for outgoing ICMP packets.*/
  #define CHECKSUM_GEN_ICMP                1
  /* CHECKSUM_GEN_IP==1: Generate checksums in software for outgoing IP packets.*/
  #define CHECKSUM_GEN_IP                 1
  /* CHECKSUM_GEN_UDP==1: Generate checksums in software for outgoing UDP packets.*/
  #define CHECKSUM_GEN_UDP                1
  /* CHECKSUM_GEN_TCP==1: Generate checksums in software for outgoing TCP packets.*/
  #define CHECKSUM_GEN_TCP                1
  /* CHECKSUM_CHECK_IP==1: Check checksums in software for incoming IP packets.*/
  #define CHECKSUM_CHECK_IP               1
  /* CHECKSUM_CHECK_UDP==1: Check checksums in software for incoming UDP packets.*/
  #define CHECKSUM_CHECK_UDP              1
  /* CHECKSUM_CHECK_TCP==1: Check checksums in software for incoming TCP packets.*/
  #define CHECKSUM_CHECK_TCP              1
#endif



/**
 * LWIP_CHECKSUM_ON_COPY==1: Calculate checksum when copying data from
 * application buffers to pbufs.
 */
#define LWIP_CHECKSUM_ON_COPY           1


/*
   ----------------------------------------------
   ---------- Sequential layer options ----------
   ----------------------------------------------
*/
/**
 * LWIP_NETCONN==1: Enable Netconn API (require to use api_lib.c)
 */
#define LWIP_NETCONN                    1

/*
   ------------------------------------
   ---------- Socket options ----------
   ------------------------------------
*/
/**
 * LWIP_SOCKET==1: Enable Socket API (require to use sockets.c)
 */
#define LWIP_SOCKET                     1


#define LWIP_DNS 1 //enable DNS
#define LWIP_NETIF_HOSTNAME 1 //enable DHCP hostname visibility


/*
   -----------------------------------
   ---------- DEBUG options ----------
   -----------------------------------
*/


#define LWIP_DEBUG 1
#define LWIP_DBG_TYPES_ON (LWIP_DBG_TRACE|LWIP_DBG_STATE|LWIP_DBG_FRESH|LWIP_DBG_HALT)
#define LWIP_DBG_MIN_LEVEL LWIP_DBG_LEVEL_ALL

#if LWIP_DEBUG

#define ICMP_DEBUG          LWIP_DBG_OFF
#define ETHARP_DEBUG        LWIP_DBG_OFF
#define NETIF_DEBUG         LWIP_DBG_OFF
#define PBUF_DEBUG          LWIP_DBG_OFF
#define API_LIB_DEBUG       LWIP_DBG_OFF
#define INET_DEBUG          LWIP_DBG_OFF
#define IP_DEBUG            LWIP_DBG_OFF
#define IP_REASS_DEBUG      LWIP_DBG_OFF
#define MEM_DEBUG           LWIP_DBG_OFF
#define MEMP_DEBUG          LWIP_DBG_OFF
#define UDP_DEBUG           LWIP_DBG_OFF
#define TCPIP_DEBUG         LWIP_DBG_OFF
#define DHCP_DEBUG          LWIP_DBG_OFF
#define ICMP_DEBUG          LWIP_DBG_OFF
#define IP_DEBUG            LWIP_DBG_OFF
#define TCP_CWND_DEBUG      LWIP_DBG_ON
#define TCP_RST_DEBUG       LWIP_DBG_ON
#define TCPIP_DEBUG         LWIP_DBG_OFF
#endif


/*
   ---------------------------------
   ---------- OS options ----------
   ---------------------------------
*/

#define TCPIP_THREAD_STACKSIZE          2048
#define DEFAULT_UDP_RECVMBOX_SIZE       2000
#define DEFAULT_TCP_RECVMBOX_SIZE       2000
#define DEFAULT_THREAD_STACKSIZE        1536
#define TCPIP_THREAD_PRIO               (configMAX_PRIORITIES - 2)
#define TCPIP_THREAD_NAME              "TCPIP"


#define DEFAULT_ACCEPTMBOX_SIZE   128
#define TCPIP_MBOX_SIZE           128

#define LWIP_ALLOW_MEM_FREE_FROM_OTHER_CONTEXT 1

#ifdef __cplusplus
}
#endif
#endif /*__ LWIPOPTS__H_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
