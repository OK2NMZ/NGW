#ifndef _lcthw_RingBuffer_h
#define _lcthw_RingBuffer_h

typedef struct
{
    char *buffer;
    int b_length;
    int b_start;
    int b_end;

} RingBuffer;

RingBuffer *RingBuffer_create(int length);

void RingBuffer_destroy(RingBuffer *buffer);

int RingBuffer_read(RingBuffer *buffer, char *target, int amount);

int RingBuffer_write(RingBuffer *buffer, char *data, int length);

int RingBuffer_empty(RingBuffer *buffer);

int RingBuffer_full(RingBuffer *buffer);

int RingBuffer_available_data(RingBuffer *buffer);

int RingBuffer_available_space(RingBuffer *buffer);

#define RingBuffer_available_data(B) (((B)->b_end + 1) % (B)->b_length - (B)->b_start - 1)

#define RingBuffer_available_space(B) ((B)->b_length - (B)->b_end - 1)

#define RingBuffer_full(B) (RingBuffer_available_data((B)) - (B)->b_length == 0)

#define RingBuffer_empty(B) (RingBuffer_available_data((B)) == 0)

#define RingBuffer_puts(B, D) RingBuffer_write((B), bdata((D)), blength((D)))

#define RingBuffer_starts_at(B) ((B)->buffer + (B)->b_start)

#define RingBuffer_ends_at(B) ((B)->buffer + (B)->b_end)

#define RingBuffer_commit_read(B, A) ((B)->b_start = ((B)->b_start + (A)) % (B)->b_length)

#define RingBuffer_commit_write(B, A) ((B)->b_end = ((B)->b_end + (A)) % (B)->b_length)

#define RingBuffer_flush(B) (RingBuffer_commit_read(B, RingBuffer_available_data(B)))

#endif
