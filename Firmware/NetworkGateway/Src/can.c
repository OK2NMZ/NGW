/**
  ******************************************************************************
  * File Name          : CAN.c
  * Date               : 19/07/2014 19:19:40
  * Description        : This file provides code for the configuration
  *                      of the CAN instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2014 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "can.h"
#include "gpio.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

CAN_HandleTypeDef hcan1;
xSemaphoreHandle  MutexCAN1;
volatile RingBuffer* RB_CAN1;
uint8_t CANCurrNum = 0;
uint8_t canisInitialized = 0;
xQueueHandle CANthruMsgbox;
uint8_t CANhasThru = 0;

canEndpointEntry canEndpointEntries[CAN_MAX_NUM];
CanTxMsgTypeDef CanTxDescriptor = {0, 0, CAN_ID_STD, CAN_RTR_DATA, 8, {1, 2, 3, 4, 5, 6, 7, 8}};
CanRxMsgTypeDef CanRxDescriptor;
xSemaphoreHandle CANsema = NULL;

void canRouterThread(void const * argument)
{
    canMessageEntry msg;
    int i;

    //wait for CAN init
    xSemaphoreTake(CANsema, portMAX_DELAY); //deplete the semaphore
    xSemaphoreTake(CANsema, portMAX_DELAY); //wait for first "give"

    while(1)
    {
#if 0
        if(RingBuffer_available_data(RB_CAN1) >= 10)
        {
            RingBuffer_read((RingBuffer*)RB_CAN1, (char*)&msg.stdId, 2);
            RingBuffer_read((RingBuffer*)RB_CAN1, (char*)&(msg.len), 1);
            RingBuffer_read((RingBuffer*)RB_CAN1, (char*)(msg.data), 8);
            for(i=0; i<CANCurrNum; i++)
            {
                if(msg.stdId == canEndpointEntries[i].stdId)
                {
                    xQueueSend(canEndpointEntries[i].msgbox, (void*)(&msg), portMAX_DELAY);
                    break;
                }
            }
            if(i == CANCurrNum)
            {
                if(CANhasThru)
                {
                    xQueueSend(CANthruMsgbox, (void*)(&msg), portMAX_DELAY);
                }
            }
        }
        else
        {
            taskYIELD();
        }
#else
        HAL_CAN_Receive_IT(&hcan1, 0);
        xSemaphoreTake(CANsema, portMAX_DELAY);
        msg.stdId = hcan1.pRxMsg->StdId;
        msg.len = hcan1.pRxMsg->DLC;
        msg.data[0] = hcan1.pRxMsg->Data[0];
        msg.data[1] = hcan1.pRxMsg->Data[1];
        msg.data[2] = hcan1.pRxMsg->Data[2];
        msg.data[3] = hcan1.pRxMsg->Data[3];
        msg.data[4] = hcan1.pRxMsg->Data[4];
        msg.data[5] = hcan1.pRxMsg->Data[4];
        msg.data[6] = hcan1.pRxMsg->Data[6];
        msg.data[7] = hcan1.pRxMsg->Data[7];
        for(i=0; i<CANCurrNum; i++)
        {
            if(msg.stdId == canEndpointEntries[i].stdId)
            {
                xQueueSend(canEndpointEntries[i].msgbox, (void*)(&msg), portMAX_DELAY);
                break;
            }
        }
        if(i == CANCurrNum)
        {
            if(CANhasThru)
            {
                xQueueSend(CANthruMsgbox, (void*)(&msg), portMAX_DELAY);
            }
        }
#endif
    }
}

/**
  * @brief  Transmission  complete callback in non blocking mode
  * @param  hcan: pointer to a CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @retval None
  */
void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef* hcan) /* Only CAN1 is active */
{
#if 0
    volatile uint8_t stdId;
    if(RingBuffer_available_space(RB_CAN1) >= 10)
    {
        stdId = (uint8_t)((hcan->pRxMsg->StdId)&0xFF);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)(&stdId), 1);
        stdId = (uint8_t)((hcan->pRxMsg->StdId>>8)&0xFF);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)(&stdId), 1);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)&(hcan->pRxMsg->DLC), 1);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)&(hcan->pRxMsg->Data[0]), 1);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)&(hcan->pRxMsg->Data[1]), 1);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)&(hcan->pRxMsg->Data[2]), 1);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)&(hcan->pRxMsg->Data[3]), 1);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)&(hcan->pRxMsg->Data[4]), 1);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)&(hcan->pRxMsg->Data[5]), 1);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)&(hcan->pRxMsg->Data[6]), 1);
        RingBuffer_write((RingBuffer*)RB_CAN1, (char*)&(hcan->pRxMsg->Data[7]), 1);
    }
    HAL_CAN_Receive_IT(hcan, 0);
#else
	volatile signed portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(CANsema, &xHigherPriorityTaskWoken);
    if(xHigherPriorityTaskWoken)
    {
        taskYIELD();
    }
#endif
}



/* CAN1 init function */
void MX_CAN1_Init(void)
{
  int i;
  for(i=0; i < CAN_MAX_NUM; i++)
  {
    canEndpointEntries[i].stdId = 0xFFFFFFFF;
    canEndpointEntries[i].msgbox = xQueueCreate(8, sizeof(canMessageEntry));
  }
  CANCurrNum = 0;

  CANthruMsgbox = xQueueCreate(8, sizeof(canMessageEntry));

  RB_CAN1 = (volatile RingBuffer*)RingBuffer_create(CAN_BUFFER_SIZE);
  MutexCAN1 = xSemaphoreCreateMutex();

  vSemaphoreCreateBinary(CANsema);
  if(CANsema == NULL)
  {
    return;
  }


  osThreadDef(CAN, canRouterThread, osPriorityNormal, 0, 4*configMINIMAL_STACK_SIZE);
  osThreadCreate (osThread(CAN), NULL);

  hcan1.pTxMsg = &CanTxDescriptor;
  hcan1.pRxMsg = &CanRxDescriptor;
 /* while(1)
  {
      HAL_Delay(3000);
      HAL_CAN_Transmit(&hcan1, HAL_MAX_DELAY);
  }
  */

}

void HAL_CAN_MspInit(CAN_HandleTypeDef* hcan)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hcan->Instance==CAN1)
  {
    /* Peripheral clock enable */
    __CAN1_CLK_ENABLE();

    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_11 | GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* Peripheral interrupt init*/
    /* Sets the priority grouping field */
    HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
    HAL_NVIC_SetPriority(CAN1_RX1_IRQn, 7, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX1_IRQn);
    /* Sets the priority grouping field */
    HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 8, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);

  }
}

void HAL_CAN_MspDeInit(CAN_HandleTypeDef* hcan)
{

  if(hcan->Instance==CAN1)
  {
    /* Peripheral clock disable */
    __CAN1_CLK_DISABLE();

    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_11|GPIO_PIN_12);


    /* Peripheral interrupt Deinit*/
    HAL_NVIC_DisableIRQ(CAN1_RX1_IRQn);
    HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);

  }
}



/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
