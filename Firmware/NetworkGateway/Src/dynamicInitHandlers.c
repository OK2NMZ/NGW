/**
  ******************************************************************************
  * @file    dynamicInitHandlers.c
  * @author  Marek NOVAK
  * @version v1.0
  * @date    2014
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, AUTHOR SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2014 Marek NOVAK (ok2nmz@gmail.com)</center></h2>
  ******************************************************************************
  */

#include "stdint.h"
#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "string.h"
#include "tlsf.h"
#include "usbd_cdc_if.h"
#include "usart.h"
#include "dynamicInit.h"
#include "dynamicInitHandlers.h"
#include "usb_device.h"
#include "RingBuffer.h"
#include "can.h"


int recvUSB(uint8_t* rx, uint32_t len, void* devstruct)
{
    extern RingBuffer* RB_USB;
    int recved = 0;

    while(RingBuffer_empty(RB_USB))
    {
        taskYIELD();
    }

    if(RingBuffer_available_data(RB_USB) >= len)
    {
        recved = len;
    }
    else
    {
        recved = RingBuffer_available_data(RB_USB);
    }

    RingBuffer_read(RB_USB, (char*)rx, (int)recved);

    return recved;
}

int sendUSBStream(uint8_t* tx, uint32_t len, void* devstruct)
{
    xSemaphoreTake(MutexUSB, portMAX_DELAY);
    CDC_Transmit_HS(tx, (uint16_t)(len));
    xSemaphoreGive(MutexUSB);
    return 0;
}

int sendUSBFrame(uint8_t* tx, uint32_t len, void* devstruct)
{
    devTxUSB* devUSB = devstruct;
    xSemaphoreTake(MutexUSB, portMAX_DELAY);
    /*No need for synchronization byte 0xA5 here*/
    CDC_Transmit_HS((uint8_t*)&len, 2);
    CDC_Transmit_HS((uint8_t*)&(devUSB->header), 4);
    CDC_Transmit_HS(tx, (uint16_t)(len));
    /*No need for checksum word here*/
    xSemaphoreGive(MutexUSB);
    return 0;
}

int recvETHTCP(uint8_t* rx, uint32_t len, void* devstruct)
{
    int rh_size;
    struct sockaddr_in remotehost;
    int recved = 0;

    devRxETH* devTCP = devstruct;

    switch(devTCP->state)
    {
        default:
        case sockDISCONNECTED:
            rh_size = sizeof(remotehost);
            devTCP->newSock = accept(devTCP->sock, (struct sockaddr *)&remotehost, (socklen_t *)&rh_size);
            devTCP->state = sockCONNECTED;

        case sockCONNECTED:
            recved = read(devTCP->newSock, rx, len);
        break;
    }

    if(recved <= 0)
    {
        devTCP->state = sockDISCONNECTED;
        close(devTCP->newSock);
        recved = 0;
    }

    return recved;
}

int recvETHUDP(uint8_t* rx, uint32_t len, void* devstruct)
{
    int recved = 0;

    devRxETH* devUDP = devstruct;

    recved = recv(devUDP->sock, rx, len, 0);

    if(recved <= 0)
    {
        recved = 0;
    }

    return recved;
}

int sendETHTCP(uint8_t* tx, uint32_t len, void* devstruct)
{
    int rh_size;
    struct sockaddr_in remotehost;
    int dataSent = 0;
    devTxETH* devTCP = devstruct;
agn:
    switch(devTCP->state)
    {
        default:
        case sockDISCONNECTED:
            remotehost.sin_family = AF_INET;
            remotehost.sin_addr.s_addr = (u32_t)(devTCP->ipaddr.addr);
            remotehost.sin_port = htons(devTCP->port);

            rh_size = sizeof(remotehost);
            if(connect(devTCP->sock, (struct sockaddr *)&remotehost, (socklen_t)rh_size) >= 0)
            {
                devTCP->state = sockCONNECTED;
            }
            else
            {
                devTCP->state = sockDISCONNECTED;
                close(devTCP->sock);
                initETH(dirDST, devTCP);
                goto agn;
            }
        case sockCONNECTED:
            while(len != 0)
            {
                dataSent = send(devTCP->sock, tx, len, 0);
                if(dataSent < 0 )
                {
                    //fail
                    devTCP->state = sockDISCONNECTED;
                    close(devTCP->sock);
                    initETH(dirDST, devTCP);
                    goto agn;
                }
                else
                {
                    len -= dataSent;
                    tx += dataSent;
                }
            }
        break;
    }

    return 0;
}


int sendETHUDP(uint8_t* tx, uint32_t len, void* devstruct)
{
    int rh_size;
    struct sockaddr_in remotehost;
    int dataSent = 0;
    devTxETH* devUDP = devstruct;


    remotehost.sin_family = AF_INET;
    remotehost.sin_addr.s_addr = (u32_t)(devUDP->ipaddr.addr);
    remotehost.sin_port = htons(devUDP->port);
    rh_size = sizeof(remotehost);

    while(len != 0)
    {
        dataSent = sendto(devUDP->sock, tx, len, 0, (struct sockaddr *)&remotehost, (socklen_t)rh_size);
        if(dataSent < 0 )
        {
            //fail
            return -1;
        }
        else
        {
            len -= dataSent;
            tx += dataSent;
        }
    }
    //! close(devTCP->sock);

    return 0;
}

int sendRS485_1_Frame(uint8_t* tx, uint32_t len, void* devstruct)
{
    uint8_t sync = 0xA5;
    devTxRS485* devRS485 = devstruct;
    uint16_t chs = 0;
    uint32_t i = 0;

    xSemaphoreTake(MutexUART1, portMAX_DELAY);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);

    HAL_UART_Transmit(&huart1, &sync, 1, 0xFFFFFFFF);
    HAL_UART_Transmit(&huart1, (uint8_t*)&len, 2, 0xFFFFFFFF);
    HAL_UART_Transmit(&huart1, (uint8_t*)&(devRS485->header), 4, 0xFFFFFFFF);
    HAL_UART_Transmit(&huart1, tx, (uint16_t)len, 0xFFFFFFFF);
    while(i < len) //calculate the checksum
    {
        chs += tx[i++];
    }
    HAL_UART_Transmit(&huart1, (uint8_t*)&chs, 2, 0xFFFFFFFF);

    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
    xSemaphoreGive(MutexUART1);
    return 0;
}
int sendRS485_1_Stream(uint8_t* tx, uint32_t len, void* devstruct)
{
    xSemaphoreTake(MutexUART1, portMAX_DELAY);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
    HAL_UART_Transmit(&huart1, tx, (uint16_t)len, 0xFFFFFFFF);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
    xSemaphoreGive(MutexUART1);
    return 0;
}

int sendRS485_2_Frame(uint8_t* tx, uint32_t len, void* devstruct)
{
    uint8_t sync = 0xA5;
    devTxRS485* devRS485 = devstruct;
    uint16_t chs = 0;
    uint32_t i = 0;

    xSemaphoreTake(MutexUART6, portMAX_DELAY);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);

    HAL_UART_Transmit(&huart6, &sync, 1, 0xFFFFFFFF);
    HAL_UART_Transmit(&huart6, (uint8_t*)&len, 2, 0xFFFFFFFF);
    HAL_UART_Transmit(&huart6, (uint8_t*)&(devRS485->header), 4, 0xFFFFFFFF);
    HAL_UART_Transmit(&huart6, tx, (uint16_t)len, 0xFFFFFFFF);
    while(i < len) //calculate the checksum
    {
        chs += tx[i++];
    }
    HAL_UART_Transmit(&huart6, (uint8_t*)&chs, 2, 0xFFFFFFFF);

    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
    xSemaphoreGive(MutexUART6);
    return 0;
}

int sendRS485_2_Stream(uint8_t* tx, uint32_t len, void* devstruct)
{
    xSemaphoreTake(MutexUART6, portMAX_DELAY);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
    HAL_UART_Transmit(&huart6, tx, (uint16_t)len, 0xFFFFFFFF);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
    xSemaphoreGive(MutexUART6);
    return 0;
}

int sendWIFI(uint8_t* tx, uint32_t len, void* devstruct)
{
    xSemaphoreTake(MutexUART5, portMAX_DELAY);
    HAL_UART_Transmit(&huart5, tx, (uint16_t)len, 0xFFFFFFFF);
    xSemaphoreGive(MutexUART5);
    return 0;
}


int recvRS485_Frame(uint8_t* rx, uint32_t len, void* devstruct, RingBuffer* RB)
{
#define RS485_FRAME_TIMEOUT (3)
#define CHECK_TIMEOUT(x) if((xTaskGetTickCount()-timestamp)>(RS485_FRAME_TIMEOUT*x)){goto timeout;}
    uint16_t recvlen;
    uint16_t recvchs;
    uint16_t chs = 0;
    uint16_t i = 0;
    uint32_t header;
    uint32_t timestamp;
    do
    {
        while(RingBuffer_empty(RB))
        {
            taskYIELD();
        }
        RingBuffer_read(RB, (char*)rx, 1);

    }while(rx[0] != 0xA5);

    timestamp = xTaskGetTickCount();

    while(RingBuffer_available_data(RB) < 6)
    {
        CHECK_TIMEOUT(6);
        taskYIELD();
    }

    RingBuffer_read(RB, (char*)&recvlen, 2);
    RingBuffer_read(RB, (char*)&header, 4); /* ignore control byte in RX*/

    if(((recvlen+2) > len)||(recvlen+8 > UART_BUFFER_SIZE))
    {
        RingBuffer_flush(RB);
        return 0; //drop too long packets
    }

    timestamp = xTaskGetTickCount();

    while(RingBuffer_available_data(RB) < (recvlen+2))
    {
        CHECK_TIMEOUT(recvlen+2);
        taskYIELD();
    }

    RingBuffer_read(RB, (char*)rx, (int)recvlen);
    RingBuffer_read(RB, (char*)&recvchs, 2);
    while(i < recvlen) chs += rx[i++];
    if(chs == recvchs)
    {
        return recvlen;
    }
    else
    {
timeout:
        RingBuffer_flush(RB);
        return 0; //drop bad packets
    }
}

int recvRS485_1_Frame(uint8_t* rx, uint32_t len, void* devstruct)
{
    return recvRS485_Frame(rx, len, devstruct, (RingBuffer*)RB_UART1);
}

int recvRS485_1_Stream(uint8_t* rx, uint32_t len, void* devstruct)
{
    int recved = 0;
    uint16_t available;

    while(RingBuffer_empty(RB_UART1))
    {
        taskYIELD();
    }

    do
    {
        available = RingBuffer_available_data(RB_UART1);
        if(available > (UART_BUFFER_SIZE/2))
        {
            break;
        }
        vTaskDelay(UART_STREAM_DEFRAG_DELAY_TICK);
    }
    while(available != RingBuffer_available_data(RB_UART1));

    if(RingBuffer_available_data(RB_UART1) >= len)
    {
        recved = len;
    }
    else
    {
        recved = RingBuffer_available_data(RB_UART1);
    }

    RingBuffer_read((RingBuffer*)RB_UART1, (char*)rx, (int)recved);

    return recved;
}


int recvRS485_2_Frame(uint8_t* rx, uint32_t len, void* devstruct)
{
    return recvRS485_Frame(rx, len, devstruct, (RingBuffer*)RB_UART6);
}

int recvRS485_2_Stream(uint8_t* rx, uint32_t len, void* devstruct)
{
    int recved = 0;
    uint16_t available;

    while(RingBuffer_empty(RB_UART6))
    {
        taskYIELD();
    }

    do
    {
        available = RingBuffer_available_data(RB_UART6);
        if(available > (UART_BUFFER_SIZE/2))
        {
            break;
        }
        vTaskDelay(UART_STREAM_DEFRAG_DELAY_TICK);
    }
    while(available != RingBuffer_available_data(RB_UART6));

    if(RingBuffer_available_data(RB_UART6) >= len)
    {
        recved = len;
    }
    else
    {
        recved = RingBuffer_available_data(RB_UART6);
    }

    RingBuffer_read((RingBuffer*)RB_UART6, (char*)rx, (int)recved);

    return recved;
}


int recvWIFI(uint8_t* rx, uint32_t len, void* devstruct)
{
    int recved = 0;
    uint16_t available;

    while(RingBuffer_empty(RB_UART5))
    {
        taskYIELD();
    }

    do
    {
        available = RingBuffer_available_data(RB_UART5);
        if(available > (UART_BUFFER_SIZE/2))
        {
            break;
        }
        vTaskDelay(1);
    }
    while(available != RingBuffer_available_data(RB_UART5));

    if(RingBuffer_available_data(RB_UART5) >= len)
    {
        recved = len;
    }
    else
    {
        recved = RingBuffer_available_data(RB_UART5);
    }

    RingBuffer_read((RingBuffer*)RB_UART5, (char*)rx, (int)recved);

    return recved;
}

int initETH(EndpointDir dir, void* dev)
{
    struct sockaddr_in address;
    if(dir == dirSRC)
    {
        devRxETH* eth = dev;

        if(eth->proto == protoTCP)
        {
            /* create a TCP socket */
            if ((eth->sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
            {
                /*can not create socket*/
                return -1;
            }
        }
        else //protoUDP
        {
            /* create a UDP socket */
            if ((eth->sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
            {
                /*can not create socket*/
                return -1;
            }
        }

        /* bind to port at any interface */
        address.sin_family = AF_INET;
        address.sin_port = htons(eth->port);
        address.sin_addr.s_addr = INADDR_ANY;

        if (bind(eth->sock, (struct sockaddr *)&address, sizeof (address)) < 0)
        {
            /* can not bind socket */
            return -1;
        }


        if(eth->proto == protoTCP)
        {
            /* listen for incoming connections (TCP listen backlog = 1) */
            listen(eth->sock, 1);
        }
        return 0;
    }
    else //dirDST
    {
        devTxETH* eth = dev;

        if(eth->proto == protoTCP)
        {
            /* create a TCP socket */
            if ((eth->sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
            {
                /*can not create socket*/
                return -1;
            }
        }
        else //protoUDP
        {
            /* create a UDP socket */
            if ((eth->sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
            {
                /*can not create socket*/
                return -1;
            }
        }

        /* bind to port at any interface */
        address.sin_family = AF_INET;
        address.sin_port = 0; //chooses any available
        address.sin_addr.s_addr = INADDR_ANY;

        if (bind(eth->sock, (struct sockaddr *)&address, sizeof (address)) < 0)
        {
            /* can not bind socket */
            return -1;
        }

        return 0;
    }
}


/***************************************************************************//**
 * @brief  Initializes the CAN1, RX in ISR, 500kBps, FIFO0
 * @param  speed Selected speed in kBps, valid values are:
 *         10, 20, 100, 125, 250, 500, 1000 [kBps]
 * @return int 0 on success, -1 on failure
 ******************************************************************************/
int initCAN(uint16_t speed)
{
    CAN_FilterConfTypeDef hcan1Filter;

    if(CANsema == NULL)
    {
        return -1;
    }

  hcan1.Instance = CAN1;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.TTCM = DISABLE;
  hcan1.Init.ABOM = ENABLE;
  hcan1.Init.AWUM = DISABLE;
  hcan1.Init.NART = DISABLE;
  hcan1.Init.RFLM = DISABLE;
  hcan1.Init.TXFP = ENABLE;
  hcan1.Init.SJW = CAN_SJW_1TQ;

  switch (speed)
  {
  case 10:
    hcan1.Init.Prescaler = 200;
    hcan1.Init.BS1 = CAN_BS1_12TQ;
    hcan1.Init.BS2 = CAN_BS2_2TQ;
    break;

  case 20:
    hcan1.Init.Prescaler = 100;
    hcan1.Init.BS1 = CAN_BS1_12TQ;
    hcan1.Init.BS2 = CAN_BS2_2TQ;
    break;

  case 50:
    hcan1.Init.Prescaler = 40;
    hcan1.Init.BS1 = CAN_BS1_12TQ;
    hcan1.Init.BS2 = CAN_BS2_2TQ;
    break;

  case 100:
    hcan1.Init.Prescaler = 20;
    hcan1.Init.BS1 = CAN_BS1_12TQ;
    hcan1.Init.BS2 = CAN_BS2_2TQ;
    break;

  case 125:
    hcan1.Init.Prescaler = 15;
    hcan1.Init.BS1 = CAN_BS1_13TQ;
    hcan1.Init.BS2 = CAN_BS2_2TQ;
    break;

  case 250:
    hcan1.Init.Prescaler = 8;
    hcan1.Init.BS1 = CAN_BS1_12TQ;
    hcan1.Init.BS2 = CAN_BS2_2TQ;
    break;

  case 500:
    hcan1.Init.Prescaler = 4;
    hcan1.Init.BS1 = CAN_BS1_12TQ;
    hcan1.Init.BS2 = CAN_BS2_2TQ;
    break;

  case 800:
    return -1;

  case 1000:
    hcan1.Init.Prescaler = 2;
    hcan1.Init.BS1 = CAN_BS1_12TQ;
    hcan1.Init.BS2 = CAN_BS2_2TQ;
    break;

  default:
    return -1;
  }

  HAL_CAN_Init(&hcan1);

  hcan1Filter.FilterNumber = 0;                       // Specifies the filter which will be initialized
  hcan1Filter.FilterMode = CAN_FILTERMODE_IDMASK;         // Specifies the filter mode to be initialized.
  hcan1Filter.FilterScale = CAN_FILTERSCALE_32BIT;          // Specifies the filter scale.
  hcan1Filter.FilterIdHigh = 0x00;                      // Specifies the filter identification number (MSBs for a 32-bit configuration, first one for a 16-bit configuration).
  hcan1Filter.FilterIdLow = 0x00;                     // Specifies the filter identification number (LSBs for a 32-bit configuration, second one for a 16-bit configuration).
  hcan1Filter.FilterMaskIdHigh = 0x00;                    // Specifies the filter mask number or identification number, according to the mode (MSBs for a 32-bit configuration, first one for a 16-bit configuration).
  hcan1Filter.FilterMaskIdLow = 0x00;                   // Specifies the filter mask number or identification number, according to the mode (LSBs for a 32-bit configuration, second one for a 16-bit configuration).
  hcan1Filter.FilterFIFOAssignment = 0;                 // Specifies the FIFO (0 or 1) which will be assigned to the filter.
  hcan1Filter.FilterActivation = ENABLE;                  // Enable or disable the filter.
  hcan1Filter.BankNumber = 0;                     // Select the start slave bank filter
  HAL_CAN_ConfigFilter(&hcan1, &hcan1Filter);

  __HAL_CAN_ENABLE_IT(&hcan1, CAN_IT_FMP0);
  xSemaphoreGive(CANsema);
  return 0;
}

int recvCANstream(uint8_t* rx, uint32_t len, void* devstruct)
{
    canMessageEntry msg;
    devRxCAN* devCAN = devstruct;
    uint32_t pos = 0;
    len -= 8;

    do
    {
        xQueueReceive(devCAN->msgbox, &msg, portMAX_DELAY);
        memcpy(&rx[pos], msg.data, msg.len);
        pos += msg.len;
    }while(uxQueueMessagesWaiting(devCAN->msgbox) && (pos <= len));

    return (int)(pos);
}

int recvCANframe(uint8_t* rx, uint32_t len, void* devstruct)
{
    canMessageEntry msg;
    devRxCAN* devCAN = devstruct;

    xQueueReceive(devCAN->msgbox, &msg, portMAX_DELAY);
    rx[0] = (uint8_t)msg.len;
    memcpy(&rx[1], msg.data, msg.len);

    return (int)(msg.len+1);
}

int recvCANthru(uint8_t* rx, uint32_t len, void* devstruct)
{
    canMessageEntry msg;
    devRxCAN* devCAN = devstruct;

    xQueueReceive(devCAN->msgbox, &msg, portMAX_DELAY);
    rx[0] = (uint8_t)msg.len;
    memcpy(&rx[1], (uint8_t*)&msg.stdId, 2);
    memcpy(&rx[3], msg.data, msg.len);

    return (int)(msg.len+3);
}

int sendCAN(uint8_t* tx, uint32_t len, void* devstruct)
{
    uint8_t i;
    devTxCAN* devCAN = devstruct;
    uint32_t pos = 0;


    if(devCAN->bufpos != 0)
    {
        if(len >= (devCAN->numDataBytes-devCAN->bufpos))
        {
            memcpy(&devCAN->buffer[devCAN->bufpos], tx, devCAN->numDataBytes-devCAN->bufpos);
            len -= devCAN->numDataBytes-devCAN->bufpos;
        }
        else
        {
            memcpy(&devCAN->buffer[devCAN->bufpos], tx, len);
            devCAN->bufpos += len;
            return 0;
        }
        xSemaphoreTake(MutexCAN1, portMAX_DELAY);
        hcan1.pTxMsg->DLC = devCAN->numDataBytes;
        hcan1.pTxMsg->StdId = devCAN->canId;
        memcpy(hcan1.pTxMsg->Data, devCAN->buffer, devCAN->numDataBytes);
        HAL_CAN_Transmit(&hcan1, HAL_MAX_DELAY);
        devCAN->bufpos = 0;
    }
    else
    {
        xSemaphoreTake(MutexCAN1, portMAX_DELAY);
        hcan1.pTxMsg->DLC = devCAN->numDataBytes;
        hcan1.pTxMsg->StdId = devCAN->canId;
    }

    while(len >= devCAN->numDataBytes)
    {
        for(i=0;i<devCAN->numDataBytes; i++)
            hcan1.pTxMsg->Data[i] = (uint32_t)tx[pos+i];

        HAL_CAN_Transmit(&hcan1, HAL_MAX_DELAY);
        len -= devCAN->numDataBytes;
        pos += devCAN->numDataBytes;
    }
    xSemaphoreGive(MutexCAN1);
    if(len > 0)
    {
        devCAN->bufpos = len;
        memcpy(devCAN->buffer, &tx[pos], len);
    }
    return 0;
}