/**
  ******************************************************************************
  * @file    dynamicInit.c
  * @author  Marek NOVAK
  * @version v1.0
  * @date    2014
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, AUTHOR SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2014 Marek NOVAK (ok2nmz@gmail.com)</center></h2>
  ******************************************************************************
  */

#include "stdint.h"
#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "string.h"
#include "tlsf.h"
#include "usbd_cdc_if.h"
#include "usart.h"
#include "dynamicInit.h"
#include "dynamicInitHandlers.h"
#include "usb_device.h"
#include "RingBuffer.h"
#include "can.h"


int calcHeader(Node* node, uint32_t* header);

void nodeThread(void const * argument)
{
#define BUF_SIZE 1536
    Node* thisNode = (void*)argument;
    uint8_t thisBuf[BUF_SIZE];
    int recved;

    while(1)
    {
        recved = thisNode->RX.rx(thisBuf, BUF_SIZE, thisNode->RX.devstruct);
        thisNode->TX.tx(thisBuf, recved, thisNode->TX.devstruct);
    }
}

int parseAndInit(uint32_t* data, uint16_t len)
{
    char txt[128];
    uint16_t i;

    Node* node;
    devRxRS485*  rxRS485;
    devRxCAN*    rxCAN;
    devRxETH*    rxETH;
    devTxRS485*  txRS485;
    devTxCAN*    txCAN;
    devTxUSB*    txUSB;
    devTxETH*    txETH;

    osThreadDef(NODE, nodeThread, osPriorityBelowNormal, 0, 5*configMINIMAL_STACK_SIZE);
    i = 0;
    while(i < len)
    {
        node = (Node*)pvPortMalloc(sizeof(Node));
        if(node == NULL)
        {
            return -1;
        }

        /** SOURCE **/
        i++; // src/Dest
        i++; // thisId
        i++; // peerId
        node->RX.dev = (devType)data[i++]; // type of interface
        switch(node->RX.dev)
        {
            case devWIFI:
                node->RX.rx = recvWIFI;
            break;

            case devRS485:
                rxRS485 = (devRxRS485*)pvPortMalloc(sizeof(devRxRS485));
                if(rxRS485 == NULL)
                {
                    return -1;
                }
                rxRS485->ifnum = (uint8_t)data[i++]; // ifNum
                if(rxRS485->ifnum == 0)
                {
                    if(!uart1isInitialized)
                    {
                        uart1isInitialized = 1;
                        huart1.Init.BaudRate = data[i++]; // baud
                        // parity
                        switch(data[i++])
                        {
                            default:
                            case 0:
                                huart1.Init.Parity = UART_PARITY_NONE;
                            break;

                            case 1:
                                huart1.Init.Parity = UART_PARITY_EVEN;
                            break;

                            case 2:
                                huart1.Init.Parity = UART_PARITY_ODD;
                            break;
                        }
                        // stop
                        if(data[i++] == 0)
                        {
                            huart1.Init.StopBits = UART_STOPBITS_1;
                        }
                        else
                        {
                            huart1.Init.StopBits = UART_STOPBITS_2;
                        }
                        huart1.Instance = USART1;
                        huart1.Init.WordLength = UART_WORDLENGTH_8B;
                        huart1.Init.Mode = UART_MODE_TX_RX;
                        huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
                        huart1.Init.OverSampling = UART_OVERSAMPLING_16;
                        HAL_UART_Init(&huart1);
                    }
                    else
                    {
                        i += 3;
                    }

                    HAL_UART_Receive_IT(&huart1, (uint8_t*)&uart1Rx, 1);
                    rxRS485->mode = (transferMode)data[i++]; // mode
                    if(rxRS485->mode == modeFRAME)
                    {
                        node->RX.rx = recvRS485_1_Frame;
                    }
                    else
                    {
                        node->RX.rx = recvRS485_1_Stream;
                    }
                    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
                }
                else //ifnum == 1
                {
                    if(!uart6isInitialized)
                    {
                        uart6isInitialized = 1;
                        huart6.Init.BaudRate = data[i++]; // baud
                        // parity
                        switch(data[i++])
                        {
                            default:
                            case 0:
                                huart6.Init.Parity = UART_PARITY_NONE;
                            break;

                            case 1:
                                huart6.Init.Parity = UART_PARITY_EVEN;
                            break;

                            case 2:
                                huart6.Init.Parity = UART_PARITY_ODD;
                            break;
                        }
                        // stop
                        if(data[i++] == 0)
                        {
                            huart6.Init.StopBits = UART_STOPBITS_1;
                        }
                        else
                        {
                            huart6.Init.StopBits = UART_STOPBITS_2;
                        }
                        huart6.Instance = USART6;
                        huart6.Init.WordLength = UART_WORDLENGTH_8B;
                        huart6.Init.Mode = UART_MODE_TX_RX;
                        huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
                        huart6.Init.OverSampling = UART_OVERSAMPLING_16;
                        HAL_UART_Init(&huart6);
                    }
                    else
                    {
                        i += 3;
                    }

                    HAL_UART_Receive_IT(&huart6, (uint8_t*)&uart6Rx, 1);
                    rxRS485->mode = (transferMode)data[i++]; // mode
                    if(rxRS485->mode == modeFRAME)
                    {
                        node->RX.rx = recvRS485_2_Frame;
                    }
                    else
                    {
                        node->RX.rx = recvRS485_2_Stream;
                    }
                    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
                }
                node->RX.devstruct = rxRS485;
            break;

            case devCAN:
                if(CANCurrNum >= CAN_MAX_NUM)
                {
                    return -1;
                }

                rxCAN = (devRxCAN*)pvPortMalloc(sizeof(devRxCAN));
                if(rxCAN == NULL)
                {
                    return -1;
                }

                if(!canisInitialized)
                {
                    canisInitialized = 1;
                    if(initCAN(data[i++]) != 0)// speed
                    {
                        return -1;
                    }
                }
                else
                {
                    i++;
                }

                rxCAN->canId = data[i++]; //stdId
                i++; //dataByte

                switch(data[i++])
                {
                    case 0:
                        node->RX.rx = recvCANstream;
                        canEndpointEntries[CANCurrNum].stdId = rxCAN->canId;
                        rxCAN->msgbox = canEndpointEntries[CANCurrNum].msgbox;
                        CANCurrNum++;
                    break;
                    case 1:
                        node->RX.rx = recvCANframe;
                        canEndpointEntries[CANCurrNum].stdId = rxCAN->canId;
                        rxCAN->msgbox = canEndpointEntries[CANCurrNum].msgbox;
                        CANCurrNum++;
                    break;
                    case 2:
                        if(CANhasThru)
                        {
                            return -1; //already has thru!
                        }
                        node->RX.rx = recvCANthru;
                        rxCAN->msgbox = CANthruMsgbox;
                        CANhasThru = 1;
                    break;
                    default:
                    return -1;
                }
                node->RX.devstruct = rxCAN;
            break;

            case devUSB:
                i++; // mode
                node->RX.rx = recvUSB;
                node->RX.devstruct = NULL;
            break;

            case devETH:
                rxETH = (devRxETH*)pvPortMalloc(sizeof(devRxETH));
                if(rxETH == NULL)
                {
                    return -1;
                }
                rxETH->proto = (ETHproto)data[i++]; // proto
                rxETH->port = (uint16_t)data[i++]; // port
                i++; // ip addr

                if(rxETH->proto == protoTCP)
                {
                    node->RX.rx = recvETHTCP;
                }
                else //UDP
                {
                    node->RX.rx = recvETHUDP;
                }

                node->RX.devstruct = rxETH;
                initETH(dirSRC, rxETH);
            break;

            default:
            return -1;
        }

        if((data[i] == 0xa5a5a5a5)&&(data[i+1] == 0x5a5a5a5a))
        {
            i += 2;
        }
        else
        {
            return -1;
        }

        /** DESTINATION **/
        i++; // src/Dest
        i++; // thisId
        i++; // peerId
        node->TX.dev = (devType)data[i++]; // type of interface
        switch(node->TX.dev)
        {
            case devWIFI:
                node->TX.tx = sendWIFI;
            break;

            case devRS485:
                txRS485 = (devTxRS485*)pvPortMalloc(sizeof(devTxRS485));
                if(txRS485 == NULL)
                {
                    return -1;
                }
                txRS485->ifnum = (uint8_t)data[i++]; // ifNum
                if(txRS485->ifnum == 0)
                {
                    if(!uart1isInitialized)
                    {
                        uart1isInitialized = 1;
                        huart1.Init.BaudRate = data[i++]; // baud
                        // parity
                        switch(data[i++])
                        {
                            default:
                            case 0:
                                huart1.Init.Parity = UART_PARITY_NONE;
                            break;

                            case 1:
                                huart1.Init.Parity = UART_PARITY_EVEN;
                            break;

                            case 2:
                                huart1.Init.Parity = UART_PARITY_ODD;
                            break;
                        }
                        // stop
                        if(data[i++] == 0)
                        {
                            huart1.Init.StopBits = UART_STOPBITS_1;
                        }
                        else
                        {
                            huart1.Init.StopBits = UART_STOPBITS_2;
                        }
                        huart1.Instance = USART1;
                        huart1.Init.WordLength = UART_WORDLENGTH_8B;
                        huart1.Init.Mode = UART_MODE_TX_RX;
                        huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
                        huart1.Init.OverSampling = UART_OVERSAMPLING_16;
                        HAL_UART_Init(&huart1);
                    }
                    else
                    {
                        i += 3;
                    }
                    txRS485->mode = (transferMode)data[i++]; // mode
                    if(txRS485->mode == modeFRAME)
                    {
                        node->TX.tx = sendRS485_1_Frame;
                        if(calcHeader(node, &(txRS485->header)) != 0)
                        {
                            return -1;
                        }
                    }
                    else
                    {
                        node->TX.tx = sendRS485_1_Stream;
                    }
                }
                else //ifnum == 1
                {
                    if(!uart6isInitialized)
                    {
                        uart6isInitialized = 1;
                        huart6.Init.BaudRate = data[i++]; // baud
                        // parity
                        switch(data[i++])
                        {
                            default:
                            case 0:
                                huart6.Init.Parity = UART_PARITY_NONE;
                            break;

                            case 1:
                                huart6.Init.Parity = UART_PARITY_EVEN;
                            break;

                            case 2:
                                huart6.Init.Parity = UART_PARITY_ODD;
                            break;
                        }
                        // stop
                        if(data[i++] == 0)
                        {
                            huart6.Init.StopBits = UART_STOPBITS_1;
                        }
                        else
                        {
                            huart6.Init.StopBits = UART_STOPBITS_2;
                        }
                        huart6.Instance = USART6;
                        huart6.Init.WordLength = UART_WORDLENGTH_8B;
                        huart6.Init.Mode = UART_MODE_TX_RX;
                        huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
                        huart6.Init.OverSampling = UART_OVERSAMPLING_16;
                        HAL_UART_Init(&huart6);
                    }
                    else
                    {
                        i += 3;
                    }

                    txRS485->mode = (transferMode)data[i++]; // mode
                    if(txRS485->mode == modeFRAME)
                    {
                        node->TX.tx = sendRS485_2_Frame;
                        if(calcHeader(node, &(txRS485->header)) != 0)
                        {
                            return -1;
                        }
                    }
                    else
                    {
                        node->TX.tx = sendRS485_2_Stream;
                    }
                }
                node->TX.devstruct = txRS485;
            break;

            case devCAN: //! todo
                txCAN = (devTxCAN*)pvPortMalloc(sizeof(devTxCAN));
                if(txCAN == NULL)
                {
                    return -1;
                }
                if(!canisInitialized)
                {
                    canisInitialized = 1;
                    if(initCAN(data[i++]) != 0)// speed
                    {
                        return -1;
                    }
                }
                else
                {
                    i++;
                }

                txCAN->canId = data[i++]; //stdId
                txCAN->numDataBytes = data[i++]; //dataByte
                i++; //mode
                txCAN->bufpos = 0;
                node->TX.tx = sendCAN;
                node->TX.devstruct = txCAN;
            break;

            case devUSB:
                txUSB = (devTxUSB*)pvPortMalloc(sizeof(devTxUSB));
                if(txUSB == NULL)
                {
                    return -1;
                }
                txUSB->mode = (transferMode)data[i++]; // mode
                if(txUSB->mode == modeSTREAM)
                {
                    node->TX.tx = sendUSBStream;
                }
                else //modeFRAME
                {
                    node->TX.tx = sendUSBFrame;
                    if(calcHeader(node, &(txUSB->header)) != 0)
                    {
                        return -1;
                    }
                }
                node->TX.devstruct = txUSB;
            break;

            case devETH:
                txETH = (devTxETH*)pvPortMalloc(sizeof(devTxETH));
                if(txETH == NULL)
                {
                    return -1;
                }
                txETH->proto = (ETHproto)data[i++]; // proto
                txETH->port = data[i++]; // port

                if(data[i] > 128)
                {
                    return -1; //too long URL
                }
                memcpy(txt, (char*)&data[i+1], data[i]);
                txt[data[i]] = '\0'; //terminate the URL
                if(netconn_gethostbyname(txt, &(txETH->ipaddr)) != ERR_OK)
                {
                    txETH->ipaddr = (ip_addr_t)ip_addr_broadcast;
                }

    //            sprintf(txt, "%d.%d.%d.%d",
    //                    (int)data[i]&0xFF,
    //                    (int)(data[i]>>8)&0xFF,
    //                    (int)(data[i]>>16)&0xFF,
    //                    (int)(data[i]>>24)&0xFF);
                i+= data[i]/4 + ((data[i]&0x03) !=0) + 1; //skip the length field and character fields

                if(txETH->proto == protoTCP)
                {
                    node->TX.tx = sendETHTCP;
                }
                else
                {
                    node->TX.tx = sendETHUDP;
                }
                initETH(dirDST, txETH);
                node->TX.devstruct = txETH;
            break;

            default:
            return -1;
        }

        if((data[i] == 0xa5a5a5a5)&&(data[i+1] == 0x5a5a5a5a))
        {
            i += 2;
            osThreadCreate (osThread(NODE), node);
        }
        else
        {
            return -1;
        }

    }

    return 0;
}

int calcHeader(Node* node, uint32_t* header)
{
    devRxRS485*  rxRS485;
    devRxCAN*    rxCAN;
    devRxETH*    rxETH;
    *header = node->RX.dev; //lowest byte is the RX dev identifier
    switch(node->RX.dev) //other bytes depend on the source interface
    {
        case devRS485:
            rxRS485 = node->RX.devstruct;
            *header |= ((uint32_t)(rxRS485->ifnum))<<8;
        break;

        case devCAN:
            rxCAN = node->RX.devstruct;
            *header |= ((uint32_t)(rxCAN->canId))<<16;
        break;

        case devUSB:
        break;

        case devETH:
            rxETH = node->RX.devstruct;
            *header |= ((uint32_t)(rxETH->proto))<<8;
            *header |= ((uint32_t)(rxETH->port))<<16;
        break;

        default:
        return -1;
    }
    return 0;
}