#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "RingBuffer.h"
#include "FreeRTOS.h"

/* http://c.learncodethehardway.org/book/ex20.html */
#define NDEBUG
#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...) //fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define clean_errno() //(errno == 0 ? "None" : strerror(errno))
#define log_err(M, ...) //fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define log_warn(M, ...) //fprintf(stderr, "[WARN] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define log_info(M, ...) //fprintf(stderr, "[INFO] (%s:%d) " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define check(A, M, ...) if(!(A)) { log_err(M, ##__VA_ARGS__); goto error; }
#define sentinel(M, ...)  //{ log_err(M, ##__VA_ARGS__); goto error; }
#define check_mem(A) //check((A), "Out of memory.")
#define check_debug(A, M, ...) if(!(A)) { debug(M, ##__VA_ARGS__); goto error; }


RingBuffer *RingBuffer_create(int length)
{
    RingBuffer *buffer = (RingBuffer*)pvPortMalloc(sizeof(RingBuffer));
    memset((void*)buffer, 0, sizeof(RingBuffer));
    buffer->b_length  = length + 1;
    buffer->b_start = 0;
    buffer->b_end = 0;
    buffer->buffer = (char*)pvPortMalloc(buffer->b_length);
    memset((void*)buffer->buffer, 0, buffer->b_length);
    return buffer;
}

void RingBuffer_destroy(RingBuffer *buffer)
{
    if(buffer) {
        vPortFree(buffer->buffer);
        vPortFree(buffer);
    }
}

int RingBuffer_write(RingBuffer *buffer, char *data, int length)
{
    if(RingBuffer_available_data(buffer) == 0) {
        buffer->b_start = buffer->b_end = 0;
    }

    check(length <= RingBuffer_available_space(buffer),
            "Not enough space: %d request, %d available",
            RingBuffer_available_data(buffer), length);

    void *result = memcpy(RingBuffer_ends_at(buffer), data, length);
    check(result != NULL, "Failed to write data into buffer.");

    RingBuffer_commit_write(buffer, length);

    return length;
error:
    return -1;
}

int RingBuffer_read(RingBuffer *buffer, char *target, int amount)
{
    check_debug(amount <= RingBuffer_available_data(buffer),
            "Not enough in the buffer: has %d, needs %d",
            RingBuffer_available_data(buffer), amount);

    void *result = memcpy(target, RingBuffer_starts_at(buffer), amount);
    check(result != NULL, "Failed to write buffer into data.");

    RingBuffer_commit_read(buffer, amount);

    if(buffer->b_end == buffer->b_start) {
        buffer->b_start = buffer->b_end = 0;
    }

    return amount;
error:
    return -1;
}
