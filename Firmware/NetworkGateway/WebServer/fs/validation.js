/*!
* This file is used to validate the entries filled in HTML form before sending off
* its content to a server/client.
*/

/*!
* @brief This function validates the input of can data field.
* @param inpt: input data to validate.
*/
function canDataValidateInput(inpt)
{
  var isOk  = /^[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}$/i.test(inpt.value);
  if(isOk == false)
  {
     inpt.value = "01:23:45:67:89:AB:CD:EF";
     alert("Valid input is: XX:XX:XX:XX:XX:XX:XX:XX, where XX stands for 00-FF hexadecimal digits.");
  }
}


function genericValidateData(elem, regexp, def)
{
  var isOk  = regexp.test(elem.value);
  if(isOk == false)
  {
     elem.value = def;
     alert("Invalid input!");
  }
}

function isInt(value) 
{
  return !isNaN(value) && 
          parseInt(Number(value)) == value && 
          (value + "").replace(/ /g,'') !== "";
}


/* ***** CAN,RS485,ETH,USB validation ***** */
/* RS485 */
function valIfNum(elem, isDst)
{
  var entries = filterEntry(0, isDst, (isDst==1)?0:1);
  var i;
  var form;

  for(i=0; i<entries.length; i++)
  {
    if(entries[i].objData.ifNum == elem.value)
    {
      if(isDst == 0)
      {
        alert("Interface endpoint already used!");
        if(entries.length == 1) /* the other one is free */
        {
          elem.value = (elem.value==1)?0:1;
        }
        return;
      }    
    }
  }

  form = document.forms.pipeCfg;
  if(isDst == 1)
  {
      entries = filterEntry(0, 1, 0);

      for(i=0; i<entries.length; i++)
      {
        if(entries[i].objData.ifNum == elem.value)
        {
            form.dstMode.value = entries[i].objData.mode;
            form.dstBaudrate.value = entries[i].objData.baud;
            form.dstStop.value = entries[i].objData.stop;
            form.dstParity.value = entries[i].objData.parity;
            return;
        }
      }

      entries = [];
  }

  entries = filterEntry(0, (isDst==1)?0:1, isDst);

  for(i=0; i<entries.length; i++)
  {
    if(entries[i].objData.ifNum == elem.value)
    {
      if(isDst == 1)
      {
        form.dstMode.value = entries[i].objData.mode;
        form.dstBaudrate.value = entries[i].objData.baud;
        form.dstStop.value = entries[i].objData.stop;
        form.dstParity.value = entries[i].objData.parity;
        return;
      }
      else
      {
        form.srcMode.value = entries[i].objData.mode;
        form.srcBaudrate.value = entries[i].objData.baud;
        form.srcStop.value = entries[i].objData.stop;
        form.srcParity.value = entries[i].objData.parity;
        return;
      }
    }
  }

}

/*!
* @brief This function validates whether the baudrate of
* talker/listener is the same
* @param elem: baudrate object element
*/
function valBaud(elem, isDst)
{
  var entries = filterEntry(0, 1, 1);
  var i;
  var ifNum = (isDst==1)?document.forms.pipeCfg.dstIfNum.value:document.forms.pipeCfg.srcIfNum.value;

  for(i=0; i<entries.length; i++)
  {
    if(entries[i].objData.ifNum == ifNum)
    {
      if(entries[i].objData.baud != elem.value)
      {
        alert("Cannot change the baudrate of a single endpoint (RX/TX)!");
        elem.value = entries[i].objData.baud;
        return;
      }
    }
  }
}

/*!
* @brief This function validates whether the parity of
* talker/listener is the same
* @param elem: parity object element
*/
function valParity(elem, isDst)
{
  var entries = filterEntry(0, 1, 1);
  var i;
  var ifNum = (isDst==1)?document.forms.pipeCfg.dstIfNum.value:document.forms.pipeCfg.srcIfNum.value;

  for(i=0; i<entries.length; i++)
  {
    if(entries[i].objData.ifNum == ifNum)
    {
      if(entries[i].objData.parity != elem.value)
      {
        alert("Cannot change the parity of a single endpoint (RX/TX)!");
        elem.value = entries[i].objData.parity;
        return;
      }
    }
  }
  
}

/*!
* @brief This function validates whether the number of stop bits
* of talker/listener is the same
* @param elem: number of stop bits object element
*/
function valStop(elem, isDst)
{
  var entries = filterEntry(0, 1, 1);
  var i;
  var ifNum = (isDst==1)?document.forms.pipeCfg.dstIfNum.value:document.forms.pipeCfg.srcIfNum.value;

  for(i=0; i<entries.length; i++)
  {
    if(entries[i].objData.ifNum == ifNum)
    {
      if(entries[i].objData.stop != elem.value)
      {
        alert("Cannot change the number of stop bits of a single endpoint (RX/TX)!");
        elem.value = entries[i].objData.stop;
        return;
      }
    }
  }
}

/* CAN */
/*!
* @brief This function validates the speed settings of CAN bus
* @param elem: CAN speed object element
*/
function valSpeedTest(elem)
{
  var i;
  try
  {
    if(isInt(elem.value)==false)
    {
      elem.value = "125";
      return;
    }
  }
  catch(e)
  {
     elem.value = "125";
  }
}

/*!
* @brief This function validates the speed settings of CAN bus
* @param elem: CAN speed object element
* @param isDst:
*/
function valSpeed(elem, isDst)
{
  var entries = filterEntry(1, 1, 1);
  var i;
  try
  {
    if(isInt(elem.value)==false)
    {
      elem.value = "125";
      return;
    }
  }
  catch(e)
  {
     elem.value = "125";
  }
  for(i=0; i<entries.length; i++)
  {
      if(entries[i].objData.speed != elem.value)
      {
        alert("Cannot change the speed of individual CAN endpoints!");
        elem.value = entries[i].objData.speed;
        return;
      }
  }
}

/*!
* @brief This function validates the standard ID settings of CAN bus
* @param elem: CAN standard ID object element
*/
function valStdIdTest(elem)
{
  try
  {
    if(isInt(elem.value)==false)
    {
      elem.value = "";
      return;
    }
    else if(Number(elem.value) < 0)
    {
      elem.value = "";
      return; 
    }
    else if(Number(elem.value) > 2047)
    {
      alert("Maximum value is 2047!");
      elem.value = "2047"; 
    }
  }
  catch(e)
  {
      elem.value = "";
      return;
  }
}

/*!
* @brief This function validates the standard ID settings of CAN bus
* @param elem: CAN standard ID object element
* @param isDst:
*/
function valStdId(elem, isDst)
{
  try
  {
    if(isInt(elem.value)==false)
    {
      elem.value = "";
      return;
    }
    else if(Number(elem.value) < 0)
    {
      elem.value = "";
      return; 
    }
    else if(Number(elem.value) > 2047)
    {
      alert("Maximum value is 2047!");
      elem.value = "2047"; 
    }
  }
  catch(e)
  {
      elem.value = "";
      return;
  }

  if(isDst == 1)
  {
    return;
  }
  var entries = filterEntry(1, 0, 1);
  var i;

  for(i=0; i<entries.length; i++)
  {
      if(entries[i].objData.stdId == elem.value)
      {
        alert("Cannot use two same source Standard IDs!");
        elem.value = "";
        return;
      }
  }
}

/*!
* @brief This function validates the value of data
* @param elem: data object element
*/
function valDataByte(elem)
{
  try
  {
    if(isInt(elem.value)==false)
    {
      elem.value = "";
      return;
    }
    else if(Number(elem.value) < 0)
    {
      elem.value = "";
      return; 
    }
    else if(Number(elem.value) > 8)
    {
      alert("Maximum value is 8!");
      elem.value = "8"; 
    }
  }
  catch(e)
  {
      elem.value = "";
      return;
  }
}

/* ETH */
/*!
* @brief This function checks whether two port values are not same.
* @param elem: port object element
* @param isDst: 
*/
function valPort(elem, isDst)
{
  if(isInt(elem.value)==false)
  {
    elem.value = "";
    return;
  }
  else if(Number(elem.value) < 0)
  {
    elem.value = "";
    return; 
  }
  else if(Number(elem.value) > 65535)
  {
    alert("Maximum value is 65535!");
    elem.value = "65535"; 
  }

  if(isDst == 1)
  {
    return;
  }

  var entries = filterEntry(3, 0, 1);
  var i;

  for(i=0; i<entries.length; i++)
  {
      if(entries[i].objData.port == elem.value)
      {
        if(entries[i].objData.proto == document.forms.pipeCfg.srcProto.value)
        {
          alert("Cannot use two same source ports!");
          elem.value = "";
          return;
        }
      }
  }
}

/*!
* @brief This function validates the port settings field input
* @param elem: port object element 
*/
function valPortTest(elem)
{
  if(isInt(elem.value)==false)
  {
    elem.value = "";
    return;
  }
  else if(Number(elem.value) < 0)
  {
    elem.value = "";
    return; 
  }
  else if(Number(elem.value) > 65535)
  {
    alert("Maximum value is 65535!");
    elem.value = "65535"; 
  }
}

/*!
* @brief This function validates the IP settings field input
* @param elem: port object element 
*/
function valIP(elem)
{
  var ip = elem.value.split('.');

  for(i=0;i<4;i++)
  {
    if(isInt(ip[i])==false)
    {
      break;
    }
    else if(Number(ip[i]) < 0)
    {
      break;
    }
    else if(Number(ip[i]) > 255)
    {
      break;
    }
  }
  if(i==4)
  {
    if(Number(ip[3]) == 0)
    {
      alert("Cannot use address of a network!");
      elem.value = "255.255.255.255";
      return; 
    }
  }
  else /* resolve the DNS name */
  {
      try /* Send DNS query via MCU */
      {
        var addr = LoadUrl("/dns.html?url="+elem.value);
        if(addr == "")
        {
           alert("Bad URL!");
           elem.value = "255.255.255.255";
        }
        else
        {
          elem.value = addr;
        }
      }
      catch(e)
      {
        elem.value = "255.255.255.255";   
      }
  }
}

/*!
* @brief This function validates the CAN mode settings field input
* @param elem: port object element 
*/
function valCanMode(elem)
{
   if(elem.value == "2")
   {
      if(sessvars.CANhasThru == 1)
      {
        alert("Already have thru endpoint installed!");
        elem.value = "";
        document.forms.pipeCfg.srcStdId.disabled = false;
        return;
      }
      document.forms.pipeCfg.srcStdId.value = "0";
      document.forms.pipeCfg.srcStdId.disabled = true;
   }
   else
   {
      document.forms.pipeCfg.srcStdId.disabled = false;
   }
   return 
}