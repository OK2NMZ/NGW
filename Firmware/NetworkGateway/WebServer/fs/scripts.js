/*!
* This file is used to set configuration of bus via IPv4 interface.
*/

var reloading;
var lastpage;

/*!
* @brief This function sets rs485 configuration parameters.
* @param baud: one of the standard baudrates
* @param parity: odd/even
* @param stop: number of stop bits 
* @param ifNum: number of communication interface
* @param mode: mode of data presentation
*/
function rs485Data(baud, parity, stop, ifNum, mode){
  this.baud = baud;
  this.parity = parity;
  this.stop = stop;
  this.ifNum = ifNum;
  this.mode = mode;
}

/*!
* @brief This function sets CAN configuration parameters.
* @param speed: speed of communication (kbit/s)
* @param stdID: standard ID
* @param dataByte: number of data bytes
* @param mode: mode of data presentation
*/
function canData(speed, stdId, dataByte, mode){
  this.speed = speed;
  this.stdId = stdId;
  this.dataByte = dataByte;
  this.mode = mode;
}

/*!
* @brief This function sets USB configuration parameters.
* @param mode: mode of data presentation
*/
function usbData(mode){
  this.mode = mode;
}

/*!
* @brief This function sets CAN configuration parameters.
* @param proto: used ethernet protocol
* @param port: communication port
* @param host: host IP address
*/
function ethData(proto, port, host){
  this.proto = proto;
  this.port = port;
  this.host = host;
}

/*!
* @brief This function enables to select and submit the entry in field.
* @param srcDest: 
* @param typeIf: type of communication interface (ie: Eth, USB, Wifi)
* @param thisId: 
* @param peerId: 
* @param objData: settings of interface object
*/
function entry(srcDest, typeIf, thisId, peerId, objData) {
    this.srcDest = srcDest;
    this.thisId = thisId;
    this.peerId = peerId;
    this.typeIf = typeIf;
    this.objData = objData;
}

/*!
* @brief This function is called when the page loads
* and sets variables.
*/
function onpageload()
{
    sessvars.entries = [];
    sessvars.entriesNum = 0;
    sessvars.dstIface = "";
    sessvars.srcIface = "";
    sessvars.CANhasThru = 0;
    window.onbeforeunload = null;
    onButtonStatsClick('stats.html', false);
}

/*!
* @brief This function is called when user press button 'Status'.
* This enable user to see current status of communication.
*/
function onButtonStatsClick(url, istmr)
{
    if((url!="stats.html")||(istmr==true))
    {
        try
        {   
            clearTimeout(reloading);
        }
        catch(e)
        {
            ;
        }
    }

    if (window.XMLHttpRequest) 
    { /* Non-IE browsers */
          req = new XMLHttpRequest(); 
          try 
          { 
            req.open("GET", url, false); 
          } 
          catch (e) 
          { 
            alert(e); 
          } 
          req.send(null); 
    } 
    else if (window.ActiveXObject) 
    { /* IE */
          req = new ActiveXObject("Microsoft.XMLHTTP"); 
          if (req) 
          { 
            req.open("GET", url, false); 
            req.send();
          } 
    } 
    
    if (req.readyState == 4) 
    { /* Complete */ 
          if (req.status == 200) 
          { /* OK response */ 
              document.getElementById("main").innerHTML = req.responseText;
              if(url=="stats.html")
              {  
                  if(istmr == true)
                  {
                      reloading=setTimeout("onButtonStatsClick('stats.html', true)", 1000);
                  }  
              } 
          } 
          else 
          { 
            alert("Problem: " + req.statusText); 
          } 
    }
    else
    {
        alert("Problem: " + req.statusText); 
    }
}

/*!
* @brief This function is called when user press button 'Status'.
* This enable user to see current status of communication.
*/
function submitForm(form, url, doReload)
{
  var i;
  var str = "";
    if (window.XMLHttpRequest) 
    { /* Non-IE browsers */ 
          req = new XMLHttpRequest(); 
          try 
          { 
            req.open("POST", url, true); 
          } 
          catch (e) 
          { 
            alert(e); 
          } 
    } 
    else if (window.ActiveXObject) 
    { /* IE */ 
          req = new ActiveXObject("Microsoft.XMLHTTP"); 
          if (req) 
          { 
            req.open("POST", url, true); 

          } 
    } 

    try
    {
      req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      for(i=0; i<form.elements.length; i++)
      {
        if((form.elements[i].name != "")&&(form.elements[i].value != ""))
        {
          str += form.elements[i].name + "=" + form.elements[i].value + "&";
        }
      }
      if(str != "")
      {
        req.send(str);
      }
      else
      {
        return true;
      }

    }
    catch(e)
    {
      ;
    }
    if(doReload==true)
    {
      onButtonStatsClick(url, false);
    }

    return false;
}

/*!
* @brief This function is used to load a page given by specified url.
* @param url: url of page to load.
*/
function LoadUrl(url)
{
     if (window.XMLHttpRequest) 
    { /* Non-IE browsers */ 
          req = new XMLHttpRequest(); 
          try 
          { 
            req.open("GET", url, false); 
          } 
          catch (e) 
          { 
            alert(e); 
          } 
          req.send(null); 
    } 
    else if (window.ActiveXObject) 
    { /* IE */ 
          req = new ActiveXObject("Microsoft.XMLHTTP"); 
          if (req) 
          { 
            req.open("GET", url, false); 
            req.send();
          } 
    } 
    
    if (req.readyState == 4) 
    { /* Complete */ 
          if (req.status == 200) 
          { /* OK response */ 
               return req.responseText;
          } 
          else 
          { 
            alert("Problem: " + req.statusText); 
          } 
    }
    else
    {
        alert("Problem: " + req.statusText); 
    }

    return "";
}

/*!
* @brief This function is used to load a page given by specified url.
* @param url: url of page to load.
*/
function LoadToDiv(url, divName)
{
  document.getElementById(divName).innerHTML = LoadUrl(url);
}

function LoadInterface(elem, dir, divName)
{
  LoadToDiv(dir + "/" + elem.value + ".html", divName);
}

/*!
* @brief This function is called when user press button 'Testing'.
* @param form: 
*/
function onSubmitTesting(form)
{
  if(submitForm(form, 'testing.html', false)==true)
  {
     document.getElementById('iSpecific').innerHTML = "<b>Please select the interface!</b>";
  }
  return false;
}

/*!
* @brief This function is used to save a configuration when all fields of form are
* 		 filled.
* @param form: HTML form
*/
function onCfgAdd(form)
{
  var obj;
  var type;
  var mode;
  var elem;
  var sour;
  var dest;
  var ifaces = ["RS485", "CAN", "USB", "ETH", "WIFI"];

  try
  {
    if(form.srcIf.value == "")
    {
       return false;
    }
    if(form.dstIf.value == "")
    {
       return false;
    }
  }
  catch(e)
  {
      alert("Please enter a pipe configuration now.");
      return true;
  }

  for(elem in form)
  {
    if(isInt(elem))
    {
      if(form[elem].value == undefined)
      {
        alert("Bad settings!");
        return false;
      }

      if(form[elem].value == "")
      {
        alert("Bad settings!");
        return false;
      }

      try
      {
        form[elem].onblur(null);
      }
      catch(e)
      {

      }
    }
  }

  type = form.srcIf.value;
  sour = type;
  if(type == 0)
  {
    try
    {
      if(form.srcIfNum.value == form.dstIfNum.value)
      {
         if((form.srcBaudrate.value != form.dstBaudrate.value)||(form.srcParity.value != form.dstParity.value)||(form.srcStop.value != form.srcStop.value))
         {
           alert("Cannot change baudrate, parity or number of stop bits of a single RS485 endpoint!");
           return false;
         }
      }
    }
    catch(e)
    {

    }
    obj = new rs485Data(form.srcBaudrate.value, form.srcParity.value, form.srcStop.value, form.srcIfNum.value, form.srcMode.value);
  }
  else if(type == 1)
  {
    try
    {
      if(form.srcSpeed.value != form.dstSpeed.value)
      {
         alert("Cannot change the speed of a single CAN endpoint!");
         return false;
      }
    }
    catch(e)
    {
    }
    if(form.srcMode.value == "2")
    {
      obj = new canData(form.srcSpeed.value, "0", "8", "2");  
      sessvars.CANhasThru = 1;
    }
    else
    {
      obj = new canData(form.srcSpeed.value, form.srcStdId.value, "8", form.srcMode.value);  
    }
  }
  else if(type == 2)
  {
    obj = new usbData(form.srcMode.value);
  }
  else if(type == 3)
  {
     obj = new ethData(form.srcProto.value, form.srcPort.value, "");
  }
  else
  {
     obj = 0; /* no data for wifi */
  }

  sessvars.entries[sessvars.entriesNum] = new entry(0, type, sessvars.entriesNum, sessvars.entriesNum+1, obj);
  sessvars.entriesNum++;

  type = form.dstIf.value;
  dest = type;
  if(type == 0)
  {
    obj = new rs485Data(form.dstBaudrate.value, form.dstParity.value, form.dstStop.value, form.dstIfNum.value, form.dstMode.value);
  }
  else if(type == 1)
  {
    obj = new canData(form.dstSpeed.value, form.dstStdId.value, form.dstDataByte.value, "0");
  }
  else if(type == 2)
  {
    obj = new usbData(form.dstMode.value);
  }
  else if(type == 3)
  {
     obj = new ethData(form.dstProto.value, form.dstPort.value, form.dstHost.value);
  }
  else
  {
     obj = 0; /* no data for wifi */
  }
  sessvars.entries[sessvars.entriesNum] = new entry(1, type, sessvars.entriesNum, sessvars.entriesNum-1, obj);
  sessvars.entriesNum++;

  alert("Pipe successfully added! ( " + ifaces[sour] + " -> " + ifaces[dest] + " )");

  window.onbeforeunload = function (e) 
  {
    var message = "If you leave this page without saving the settings, you will lose it! Are you sure to proceed?",
    e = e || window.event;
    /* For IE and Firefox */
    if (e) 
    {
      e.returnValue = message;
    }

    /* For Safari */
    return message;
  };

  return true;
}
/*dec string to hex string, aligned to 32bits*/

/*!
* @brief This function fills a specified field with a value from table selection.
* @param entry: number of table entry 
*/
function addTableEntry(entry)
{
  var retval = "<td>";
  switch(Number(entry.typeIf)) 
  {
    case 0:
    retval += "RS485</td><td>Interface:&nbsp"+(Number(entry.objData.ifNum)+1).toString()+", Baudrate:&nbsp"+entry.objData.baud+", Parity:&nbsp";
    switch(Number(entry.objData.parity))
    {
      case 0:
      retval += "None";
      break;
      case 1:
      retval += "Even";
      break;
      case 2:
      retval += "Odd";
      break;
      default:
      break;
    }
    retval += ", Stop&nbspBits:&nbsp"+(Number(entry.objData.stop)+1).toString()+", Mode:&nbsp";
    if(Number(entry.objData.mode)==0)
    {
      retval += "Stream";
    }
    else
    {
      retval += "Frame"; 
    }  
    break;
    case 1:
    retval += "CAN</td><td>Speed:&nbsp"+entry.objData.speed;
    if(Number(entry.objData.mode) != 2)
    {
      retval += ", Standard&nbspID:&nbsp"+entry.objData.stdId;
    }
    else
    {
      retval += ", Standard&nbspID:&nbspAll";
    }

    if(entry.srcDest == 1)
    {
      retval += ", Data&nbspBytes:&nbsp"+entry.objData.dataByte;
    } 
    else
    {
      switch(Number(entry.objData.mode))
      {
        case 0:
        retval += ", Mode:&nbspStream";
        break;
        case 1:
        retval += ", Mode:&nbspFrame";
        break;
        case 2:
        retval += ", Mode:&nbspThru";
        break;
        default:
        break;
      }
    }
    break;
    case 2:
    retval += "USB</td><td>Mode:&nbsp";
    if(Number(entry.objData.mode)==0)
    {
      retval += "Stream";
    }
    else
    {
      retval += "Frame"; 
    }  
    break;
    case 3:
    retval += "ETH</td><td>Protocol:&nbsp";
    if(Number(entry.objData.proto)==0)
    {
      retval += "TCP";
    }
    else
    {
      retval += "UDP";
    }
    retval += ", Port:&nbsp"+entry.objData.port;

    if(entry.srcDest == 1)
    {
      retval += ", Host:&nbsp"+entry.objData.host;
    }
    break;
    
    case 4:
    retval += "WIFI</td><td>WIFI-LPT100 802.11bgn module";
    break;

    default:
    break;
  }
  retval += "</td>";
  return retval;
}

/*!
* @brief Clears all fileds.
*/
function deleteAll()
{
  var i;
  for(i=0; i<sessvars.entriesNum;i++)
  {
        sessvars.entries[i] = null;
  }
  sessvars.CANhasThru = 0;
}

/*!
* @brief Clear a specified entry.
* @param src:  
*/
function deleteEntry(src, dst)
{
  var i;
  for(i=0; i<sessvars.entriesNum;i++)
  {
    if(sessvars.entries[i]!=null)
    {
      if((sessvars.entries[i].thisId == src)||(sessvars.entries[i].thisId == dst))
      {
        if(sessvars.entries[i].typeIf == 1)
        {
           if(sessvars.entries[i].objData.mode == "2")
           {
              sessvars.CANhasThru = 0;
           }
        }
        sessvars.entries[i] = null;
      }
    }
  }
  if(onCfgShow(true) == false)
  {
    onButtonStatsClick('config.html', false);
  }
}

function onCfgShow(silent)
{
    var i,j,k;
    var max = sessvars.entriesNum;
    var entries = sessvars.entries.slice(0);
    var peer;
    var str = "";
    if(max==0)
    {
        if(!silent)
        {
          alert("No pipes added yet!");
        }
       return false;
    }
    else
    {
      for(k=0;k<max;k++)
      {
        if(entries[k]!=null)
        {
          break;
        }
      }
      if(k==max)
      {
        if(!silent)
        {
          alert("All pipes have been deleted!");
        }
        return false;
      }
    }
    str += "<div class='Entries'><table><tr><td>Source</td><td>Source interface parameters</td><td>Destination</td><td>Destination interface parameters</td><td></td></tr>";
    for(i = 0; i<(max/2); i++)
    {
      for(j=0; j<max; j++)
      {
        if(entries[j]!=null)
        {
          peer = findId(entries, entries[j].peerId);
          if(peer == -1)
          {
            alert("Error occurred!");
            disp.innerHTML = "";
            return false;
          }
          else
          { 
            str += "<tr>" + addTableEntry(entries[j]) + addTableEntry(entries[peer]);
            str += "<td><input type='button' value='Delete' class='pageButton' onclick='javascript:deleteEntry("+ entries[peer].thisId.toString() + "," + entries[peer].peerId.toString() + ");'></td></tr>";
            entries[j] = null;
            entries[peer] = null;

          }
          break;
        }
      }
    }

    str += "</table></div>";
    document.getElementById('cfgContent').innerHTML = str + "<br><br>";
    return true;
}

/*!
* @brief Converts decimal integer to hexadecimal string.
* @param i: decimal integer 
*/
function dec2hex(i) 
{
   return ("0000000" + parseInt(i, 10).toString(16)).substr(-8);
}

/*!
* @brief This function is performed when user click on Upload button.
* All configurations then will be loaded from MCU.
*/
function onCfgUp()
{
  var url = "config.html";
  var message = "flashCfg=";
  var i,k;
  var max = sessvars.entriesNum;
  var entries = sessvars.entries.slice(0);
  var peer;
  var str = "";
  if(max==0)
  {
      alert("No pipes added yet, cannot upload!"); 
     return false;
  }
  else
  {
    for(k=0;k<max;k++)
    {
      if(entries[k]!=null)
      {
        break;
      }
    }
    if(k==max)
    {
      alert("All pipes have been deleted, cannot upload!");
      return false;
    }
  }

  for(k=0;k<max;k++)
  {
    if(entries[k]!=null)
    {
      message += dec2hex(entries[k].srcDest);
      message += dec2hex(entries[k].thisId);
      message += dec2hex(entries[k].peerId);
      message += dec2hex(entries[k].typeIf);

      switch(Number(entries[k].typeIf))
      {
        case 0: /* RS485 */
        message += dec2hex(entries[k].objData.ifNum);
        message += dec2hex(entries[k].objData.baud);
        message += dec2hex(entries[k].objData.parity);
        message += dec2hex(entries[k].objData.stop);
        message += dec2hex(entries[k].objData.mode);

        break;

        case 1: /* CAN */
        message += dec2hex(entries[k].objData.speed);
        message += dec2hex(entries[k].objData.stdId);
        message += dec2hex(entries[k].objData.dataByte);
        message += dec2hex(entries[k].objData.mode);

        break;

        case 2: /* USB */
        message += dec2hex(entries[k].objData.mode);
        break;

        case 3: /* ETH */
        message += dec2hex(entries[k].objData.proto);
        message += dec2hex(entries[k].objData.port);
        message += dec2hex(entries[k].objData.host.length);
        var i;
        var add;
        for(i=0;i<entries[k].objData.host.length;i+=4)
        {
          add = Number(entries[k].objData.host.charCodeAt(i))*0x01;
          if((i+1) < entries[k].objData.host.length)
          {
             add += Number(entries[k].objData.host.charCodeAt(i+1))*0x0100;
             if((i+2) < entries[k].objData.host.length)
             {
               add += Number(entries[k].objData.host.charCodeAt(i+2))*0x010000;
               if((i+3) < entries[k].objData.host.length)
               {
                 add += Number(entries[k].objData.host.charCodeAt(i+3))*0x01000000;
               }
             }
          }
          message += dec2hex(add);
        }
        break;

        case 4:
        break;

        default:
        alert("Error when parsing output data, cannot upload!");
        return false;
        break;

      }
      /* interface descriptor terminator */
      message += "a5a5a5a5"; 
      message += "5a5a5a5a";

    }

  }

  if (window.XMLHttpRequest) 
  { /* Non-IE browsers */
        req = new XMLHttpRequest(); 
        try 
        { 
          req.open("POST", url, true); 
        } 
        catch (e) 
        { 
          alert(e); 
          return false;
        } 
  } 
  else if (window.ActiveXObject) 
  { /* IE */
        req = new ActiveXObject("Microsoft.XMLHTTP"); 
        if (req) 
        { 
          req.open("POST", url, true); 
        } 
  } 

  try
  {
    req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    req.send(message);
  }
  catch(e)
  {
    alert("Problem sending configuration data, check the connection!");
    return false;
  }

  alert("Configuration successfully uploaded!");
  return true;

}

/*!
* @brief This function is performed when user click on Download button.
* All configurations then will be loaded to MCU.
*/
function onCfgDown()
{
    if (window.XMLHttpRequest) 
    { /* Non-IE browsers */
          req = new XMLHttpRequest(); 
          try 
          { 
            req.open("GET","flsh.html", false); 
          } 
          catch (e) 
          { 
            alert(e); 
          } 
          req.send(null); 
    } 
    else if (window.ActiveXObject) 
    { /* IE */
          req = new ActiveXObject("Microsoft.XMLHTTP"); 
          if (req) 
          { 
            req.open("GET", "flsh.html", false); 
            req.send();
          } 
    } 
    
    if (req.readyState == 4) 
    { /* Complete */
          if (req.status == 200) 
          { /* OK response */
              deleteAll(); 
              if(AddFromString(req.responseText)==false)
              {
                deleteAll();
              }
              else
              {
                onCfgShow(true);
              }
          } 
          else 
          { 
            alert("Problem: " + req.statusText); 
          } 
    }
    else
    {
        alert("Problem: " + req.statusText); 
    }
}

function AddFromString(str)
{

  var i,k;
  var entries = sessvars.entries;
  var peer;

  var srcDest, Id, peerId, typeIf, ifNum, baud, parity, stop, mode;
  var speed, stdId, dataByte, mode, proto, port, host, obj;

  for(k=0;k<str.length;)
  {

    srcDest = parseInt(str.substr(k, 8), 16);
    k += 8;
    Id = parseInt(str.substr(k, 8), 16);
    k += 8;
    peerId = parseInt(str.substr(k, 8), 16);
    k += 8;
    typeIf = parseInt(str.substr(k, 8), 16);
    k += 8;

    switch(typeIf)
    {
      case 0: /* RS485 */
      ifNum = parseInt(str.substr(k, 8), 16);
      k += 8;
      baud = parseInt(str.substr(k, 8), 16);
      k += 8;
      parity = parseInt(str.substr(k, 8), 16);
      k += 8;
      stop = parseInt(str.substr(k, 8), 16);
      k += 8;
      mode = parseInt(str.substr(k, 8), 16);
      k += 8;
      break;

      case 1: /* CAN */
      speed = parseInt(str.substr(k, 8), 16);
      k += 8;
      stdId = parseInt(str.substr(k, 8), 16);
      k += 8;
      dataByte = parseInt(str.substr(k, 8), 16);
      k += 8;
      mode = parseInt(str.substr(k, 8), 16);
      k += 8;
      break;

      case 2: /* USB */
      mode = parseInt(str.substr(k, 8), 16);
      k += 8;
      break;

      case 3: /* ETH */
      proto = parseInt(str.substr(k, 8), 16);
      k += 8;
      port = parseInt(str.substr(k, 8), 16);
      k += 8;
      var len = parseInt(str.substr(k, 8), 16);
      /* use entire 32bit words */
      /* if len is i.e. 13, round to 16. */
      if((len%4)!=0)
      {
        len += 4;
        len -= (len%4);
      }

      k += 8;
      host = "";
      var i;
      for(i=0;i<len;i+=4)
      {
        s3 = String.fromCharCode(parseInt(str.substr(k, 2), 16)); k += 2;
        s2 = String.fromCharCode(parseInt(str.substr(k, 2), 16)); k += 2;
        s1 = String.fromCharCode(parseInt(str.substr(k, 2), 16)); k += 2;
        s0 = String.fromCharCode(parseInt(str.substr(k, 2), 16)); k += 2;
        host += s0 + s1 + s2 + s3;
      }
      break;

      case 4: /* WIFI */
      break;

      default:
      return false;
      break;

    }
    /* interface descriptor terminator */
    if(str.substr(k, 8)!= "A5A5A5A5")
    {
      return false;
    }
    else
    {
      k += 8;
    }

    if(str.substr(k, 8)!= "5A5A5A5A")
    {
      return false;
    }
    else
    {
      k += 8;
      switch(typeIf)
      {
        case 0: /* RS485 */
        obj = new rs485Data(baud, parity, stop, ifNum, mode);
        break;

        case 1: /* CAN */
        obj = new canData(speed, stdId, dataByte, mode);
        break;

        case 2: /* USB */
        obj = new usbData(mode);
        break;

        case 3: /* ETH */
        obj = new ethData(proto, port, host);
        break;

        case 4: /* WIFI */
        obj = 0;
        break;

        default:
        return false;
        break;
      }
      sessvars.entries[sessvars.entriesNum] = new entry(srcDest, typeIf, Id, peerId, obj);
      sessvars.entriesNum++;
    }
  }
  return true;
}

/*!
* @brief This function is called to change used source interface.
* @param elem: 
*/
function onChgSrcIf(elem)
{
   sessvars.srcIface = elem.value;
   LoadInterface(elem, 'srcIf','cfgSrcIf');
   if(LoadDefaults(Number(elem.value), 0)==false)
   {
      document.getElementById('cfgSrcIf').innerHTML = "";
      document.getElementById('cfgDstIf').innerHTML = "";
      alert("Interface already used!");
      return;
   }
   if(sessvars.dstIface != undefined)
   {
      if(sessvars.dstIface != "")
      {
        document.forms.pipeCfg.dstIf.value = sessvars.dstIface;
        onChgDstIf(document.forms.pipeCfg.dstIf, false);
      }
   }
}

/*!
* @brief This function is called to change used destination interface.
* @param elem: 
* @param pop:
*/
function onChgDstIf(elem, pop)
{
   sessvars.dstIface = elem.value;
   LoadInterface(elem, 'dstIf','cfgDstIf');
   if(LoadDefaults(Number(elem.value), 1)==false)
   {
      document.getElementById('cfgDstIf').innerHTML = "";
      if(pop == true)
      {
        alert("Interface already used!");
      }
      return;
   }
}

/*!
* @brief Loads the default settings.
* @param type:
* @param isDist: 
*/
function LoadDefaults(type, isDst)
{

    if(type == 0)
    {
      if(isDst == 0)
      {
        if(filterEntry(type, 0, 1).length == 2)
        {
          return false;
        }
        else
        {
          return true;
        }
      }
      return true;
    }

    var same = filterEntry(type, isDst, (isDst==1)?0:1); 
    var entries = filterEntry(type, (isDst==1)?0:1, isDst);
    var form = document.forms.pipeCfg;
    var usingSame = 0;

    if(type == 4)
    {
      if(same.length != 0)
      {
        return false;
      }
    }

    if(entries.length == 0)
    {
      usingSame = 1;
      entries = same;
      if(same.length == 0)
      {
        return true;
      }
    }

    if(isDst==1)
    {
      if(type != 1)
      {
        form.dstMode = entries[0].objData.mode;
      }
      if(type == 1)
      {
        form.dstSpeed.value = entries[0].objData.speed; 

         /* allow the same stdId for dst */
        form.dstStdId.value = entries[0].objData.stdId; 
        
        form.dstDataByte.value = entries[0].objData.DataByte; 
      }
      else if(type == 3)
      {
        form.dstProto.value = entries[0].objData.proto; 
        if(usingSame == 0)
        {
            if(findEntry(same, "port", entries[0].objData.port.value).length == 0)
            {
              form.dstPort.value = entries[0].objData.port;
            } 
        }
        form.dstHost.value = entries[0].objData.host; 
      }
    }
    else
    {
      if(type != 1)
      {
        form.srcMode = entries[0].objData.mode;
      }

      if(type == 1)
      {
        form.srcSpeed.value = entries[0].objData.speed; 
        if(usingSame == 0)
        {
            if(findEntry(same, "stdId", entries[0].objData.stdId.value).length == 0)
            {
              form.srcStdId.value = entries[0].objData.stdId;
            } 
        }
      }
      else if(type == 2)
      {
        if(same.length != 0)
        {
          return false;
        }
      }
      else if(type == 3)
      {
        form.srcProto.value = entries[0].objData.proto; 
        if(usingSame == 0)
        {
            if(findEntry(same, "port", entries[0].objData.port.value).length == 0)
            {
              form.srcPort.value = entries[0].objData.port;
            } 
        }
      }
    }
    return true;
}

function filterEntry(typeIf, dst, src)
{
   var retval = [];
   var i = 0;
   var j=0;

   for(i=0; i<sessvars.entriesNum; i++)
   {
      if(sessvars.entries[i]!=null)
      {
        if(sessvars.entries[i].typeIf == typeIf)
        {
           if(((sessvars.entries[i].srcDest == 0)&&(src==1))||((sessvars.entries[i].srcDest == 1)&&(dst==1)))
           {
             retval[j++] = sessvars.entries[i];
           }
        }
      }
      
   }

   return retval;
}

function findEntry(entries, property, value)
{
  var i, j=0;
  var out = [];
  for(i=0; i<entries.length; i++)
  {
    if(entries[i] != null)
    {
      if(entries[i].objData[property].value==value)
      {
        out[j++] = entries[i];
      }
    }
  }
  return out;
}

/*!
* @brief 
* @param entries:
* @param idval: 
*/
function findId(entries, idval)
{
  var i;
  for(i=0; i<entries.length; i++)
  {
    if(entries[i] != null)
    {
      if(entries[i].thisId==idval)
      {
        return i;
      }
    }
  }
  return -1;
}
