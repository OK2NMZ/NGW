:: make temporary folder with its hierarchy
mkdir temporary
cd temporary
mkdir dstIf
mkdir if
mkdir srcIf
cd ..
:: copy all files ignored by Compressor to temporary folder
COPY fs/bluebliss.css temporary/bluebliss.css
COPY fs/*.gif temporary/*.gif
:: run Compressor program to compress code
Compressor.exe C:\\Users\\Lenka\\networkgateway\\Firmware\\NetworkGateway\\WebServer
:: remove temporary template file
del C:\Users\Lenka\networkgateway\Firmware\NetworkGateway\WebServer\temporary\new_file.js
:: run makefsdata program to create fsdata.c
makefsdata.exe temporary -e
:: remove temporary with its hierarchy
rmdir temporary /S /Q
:: move fsdata.c into src folder
mv fsdata.c src/fsdata.c
