/**
  ******************************************************************************
  * @file    httpserver-post.c
  * @author  Marek NOVAK
  * @version v1.0
  * @date    2014
  * @brief   Basic http server implementation using LwIP socket API - POST method
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, AUTHOR SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2014 Marek NOVAK (ok2nmz@gmail.com)</center></h2>
  ******************************************************************************
  */
#include "stdint.h"
#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "fs.h"
#include "fsdata.h"
#include "string.h"
#include "httpserver-socket.h"
#include "tlsf.h"
#include "httpserver-post.h"
#include "usbd_cdc_if.h"
#include "usb_device.h"
#include "dynamicInit.h"
#include "lwip/ip_addr.h"
#include "can.h"


/* Private typedef -----------------------------------------------------------*/
typedef struct
{
   char*  name;
   void (*handler)(char*, int);
   uint8_t len;
 }get_function;
/* Private define ------------------------------------------------------------*/
#define GET_FLAG_NON_STATIC (0x01)
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
 void post_wifi_test(char* arg, int arg_len);
 void post_usb_test(char* arg, int arg_len);
 void post_rs485_1_test(char* arg, int arg_len);
 void post_rs485_2_test(char* arg, int arg_len);
 void post_rs485_baudrate(char* arg, int arg_len);
 void post_rs485_parity(char* arg, int arg_len);
 void post_rs485_stopBits(char* arg, int arg_len);
 void post_can_test(char* arg, int arg_len);
 void post_can_stdId(char* arg, int arg_len);
 void post_can_dataByte(char* arg, int arg_len);
 void post_upload_config(char* arg, int arg_len);
 void post_eth_proto(char* arg, int arg_len);
 void post_eth_IP(char* arg, int arg_len);
 void post_eth_port(char* arg, int arg_len);
 void post_eth_send(char* arg, int arg_len);
 void post_unknown_field(char* arg, int arg_len);
/* Private variables ---------------------------------------------------------*/
get_function http_post_functions[] =
{
  {"wifiThru", post_wifi_test, 8},
  {"usbThru", post_usb_test, 7},
  {"RS485-1-data", post_rs485_1_test, 12},
  {"RS485-2-data", post_rs485_2_test, 12},
  {"baudrate", post_rs485_baudrate, 8},
  {"parity", post_rs485_parity, 6},
  {"stop", post_rs485_stopBits, 4},
  {"canData", post_can_test, 7},
  {"stdId", post_can_stdId, 5},
  {"dataByte", post_can_dataByte, 8},
  {"EthProto", post_eth_proto, 8},
  {"EthIP", post_eth_IP, 5},
  {"EthPort", post_eth_port, 7},
  {"EthSend", post_eth_send, 7},
  {"flashCfg", post_upload_config, 8},
  {"", post_unknown_field, 0}, /* If nothing matches, this does */
};

/**
  * @brief  Replaces URL formater characters by ASCII ones
  * @param  io  String to reformat
  * @retval None
  */
void http_format_decoder(char* io)
{
    int readpos = 0;
    int writepos = 0;
    unsigned int c;
    while(io[readpos])
    {
        if(io[readpos]=='%')
        {
            readpos++;
            sscanf(&io[readpos], "%02X", &c);
            readpos+=2;
            io[writepos++] = (char)c;
        }
        else if(io[readpos]=='+')
        {
            readpos++;
            io[writepos++] = ' ';
        }
        else
        {
            io[writepos++] = io[readpos++];
        }

    }
    io[writepos] = '\0';

}

/**
  * @brief  Handles POST requests
  * @param  conn connection socket
  * @retval None
  */
void http_server_method_post(int conn)
{
    const char* dataStartDelimiter = "\r\n\r\n";
#define BUFLEN 1500
    int ret;
    char recv_buffer[BUFLEN];
    char* ptr;
    char* curr;
    char* next;
    int i;


    /* Read in the request */
    ret = read(conn, (unsigned char*)recv_buffer, BUFLEN-1);
    recv_buffer[BUFLEN-1] = '\0'; /* Lets be save here */
    if(ret <= 0) /* Socket error */
    {
        return;
    }

    ptr = strstr(recv_buffer, dataStartDelimiter);
    if(ptr == NULL)
    {
      return;
    }
    curr = ptr + sizeof(dataStartDelimiter); /* Find the begining of the data */
    ptr = (char*)(recv_buffer + ret);
    *ptr = '\0'; /* Terminate the data string */

    for(i = 0; curr!=ptr;)
    {
        if(strncmp(curr, http_post_functions[i].name, http_post_functions[i].len)==0)
        {
            /* Zero-length is the unknown field, pass it thru */
            if(http_post_functions[i].len)
            {
                curr += http_post_functions[i].len + 1;
            }

            next = strchr(curr, '&');

            if(next == NULL)
            {
                http_format_decoder(curr); /* Decode the data */
                http_post_functions[i].handler(curr, (int)(ptr-curr));
                break;
            }
            else
            {
                *next = '\0'; /* Terminate this parameters data section */
                http_format_decoder(curr); /* Decode the data */
                http_post_functions[i].handler(curr, (int)(next-curr));
                curr = next+1;
            }
            i = 0;
        }
        else
        {
            i++; /* Test against other parameter names */
        }
    }

  }

/******POST Handlers**********/
#include "stm32f2xx_hal_conf.h"
#include "usart.h"
CanTxMsgTypeDef CanTxTest = {0, 0, CAN_ID_STD, CAN_RTR_DATA, 8, {1, 2, 3, 4, 5, 6, 7, 8}};
uint32_t speed = 115200, stopBits = UART_STOPBITS_1, RS485parity = UART_PARITY_NONE;
struct ip_addr ipaddr = {0xFFFFFFFF};
ETHproto proto = protoUDP;
int port = 7;

/**
  * @brief  Process received post data
  * @param  arg function argument
  * @param  arg_len length of the argument string
  * @retval None
  */
void post_usb_test(char* arg, int arg_len)
{
    sendUSBStream((uint8_t*)arg, (uint32_t)arg_len, NULL);
}

/**
  * @brief  Process received post data
  * @param  arg function argument
  * @param  arg_len length of the argument string
  * @retval None
  */
void post_wifi_test(char* arg, int arg_len)
{
    sendWIFI((uint8_t*)arg, (uint32_t)arg_len, NULL);
}

/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_rs485_1_test(char* arg, int arg_len)
{
    UART_InitTypeDef backup;
    if(uart1isInitialized)
    {
        backup.BaudRate = huart1.Init.BaudRate;
        backup.WordLength = huart1.Init.WordLength;
        backup.StopBits = huart1.Init.StopBits;
        backup.Parity = huart1.Init.Parity;
        backup.Mode = huart1.Init.Mode;
        backup.HwFlowCtl = huart1.Init.HwFlowCtl;
        backup.OverSampling = huart1.Init.OverSampling;
    }

    huart1.Instance = USART1;
    huart1.Init.BaudRate = speed;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = stopBits;
    huart1.Init.Parity = RS485parity;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;
    HAL_UART_Init(&huart1);

    sendRS485_1_Stream((uint8_t*)arg, (uint32_t)arg_len, NULL);

    if(uart1isInitialized)
    {
        huart1.Init.BaudRate = backup.BaudRate;
        huart1.Init.WordLength = backup.WordLength;
        huart1.Init.StopBits = backup.StopBits;
        huart1.Init.Parity = backup.Parity;
        huart1.Init.Mode = backup.Mode;
        huart1.Init.HwFlowCtl = backup.HwFlowCtl;
        huart1.Init.OverSampling = backup.OverSampling;
        HAL_UART_Init(&huart1);
    }
}


/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_rs485_2_test(char* arg, int arg_len)
{
    UART_InitTypeDef backup;

    if(uart6isInitialized)
    {
        backup.BaudRate = huart6.Init.BaudRate;
        backup.WordLength = huart6.Init.WordLength;
        backup.StopBits = huart6.Init.StopBits;
        backup.Parity = huart6.Init.Parity;
        backup.Mode = huart6.Init.Mode;
        backup.HwFlowCtl = huart6.Init.HwFlowCtl;
        backup.OverSampling = huart6.Init.OverSampling;
    }

    huart6.Instance = USART6;
    huart6.Init.BaudRate = speed;
    huart6.Init.WordLength = UART_WORDLENGTH_8B;
    huart6.Init.StopBits = stopBits;
    huart6.Init.Parity = RS485parity;
    huart6.Init.Mode = UART_MODE_TX_RX;
    huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart6.Init.OverSampling = UART_OVERSAMPLING_16;
    HAL_UART_Init(&huart6);

    sendRS485_2_Stream((uint8_t*)arg, (uint32_t)arg_len, NULL);

    if(uart6isInitialized)
    {
        huart6.Init.BaudRate = backup.BaudRate;
        huart6.Init.WordLength = backup.WordLength;
        huart6.Init.StopBits = backup.StopBits;
        huart6.Init.Parity = backup.Parity;
        huart6.Init.Mode = backup.Mode;
        huart6.Init.HwFlowCtl = backup.HwFlowCtl;
        huart6.Init.OverSampling = backup.OverSampling;
        HAL_UART_Init(&huart6);
    }

}


/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_rs485_baudrate(char* arg, int arg_len)
{
    sscanf(arg, "%d", &speed);
}


/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_rs485_parity(char* arg, int arg_len)
{
    sscanf(arg, "%d", &RS485parity);
    switch(RS485parity)
    {
        default:
        case 0:
            RS485parity = UART_PARITY_NONE;
        break;

        case 1:
            RS485parity = UART_PARITY_EVEN;
        break;

        case 2:
            RS485parity = UART_PARITY_ODD;
        break;
    }
}


/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_rs485_stopBits(char* arg, int arg_len)
{
      sscanf(arg, "%d", &stopBits);
      if(stopBits == 0)
      {
          stopBits = UART_STOPBITS_1;
      }
      else
      {
          stopBits = UART_STOPBITS_2;
      }
}


/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_can_test(char* arg, int arg_len)
{
    sscanf(arg, "%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x", &CanTxTest.Data[0], &CanTxTest.Data[1],
            &CanTxTest.Data[2], &CanTxTest.Data[3], &CanTxTest.Data[4], &CanTxTest.Data[5],
            &CanTxTest.Data[6], &CanTxTest.Data[7]);

    hcan1.pTxMsg = &CanTxTest;
    HAL_CAN_Transmit(&hcan1, HAL_MAX_DELAY);
}


/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_can_stdId(char* arg, int arg_len)
{
    int i;
    sscanf(arg, "%d", &i);
    CanTxTest.StdId = i;
}

/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_can_dataByte(char* arg, int arg_len)
{
    int i;
    sscanf(arg, "%d", &i);
    CanTxTest.DLC = i;
}

/**
* @brief  Save selected protocol
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_eth_proto(char* arg, int arg_len)
{
    int p;
    sscanf(arg, "%d", &p);
    proto = (ETHproto)p;
}

/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_eth_IP(char* arg, int arg_len)
{
    ipaddr_aton(arg, &ipaddr);
}

/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_eth_port(char* arg, int arg_len)
{
    sscanf(arg, "%d", &port);
}

/**
* @brief  Process received post data
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_eth_send(char* arg, int arg_len)
{
    struct sockaddr_in address;
    int rh_size;
    struct sockaddr_in remotehost;
    int dataSent = 0;
    int sock;

    if(proto == protoTCP)
    {
        /* create a TCP socket */
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            /*can not create socket*/
            return;
        }

        /* bind to port at any interface */
        address.sin_family = AF_INET;
        address.sin_port = 0; //chooses any available
        address.sin_addr.s_addr = INADDR_ANY;

        if (bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0)
        {
            /* can not bind socket */
            return;
        }

        remotehost.sin_family = AF_INET;
        remotehost.sin_addr.s_addr = (u32_t)(ipaddr.addr);
        remotehost.sin_port = htons(port);

        rh_size = sizeof(remotehost);

        if(connect(sock, (struct sockaddr *)&remotehost, (socklen_t)rh_size) < 0)
        {
            close(sock);
            return;
        }

        while(arg_len != 0)
        {
            dataSent = send(sock, arg, arg_len, 0);
            if(dataSent < 0 )
            {
                //fail
                close(sock);
                return;
            }
            else
            {
                arg_len -= dataSent;
                arg += dataSent;
            }
        }
        close(sock);
    }
    else //protoUDP
    {
        /* create a UDP socket */
        if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        {
            /*can not create socket*/
            return;
        }


        /* bind to port at any interface */
        address.sin_family = AF_INET;
        address.sin_port = 0; //chooses any available
        address.sin_addr.s_addr = INADDR_ANY;

        if (bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0)
        {
            /* can not bind socket */
            close(sock);
            return;
        }

        remotehost.sin_family = AF_INET;
        remotehost.sin_addr.s_addr = (u32_t)(ipaddr.addr);
        remotehost.sin_port = htons(port);
        rh_size = sizeof(remotehost);

        while(arg_len != 0)
        {
            dataSent = sendto(sock, arg, arg_len, 0, (struct sockaddr *)&remotehost, (socklen_t)rh_size);
            if(dataSent < 0 )
            {
                //fail
                close(sock);
                return;
            }
            else
            {
                arg_len -= dataSent;
                arg += dataSent;
            }
        }

        close(sock);
    }
}


/**
* @brief  Converts a two-character string to
*         a byte number
* @param  str string to convert
* @retval byte representation of a string
*/
unsigned char str2byte(char* str)
{
    unsigned char retval = 0;

    if(str[0] >= 'a')
    {
        retval += str[0]-'a'+10;
    }
    else
    {
        retval += str[0]-'0';
    }

    retval <<= 4;

    if(str[1] >= 'a')
    {
        retval += str[1]-'a'+10;
    }
    else
    {
        retval += str[1]-'0';
    }
    return retval;
}

/**
* @brief  Converts a six-character string to a hex number
* @param  str string to convert
* @retval hexadecimal representation of a string
*/
unsigned int str2hex(char* str)
{
    int i = 0;
    unsigned int retval = 0;

    for(i=0; i < 4; i++)
    {
        retval += (1<<((3-i)<<3))*str2byte(&str[2*i]);
    }
    return retval;
}

/**
* @brief  New configuration upload handler
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_upload_config(char* arg, int arg_len)
{
    int i = 0;

    if(arg_len/8 > CONFIG_LENGTH_MAX) //do not let it to corrupt the program flash!
    {
        return;
    }

    HAL_FLASH_Unlock();
    FLASH_Erase_Sector(CONFIG_SECTOR_NUM, VOLTAGE_RANGE_3);
    HAL_FLASH_Program(TYPEPROGRAM_WORD, CONFIG_LENGTH_ADDR, arg_len/8);
    for(i=0; i<arg_len; i+=8)
    {
        HAL_FLASH_Program(TYPEPROGRAM_WORD, CONFIG_START_ADDR+(i>>1), str2hex(&arg[i]));
    }
    HAL_FLASH_Lock();
}


/**
* @brief  Unknown post field handler
* @param  arg function argument
* @param  arg_len length of the argument string
* @retval None
*/
void post_unknown_field(char* arg, int arg_len)
{
    asm("nop");
    asm("nop");
    asm("nop");
}