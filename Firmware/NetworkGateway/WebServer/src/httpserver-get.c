/**
  ******************************************************************************
  * @file    httpserver-get.c
  * @author  Marek NOVAK
  * @version v1.0
  * @date    2014
  * @brief   Basic http server implementation using LwIP socket API - GET method
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, AUTHOR SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2014 Marek NOVAK (ok2nmz@gmail.com)</center></h2>
  ******************************************************************************
  */
#include "stdint.h"
#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "fs.h"
#include "fsdata.h"
#include "string.h"
#include "httpserver-socket.h"
#include "tlsf.h"
#include "httpserver-get.h"
#include "dynamicInit.h"


/* Private define ------------------------------------------------------------*/
#define GET_NO_HEADER           (0x08)
#define GET_NO_FOOTER           (0x04)
#define GET_STATIC_LOAD_AFTER   (0x02)
#define GET_STATIC_LOAD_BEFORE  (0x01)
#define HTTP_FLAGS_IMAGE    (GET_STATIC_LOAD_BEFORE|GET_NO_HEADER|GET_NO_FOOTER)
#define HTTP_FLAGS_RX       (GET_NO_HEADER|GET_NO_FOOTER)
#define HTTP_MAX_PATH_LEN (48)
/* Private typedef -----------------------------------------------------------*/
typedef struct
{
   char*  page_name;
   void (*handler)(int, char*, int);
   uint8_t flags;
   uint8_t len;
}get_function;
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void get_system_status(int conn, char* arg, int arg_len);
void get_index(int conn, char* arg, int arg_len);
void get_static_or_404(int conn, char* arg, int arg_len);
void get_flash_config(int conn, char* arg, int arg_len);
void get_dns_resolve(int conn, char* arg, int arg_len);

/* Private variables ---------------------------------------------------------*/
/* Format of dynamic web page: the page header */
const char const HTTP_PAGE_START[] =
"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\">"
"<html>"
"<head>"
"<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />"
"</head>"
"<body>"
;
const char const HTTP_PAGE_END[] =
"</body>"
"</html>"
;
const get_function http_get_functions[] = {
  {"/ ", get_index, GET_NO_HEADER|GET_NO_FOOTER, 2},
  {"/index.html", NULL, GET_STATIC_LOAD_BEFORE|GET_NO_HEADER|GET_NO_FOOTER, 11},
  {"/stats.html", get_system_status, GET_STATIC_LOAD_AFTER, 11},
  {"/flsh.html", get_flash_config, GET_NO_HEADER|GET_NO_FOOTER, 10},
  {"/dns.html", get_dns_resolve, GET_NO_HEADER|GET_NO_FOOTER, 9},
  {"", get_static_or_404, GET_NO_HEADER|GET_NO_FOOTER, 0}, /* Comparison always matches */
};

/**
  * @brief  Handles GET requests
  * @param  conn connection socket
  * @retval None
  */
void http_server_method_get(int conn)
{
#define HTTP_BUFFER_SIZE (1500)
  int ret;
  struct fs_file * file;
  char recv_buffer[HTTP_BUFFER_SIZE];
  int i;

    /* Read in the request */
  ret = read(conn, (unsigned char*)recv_buffer, HTTP_BUFFER_SIZE);

  if(ret <= 0) /* Socket error */
  {
    return;
  }

  for(i = 0;; i++)
  {
    if(strncmp(recv_buffer, http_get_functions[i].page_name, http_get_functions[i].len)==0)
    {
       if(!(http_get_functions[i].flags & GET_NO_HEADER))
       {
          write(conn, HTTP_PAGE_START, sizeof(HTTP_PAGE_START));
       }

      if(http_get_functions[i].flags & GET_STATIC_LOAD_BEFORE)
      {
        file = fs_open(http_get_functions[i].page_name);
        if(file)
        {
          write(conn, (const unsigned char*)(file->data), (size_t)file->len);
          fs_close(file);
        }

        if(http_get_functions[i].handler != NULL)
        {
            http_get_functions[i].handler(conn, (recv_buffer + http_get_functions[i].len), ret - http_get_functions[i].len);
        }
      }
      else if(http_get_functions[i].flags & GET_STATIC_LOAD_AFTER)
      {
        if(http_get_functions[i].handler != NULL)
        {
            http_get_functions[i].handler(conn, (recv_buffer + http_get_functions[i].len), ret - http_get_functions[i].len);
        }
        file = fs_open(http_get_functions[i].page_name);
        if(file)
        {
          write(conn, (const unsigned char*)(file->data), (size_t)file->len);
          fs_close(file);
        }

      }
      else
      {
        if(http_get_functions[i].handler != NULL)
        {
            http_get_functions[i].handler(conn, (recv_buffer + http_get_functions[i].len), ret - http_get_functions[i].len);
        }
        else
        {
            write(conn, "Error!", 6);
        }
      }

       if(!(http_get_functions[i].flags & GET_NO_FOOTER))
       {
          write(conn, HTTP_PAGE_END, sizeof(HTTP_PAGE_END));
       }

      return;

    }
  }
}

/* get functions handlers and variables ---------------------------------------------------------*/
u32_t nPageHits = 0;
/**
  * @brief  Create and send a dynamic Web Page. This page contains the list of
  *         running tasks and the number of page hits.
  * @param  conn connection socket
  * @param  arg function argument
  * @param  arg_len length of the argument string
  * @retval None
  */
void get_system_status(int conn, char* arg, int arg_len)
{
    const char tableStart[] =
    "<div class=\"Entries\" >"
    "<table>"
    "<tr>"
    "<td>Name</td>"
    "<td>State</td>"
    "<td>Priority</td>"
    "<td>Base Priority</td>"
    "<td>High-water</td>"
    "</tr>";
    const char tableEnd[] =
    "</td></tr></table></div>";
    const char rowStart[] =
    "<tr><td>";
    const char rowEnd[] =
    "</tr></td>";
    const char rowSeparator[] =
    "</td><td>";
    const char* taskStatesTxt[] = {"Running  ", "Ready    ", "Blocked  ", "Suspended", "Deleted  "};
    char PAGE_BODY[128];
    xTaskStatusType *pxTaskStatusArray;
    volatile unsigned portBASE_TYPE uxArraySize, x;

    /* Update the hit count */
    nPageHits++;

    write(conn, (unsigned char*)tableStart, sizeof(tableStart));

    /* Take a snapshot of the number of tasks in case it changes while this
    function is executing. */
    uxArraySize = uxTaskGetNumberOfTasks();

    /* Allocate an array index for each task. */
    pxTaskStatusArray = pvPortMalloc( uxArraySize * sizeof( xTaskStatusType ) );

    if( pxTaskStatusArray != NULL )
    {
        /* Generate the (binary) data. */
        uxArraySize = uxTaskGetSystemState( pxTaskStatusArray, uxArraySize, NULL );

        /* Create a human readable table from the binary data. */
        for( x = 0; x < uxArraySize; x++ )
        {
            write(conn, (unsigned char*)rowStart, sizeof(rowStart));
            write(conn, (unsigned char*)pxTaskStatusArray[x].pcTaskName, strlen(pxTaskStatusArray[x].pcTaskName));
            write(conn, (unsigned char*)rowSeparator, sizeof(rowSeparator));
            write(conn, (unsigned char*)taskStatesTxt[pxTaskStatusArray[x].eCurrentState], strlen(taskStatesTxt[pxTaskStatusArray[x].eCurrentState]));
            write(conn, (unsigned char*)rowSeparator, sizeof(rowSeparator));
            sprintf((char*)PAGE_BODY, "%2d", pxTaskStatusArray[x].uxCurrentPriority);
            write(conn, (unsigned char*)PAGE_BODY, strlen(PAGE_BODY));
            write(conn, (unsigned char*)rowSeparator, sizeof(rowSeparator));
            sprintf((char*)PAGE_BODY, "%2d", pxTaskStatusArray[x].uxBasePriority);
            write(conn, (unsigned char*)PAGE_BODY, strlen(PAGE_BODY));
            write(conn, (unsigned char*)rowSeparator, sizeof(rowSeparator));
            sprintf((char*)PAGE_BODY, "%5d", pxTaskStatusArray[x].usStackHighWaterMark);
            write(conn, (unsigned char*)PAGE_BODY, strlen(PAGE_BODY));
            write(conn, (unsigned char*)rowEnd, sizeof(rowEnd));
        }

        /* Free the array again. */
        vPortFree( pxTaskStatusArray );
    }

    write(conn, (unsigned char*)tableEnd, sizeof(tableEnd));
    sprintf((char *)PAGE_BODY, "<br> Used Mem: %6dB, Available Mem: %6dB, Page Hits: %7d <br>",
    tlsf_used_memory(), tlsf_available_memory(), nPageHits);
    write(conn, (unsigned char*)PAGE_BODY, strlen(PAGE_BODY));
}

/**
  * @brief  Gets the index page
  * @param  conn connection socket
  * @param  arg function argument
  * @param  arg_len length of the argument string
  * @retval None
  */
void get_index(int conn, char* arg, int arg_len)
{
    struct fs_file * file;
    file = fs_open("/index.html");
    if(file)
    {
      write(conn, (const unsigned char*)(file->data), (size_t)file->len);
      fs_close(file);
    }
}

/**
  * @brief  Gets the configuration from flash
  * @param  conn connection socket
  * @param  arg function argument
  * @param  arg_len length of the argument string
  * @retval None
  */
void get_flash_config(int conn, char* arg, int arg_len)
{
    uint32_t len = *((uint32_t*)CONFIG_LENGTH_ADDR), i;
    char txt[32];

    if((len == 0)||(len==0xFFFFFFFF)) //no config loaded
    {
        return;
    }

    for(i=0; i<len; i++)
    {
        snprintf(txt, 32, "%08X", *((uint32_t*)(CONFIG_START_ADDR+4*i)));
        write(conn, (const unsigned char*)txt, 8);
    }
}

/**
  * @brief  Gets an IP address in string format for supplied hostname
  * @param  conn connection socket
  * @param  arg function argument
  * @param  arg_len length of the argument string
  * @retval None
  */
void get_dns_resolve(int conn, char* arg, int arg_len)
{
    char none = '\0';
    char* ptr;
    char* pend;
    ip_addr_t result;
    ptr = strstr(arg, "url=");
    if(ptr != NULL)
    {
        ptr += 4; //skip "url="
        pend = strchr(ptr, ' ');
        *pend = '\0';
        if(netconn_gethostbyname(ptr, &result) != ERR_OK)
        {
            write(conn, (const unsigned char*)&none, 1);
        }
        else
        {
            sprintf(arg, "%s", ipaddr_ntoa((const ip_addr_t*)&result));
            write(conn, (const unsigned char*)arg, strlen(arg));
        }
    }
    else
    {
        write(conn, (const unsigned char*)&none, 1);
    }
}


/**
  * @brief  Gets a static page or 404.html
  * @param  conn connection socket
  * @param  arg function argument
  * @param  arg_len length of the argument string
  * @retval None
  */
void get_static_or_404(int conn, char* arg, int arg_len)
{
    struct fs_file * file;
    char* ptr;
    if(arg_len)
    {
        ptr = strchr(arg, ' ');
        if(ptr)
        {
            *ptr = '\0';
            file = fs_open(arg);
            if(file)
            {
              write(conn, (const unsigned char*)(file->data), (size_t)file->len);
              fs_close(file);
              return;
            }
        }

    }

    file = fs_open("/404.html");
    if(file)
    {
      write(conn, (const unsigned char*)(file->data), (size_t)file->len);
      fs_close(file);
      return;
    }
}