/**
  ******************************************************************************
  * @file    httpserver-socket.c
  * @author  MCD Application Team AND Marek NOVAK
  * @version V1.1.0
  * @date    07-October-2011
  * @brief   Basic http server implementation using LwIP socket API
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  * <h2><center>&copy; COPYRIGHT 2014 Marek NOVAK (ok2nmz@gmail.com)</center></h2>
  ******************************************************************************
  */

#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "fs.h"
#include "fsdata.h"
#include "string.h"
#include "httpserver-socket.h"
#include "tlsf.h"
#include "httpserver-get.h"
#include "httpserver-post.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define WEBSERVER_THREAD_PRIO    ( tskIDLE_PRIORITY + 4)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void http_server_method_unknown(int conn);
/* Private functions ---------------------------------------------------------*/

/**
  * @brief serve tcp connection
  * @param conn: connection socket
  * @retval None
  */
void http_server_serve(int conn)
{
#define BUFLEN 4
    int ret;
    struct fs_file * file;
    unsigned char recv_buffer[BUFLEN];

    /* Read in the request */
    ret = read(conn, recv_buffer, BUFLEN);
    if(ret < 0) return; /* Socket error occurred */

    if(strncmp(recv_buffer, "GET", 3)==0)
    {
        http_server_method_get(conn);
    }
    else if(strncmp(recv_buffer, "POST", 4)==0)
    {
        read(conn, recv_buffer, 1); /* Skip the space */
        http_server_method_post(conn);
    }
    else
    {
        http_server_method_unknown(conn);
    }

    /* Close connection socket */
    close(conn);
}

/**
  * @brief  http server thread
  * @param arg: pointer on argument(not used here)
  * @retval None
  */
static void http_server_socket_thread(void *arg)
{
    int sock, newconn, rh_size;
    struct sockaddr_in address, remotehost;

    /* create a TCP socket */
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("can not create socket");
        return;
    }

    /* bind to port 80 at any interface */
    address.sin_family = AF_INET;
    address.sin_port = htons(80);
    address.sin_addr.s_addr = INADDR_ANY;

    if (bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0)
    {
        printf("can not bind socket");
        return;
    }

    /* listen for incoming connections (TCP listen backlog = 5) */
    listen(sock, 5);

    rh_size = sizeof(remotehost);

    while (1)
    {
        newconn = accept(sock, (struct sockaddr *)&remotehost, (socklen_t *)&rh_size);
        http_server_serve(newconn);
    }
}

/**
  * @brief  Initialize the HTTP server (start its thread)
  * @param  none
  * @retval None
  */
void http_server_socket_init()
{
    sys_thread_new("HTTP", http_server_socket_thread, NULL, DEFAULT_THREAD_STACKSIZE, WEBSERVER_THREAD_PRIO);
}

/**
  * @brief  Handles unknown method requests
  * @param  conn connection socket
  * @retval None
  */
void http_server_method_unknown(int conn)
{
    struct fs_file * file;
    file = fs_open("/501.html");
    if(file)
    {
        write(conn, (const unsigned char*)(file->data), (size_t)file->len);
        fs_close(file);
    }
}


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/