
#include <stdlib.h>

/* Defining MPU_WRAPPERS_INCLUDED_FROM_API_FILE prevents task.h from redefining
all the API functions to use the MPU wrappers.  That should only be done when
task.h is included from an application file. */
#define MPU_WRAPPERS_INCLUDED_FROM_API_FILE

#include "FreeRTOS.h"
#include "task.h"

#undef MPU_WRAPPERS_INCLUDED_FROM_API_FILE

#include "tlsf.h"

/*-----------------------------------------------------------*/

/*-----------------------------------------------------------*/

void *pvPortMalloc( size_t xWantedSize )
{
	void *pvReturn = NULL;

	/* The heap must be initialised before the first call to
	prvPortMalloc(). */

	asm("cpsid i"); /* vTaskSuspendAll();  This could be used if we did no allocation in ISR context */
	{
		pvReturn = tlsf_malloc(DynamicAllocator, xWantedSize);
		traceMALLOC( pvReturn, xWantedSize );
	}
	asm("cpsie i"); /* ( void ) xTaskResumeAll(); This could be used if we did no allocation in ISR context */

	#if( configUSE_MALLOC_FAILED_HOOK == 1 )
	{
		if( pvReturn == NULL )
		{
			extern void vApplicationMallocFailedHook( void );
			vApplicationMallocFailedHook();
		}
		else
		{
			mtCOVERAGE_TEST_MARKER();
		}
	}
	#endif

	return pvReturn;
}
/*-----------------------------------------------------------*/
void vPortFree( void *pv )
{
	asm("cpsid i"); /* vTaskSuspendAll(); This could be used if we did no allocation in ISR context */
	{
        tlsf_free(DynamicAllocator, pv);
	}
	asm("cpsie i"); /* ( void ) xTaskResumeAll(); This could be used if we did no allocation in ISR context */
}

