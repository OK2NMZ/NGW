### Network Gateway

This page describes the Network Gateway device, which is a multi-purpose network device capable to interconnect CAN, RS485, USB, Ethernet and WiFi. The data pipes between endpoints are dynamically configurable via a web-based [GUI].
Don't forget to check the [presentation] of this project!
<br>
Don't forget to visit the project's :link:[WIKI] pages!

[GUI]: Firmware/NetworkGateway/WebServer/fs/index.html
[WIKI]: https://gitlab.com/OK2NMZ/NGW/wikis/home
[presentation]: http://ok2nmz.ltech.cz/ngw_index.html